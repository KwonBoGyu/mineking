﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cItem_Bomb_Collider : MonoBehaviour
{
    public cItem_Bomb_O _origin;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            //눈사람, 바위늘보는 자기가 만든 폭탄에는 피격 불가
            if (cUtil._player.skinId.Equals(6) && _origin.id.Equals(1))
            {
                return;
            }
            if (cUtil._player.skinId.Equals(8) && _origin.id.Equals(2))
            {
                return;
            }

            cUtil._player.ReduceHp(_origin.damage.value, Vector3.zero);
        }

        if (collision.gameObject.tag.Equals("Enemy"))
        {
            collision.transform.GetChild(0).GetComponent<cEnemy_monster>().ReduceHp(_origin.damage.value, Vector3.zero);
        }
    }
}
