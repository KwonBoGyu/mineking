﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cUseManager : MonoBehaviour
{
    public GameObject bombPrefab;
    public GameObject bombPrefab_snowman;
    public GameObject bombPrefab_monkey;
    private GameObject bomb;
    public GameObject torchPrefab;
    
    public void SetBomb(byte pChar = 0)
    {
        switch(pChar)
        {
            case 0:
                bomb = Instantiate(bombPrefab, cUtil._player.originObj.transform.position,
            Quaternion.identity, this.gameObject.transform);
                break;

            case 1:
                bomb = Instantiate(bombPrefab_snowman, cUtil._player.originObj.transform.position,
            Quaternion.identity, this.gameObject.transform);
                break;

            case 2:
                bomb = Instantiate(bombPrefab_monkey, cUtil._player.originObj.transform.position,
            Quaternion.identity, this.gameObject.transform);
                break;
        }

        bomb.GetComponent<cItem_Bomb_O>().id = pChar;

        if (cUtil._player.GetDirection().x > 0)
        {
            bomb.GetComponent<cItem_Bomb_O>().SetDir(new Vector3(0.5f, 0.5f));
        }
        else
        {
            bomb.GetComponent<cItem_Bomb_O>().SetDir(new Vector3(-0.5f, 0.5f));
        }

        bomb.SetActive(true);
    }

    public void SetBomb(Vector3 pDir, byte pChar = 0)
    {
        switch (pChar)
        {
            case 0:
                bomb = Instantiate(bombPrefab, cUtil._player.originObj.transform.position,
            Quaternion.identity, this.gameObject.transform);
                break;

            case 1:
                bomb = Instantiate(bombPrefab_snowman, cUtil._player.originObj.transform.position,
            Quaternion.identity, this.gameObject.transform);
                break;

            case 2:
                bomb = Instantiate(bombPrefab_monkey, cUtil._player.originObj.transform.position,
            Quaternion.identity, this.gameObject.transform);
                break;
        }

        bomb.GetComponent<cItem_Bomb_O>().id = pChar;

        bomb.GetComponent<cItem_Bomb_O>().SetDir(pDir);
        bombPrefab.SetActive(true);
    }

    public void SetTorch()
    {
        GameObject torch = Instantiate(torchPrefab, cUtil._player.originObj.transform.position,
            Quaternion.identity, this.gameObject.transform);
        torch.GetComponent<cLight>().SetLightRange(60);
        torch.SetActive(true);
    }

    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Debug.Log("BONB00");
            SetBomb();
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            SetTorch();
        }
    }
}
