﻿Shader "Custom/LightShader"
{
	//변수
	Properties
	{
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
	_Color("Tint", Color) = (1, 1, 1, 1)
		[PerRendererData] PixelSnap("Pixel snap", float) = 0		
		[PerRendererData] _MaskTargetX("Mask Target X", Float) = 0
		[PerRendererData] _MaskTargetY("Mask Target Y", Float) = 0
		[PerRendererData] _RenderDistance("Render Distance", Float) = 1
		[PerRendererData] _DistMinus("Dist Minus", Float) = 0
		[PerRendererData] _MaskType("Mask Type", Float) = 0
	}

		SubShader
	{
		Tags
	{
		"Queue" = "Transparent"
		"IgnoreProjector" = "True"
		"RenderType" = "Transparent"
		"PreviewType" = "Plane"
		"CanUseSpriteAtlas" = "True"
	}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha

		Pass
	{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma multi_compile _ PIXELSNAP_ON
#include "UnityCG.cginc"

		struct appdata_t
		{
			float4 vertex	: POSITION;
			float4 color		: COLOR;
			float2 texcoord : TEXCOORD0;
		};
		
	struct v2f
	{
		float4 vertex	: SV_POSITION;
		fixed4 color : COLOR;
		float2 texcoord : TEXCOORD0;
		fixed4 worldPos : WORLDPOS;
		};

	fixed4 _Color;
	int _TargetCount;
	float _MaskTargetX[30];
	float _MaskTargetY[30];
	float _RenderDistance[30];
	float _DistMinus;
	float _MaskType;

	v2f vert(appdata_t IN)
	{
		v2f OUT;
		OUT.worldPos = mul(unity_ObjectToWorld, fixed4(IN.vertex.x, IN.vertex.y, 0 , 1));
		OUT.vertex = UnityObjectToClipPos(IN.vertex);
		OUT.texcoord = IN.texcoord;
		OUT.color = IN.color * _Color;
#ifdef PIXELSNAP_ON
		OUT.vertex = UnityPixelSnap(OUT.vertex);
#endif

		return OUT;
	}

	sampler2D _MainTex;
	sampler2D _AlphaTex;
	float _AlphaSplitEnabled;

	fixed4 SampleSpriteTexture(float2 uv)
	{
		fixed4 color = tex2D(_MainTex, uv);

#if UNITY_TEXTURE_ALPHASPLIT_ALLOWED
		if (_AlphaSplitEnabled)
			color.a = tex2D(_AlphaTex, uv).r;
#endif

		return color;
	}

	//IN은 오브젝트의 모든 좌표값을 나타냄
	fixed4 frag(v2f IN) : SV_TARGET
	{
		//칼라
		fixed4 c = SampleSpriteTexture(IN.texcoord) * IN.color;
		
	for (int i = 0; i < _TargetCount; i++)
		{
		if (_RenderDistance[i] == 0)
			continue;

		//for (int k = 0; k < 2; k++)
		//{
		//	float dist = distance(IN.worldPos, fixed4(_MaskTargetX[i], _MaskTargetY[i], IN.worldPos.z, 1));
		//	if (_MaskType > 0 && dist <= _RenderDistance[i])
		//	{
		//		c.a = c.a * ((dist) / _RenderDistance[i]);
		//	}

		//	c.rgb *= c.a;
		//}

		//생각하자!!!!!!!!!!!!!!!!!!!!!!!!!!!
		//2차원 - 3차원 계산 무조건 분리해서 생각해라

		for (int k = 0; k < 2; k++)
		{
			float dist = sqrt((IN.worldPos.x - _MaskTargetX[i]) * (IN.worldPos.x - _MaskTargetX[i]) +
				(IN.worldPos.y - _MaskTargetY[i])*(IN.worldPos.y - _MaskTargetY[i]));

			if (dist <= _RenderDistance[i])
			{
				c.a = c.a * ((dist - _DistMinus) / _RenderDistance[i]);
			}

			c.rgb *= c.a;
		}

		
		}
		
		return c;
	}
		ENDCG
	}
	}

}
