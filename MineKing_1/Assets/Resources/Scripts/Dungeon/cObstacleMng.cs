﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cObstacleMng : MonoBehaviour
{
    void Start()
    {
        for (byte i = 0; i < this.transform.childCount; i++)
            this.transform.GetChild(i).GetComponent<cObstacle>().Init();
    }

    void Update()
    {
        for (byte i = 0; i < this.transform.childCount; i++)
        {
            //Off일 때
            if(this.transform.GetChild(i).GetComponent<cObstacle>().isOn.Equals(false))
            {
                this.transform.GetChild(i).GetComponent<cObstacle>().offCoolTimer += Time.deltaTime;

                if(this.transform.GetChild(i).GetComponent<cObstacle>().offCoolTimer >= 
                    this.transform.GetChild(i).GetComponent<cObstacle>().offCoolTime)
                {
                    this.transform.GetChild(i).GetComponent<cObstacle>().offCoolTimer = 0;
                    this.transform.GetChild(i).GetComponent<cObstacle>().ObstacleOn();
                }
            }
            //On일 때
            else
            {
                this.transform.GetChild(i).GetComponent<cObstacle>().onCoolTimer += Time.deltaTime;

                if (this.transform.GetChild(i).GetComponent<cObstacle>().onCoolTimer >=
                    this.transform.GetChild(i).GetComponent<cObstacle>().onCoolTime)
                {
                    this.transform.GetChild(i).GetComponent<cObstacle>().onCoolTimer = 0;
                    this.transform.GetChild(i).GetComponent<cObstacle>().ObstacleOff();
                }
            }
        }
    }
}
