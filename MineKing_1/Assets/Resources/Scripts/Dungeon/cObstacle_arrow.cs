﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cObstacle_arrow : cObstacle
{
    public cObstacle_arrowFlying arrow;
    public Vector3 dir;
    public override void Init()
    {
        base.Init();
        offCoolTime = 2.0f;
        offCoolTimer = 0;
        onCoolTime = 3.0f;
        onCoolTimer = 0;
        hitCoolTime = 1.0f;
        hitCoolTimer = 0;
        isAttached = true;
    }
    
    public void InitArrow()
    {
        arrow.Init();
        arrow.SetDir(dir);
        arrow.gameObject.SetActive(true);
    }

}
