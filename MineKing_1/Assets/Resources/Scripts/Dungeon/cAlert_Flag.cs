﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cAlert_Flag : MonoBehaviour
{
    public cFlag flag;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))
        {
            // 점령상태 아닐때만 알림 UI 띄움
            if (cUtil._player.isFlagDone.Equals(false))
                flag.dp.ActiveFlagAlert();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))
        {
            flag.dp.InActiveFlagAlert();
        }
    }
}
