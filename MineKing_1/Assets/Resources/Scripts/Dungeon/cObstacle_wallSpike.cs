﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cObstacle_wallSpike : cObstacle
{
    private long damage;

    IEnumerator Hit;
    private bool isCorOn;

    public override void Init()
    {
        base.Init();
        offCoolTime = 2.0f;
        offCoolTimer = 0;
        onCoolTime = 3.0f;
        onCoolTimer = 0;
        hitCoolTime = 1.0f;
        hitCoolTimer = 0;
        damage = 1;
    }
           
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))
        {
            isPlayerIn = true;

            if(isOn.Equals(true) && isCorOn.Equals(false))
            {
                cUtil._player.ReduceHp(damage,Vector3.zero);
                hitCoolTimer = 0;
                Hit = Hit_cor();
                StartCoroutine(Hit);
            }
        }
    }
    public override void AlreadyExistCheck()
    {
        if(isPlayerIn.Equals(true) && isOn.Equals(true))
        {
            if (isCorOn.Equals(false))
            {
                cUtil._player.ReduceHp(damage,Vector3.zero);
                hitCoolTimer = 0;
                Hit = Hit_cor();
                StartCoroutine(Hit);
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))
        {
            isPlayerIn = false;
        }
    }

    IEnumerator Hit_cor()
    {
        isCorOn = true;

        while (true)
        {
            yield return null;

            if (isOn.Equals(false) || isPlayerIn.Equals(false))
            {
                hitCoolTimer = 0;
                isCorOn = false;
                break;
            }

            if(isPlayerIn.Equals(true) && isOn.Equals(true))
            {
                hitCoolTimer += Time.deltaTime;

                if(hitCoolTimer >= hitCoolTime)
                {
                    cUtil._player.ReduceHp(damage,Vector3.zero);
                    hitCoolTimer = 0.01f;
                }
            }
        }
    }
}
