﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cObstacle_arrowFlying : cProjectile
{
    public long damage;
    public Vector3 defaultPos;

    public override void Init()
    {
        flyingTime = 0;
        defaultPower = 12f;
        changingPower = defaultPower;
        isReflectOn = false;
        isGravityOn = false;
        gravityAmount = 0;
        damage = 2;
        defaultPos = this.transform.position;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag.Equals("Player"))
        {
            cUtil._player.ReduceHp(damage,Vector3.zero);
            this.gameObject.SetActive(false);
            this.transform.position = defaultPos;
        }

        if(collision.tag.Equals("Tile_canHit") || collision.tag.Equals("Tile_cannotHit"))
        {
            this.gameObject.SetActive(false);
            this.transform.position = defaultPos;
        }
    }
}
