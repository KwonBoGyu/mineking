﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cLight : MonoBehaviour
{
    protected float lightDist;
    protected bool isInit;
    protected int idx;

    public virtual void ChangeLightDist(float pDist)
    {
        cUtil._lightMng.ChangeLight(idx, pDist);
    }

    public virtual void SetLightRange(float pValue)
    {
        lightDist = 150 + pValue;

        idx = cUtil._lightMng.SetLight(this.gameObject, lightDist);

        isInit = true;
    }

    public virtual void RemoveLight()
    {
        cUtil._lightMng.RemoveLight(idx);
        isInit = false;
    }
}
