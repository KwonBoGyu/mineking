﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class cBtn_dash : MonoBehaviour, IPointerDownHandler
{
    public cPlayer scr_player;
    public bool isButtonEnable { get; set; }

    public void Init()
    {
        isButtonEnable = true;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if(isButtonEnable)
        {
            Dash();
        }
    }

    private void Dash()
    {
        if (scr_player.GetIsClimbing().Equals(true))
            return;

        // 쿨다운이 다 찼을때 대쉬 발동
        if (scr_player.GetStatus() != CHARACTERSTATUS.ATTACK &&
            scr_player.GetDashCoolDown() == scr_player.GetMaxDashCoolDown())
        {
            scr_player.StartCoroutine("Dash");
            scr_player.StartCoroutine("DashCoolDown");
            scr_player.sm.playEffect(9);
        }
    }
}
