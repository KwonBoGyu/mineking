﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cCamera : MonoBehaviour
{
    public float bossPlayerPosY;

    public Transform trans_nextGate;
    private bool stopPlayerCam;

    public GameObject _player;
    public Camera cam;
    public Image canvas;
    public Image moveArea;
    private RectTransform camRt;

    private Vector4 camRange; // 현재 카메라가 비추는 범위
    private Vector4 minMax_entireRange; // 맵 전체 범위
    private Vector4 minMax_moveRange; // 카메라 고정 범위

    private float moveRange_width;
    private float moveRange_height;

    // x,y축 잠금 여부
    bool isXLocked;
    bool isYLocked;

    // 최종 카메라 x,y 포지션
    float xFixedPos;
    float yFixedPos;

    float cam_height;
    float cam_width;

    public SpriteRenderer fog_forPlayerBlind;

    public byte sceneNum;

    private WaitForFixedUpdate wf = new WaitForFixedUpdate();

    void Start()
    {
        RectTransform bt = canvas.GetComponent<RectTransform>();
        minMax_entireRange = new Vector4();
        minMax_entireRange.x = bt.position.y + bt.sizeDelta.y * 0.5f;
        minMax_entireRange.y = bt.position.x + bt.sizeDelta.x * 0.5f;
        minMax_entireRange.z = bt.position.y - bt.sizeDelta.y * 0.5f;
        minMax_entireRange.w = bt.position.x - bt.sizeDelta.x * 0.5f;

        RectTransform mv = moveArea.GetComponent<RectTransform>();
        moveRange_width = mv.sizeDelta.x;
        moveRange_height = mv.sizeDelta.y + bossPlayerPosY;

        camRt = this.GetComponent<RectTransform>();

        cam_height = 2 * cam.orthographicSize;
        cam_width = cam_height * cam.aspect;
    }

    void FixedUpdate()
    {
        if(sceneNum.Equals(2))
        {
            yFixedPos = Mathf.Lerp(this.transform.position.y, _player.transform.position.y, 0.2f);
            xFixedPos = Mathf.Lerp(this.transform.position.x, _player.transform.position.x, 0.2f);
            this.gameObject.transform.position = new Vector3(xFixedPos, yFixedPos, -9.5f);
        }

        if (stopPlayerCam.Equals(false))
        {
            // 카메라 경계 실시간 위치값
            camRange.x = cam.ViewportToWorldPoint(new Vector2(0, 1)).y;
            camRange.y = cam.ViewportToWorldPoint(new Vector2(1, 1)).x;
            camRange.z = cam.ViewportToWorldPoint(new Vector2(1, 0)).y;
            camRange.w = cam.ViewportToWorldPoint(new Vector2(0, 0)).x;

            // 플레이어 팔로우
            // X방향만 잠긴 경우
            if (isXLocked.Equals(true) && isYLocked.Equals(false))
            {
                // 카메라 고정 범위 밖으로 플레이어 이동
                if (Vector3.Distance(_player.transform.position, moveArea.transform.position) > moveRange_width * 0.5f)
                {
                    yFixedPos = Mathf.Lerp(this.gameObject.transform.position.y, _player.transform.position.y, 0.2f);
                }
                // 이동 범위 안인 경우 고정
                else
                {
                    yFixedPos = Mathf.Lerp(this.transform.position.y, _player.transform.position.y, 0.2f);
                }
            }
            // Y방향만 잠긴 경우
            else if (isXLocked.Equals(false) && isYLocked.Equals(true))
            {
                // 카메라 고정 범위 밖으로 플레이어 이동
                if (Vector3.Distance(_player.transform.position, moveArea.transform.position) > moveRange_width * 0.5f)
                {
                    // 오른쪽으로 플레이어 이동중인 경우
                    if (_player.transform.position.x >= this.transform.position.x)
                    {
                        xFixedPos = Mathf.Lerp(this.transform.position.x, _player.transform.position.x - moveRange_width * 0.5f, 0.2f);
                    }
                    // 왼쪽으로 플레이어 이동중인 경우
                    else
                    {
                        xFixedPos = Mathf.Lerp(this.transform.position.x, _player.transform.position.x + moveRange_width * 0.5f, 0.2f);
                    }
                }
                // 이동 범위 안인 경우
                else
                {
                    xFixedPos = this.gameObject.transform.position.x;
                }
            }
            // 두 방향 모두 잠기지 않은 경우
            else if (isXLocked.Equals(false) && isYLocked.Equals(false))
            {
                // 카메라 고정 범위 밖으로 플레이어 이동
                if (Vector2.Distance(new Vector3(_player.transform.position.x, _player.transform.position.y + bossPlayerPosY, _player.transform.position.z),
                    moveArea.transform.position) > moveRange_width * 0.5f)
                {
                    // 오른쪽
                    if (_player.transform.position.x >= this.transform.position.x)
                    {
                        xFixedPos = Mathf.Lerp(this.transform.position.x, _player.transform.position.x - moveRange_width * 0.5f, 0.2f);
                    }
                    // 왼쪽
                    else
                    {
                        xFixedPos = Mathf.Lerp(this.transform.position.x, _player.transform.position.x + moveRange_width * 0.5f, 0.2f);
                    }
                    yFixedPos = Mathf.Lerp(this.transform.position.y, _player.transform.position.y + bossPlayerPosY, 0.2f);
                }
                // 이동 범위 안
                else
                {
                    xFixedPos = this.gameObject.transform.position.x;
                    yFixedPos = Mathf.Lerp(this.transform.position.y, _player.transform.position.y + bossPlayerPosY, 0.2f);
                }
            }
            this.gameObject.transform.position = new Vector3(xFixedPos, yFixedPos, -9.5f);
        }

    }

    public void ShowNextGate()
    {
        StartCoroutine(Cor_ShowNextGate());
    }

    IEnumerator Cor_ShowNextGate()
    {
        yield return new WaitForSeconds(5.0f);

        stopPlayerCam = true;
        Vector2 dir = (trans_nextGate.position - this.transform.position).normalized;

        float speed = 6000;

        while (true)
        {
            yield return new WaitForFixedUpdate();

            float dist = Vector2.Distance(this.transform.position, trans_nextGate.position);

            //도착하였다면..
            if (dist < 200)
            {
                trans_nextGate.GetComponent<cGate>().OpenAnimation();
                this.transform.position = new Vector3(trans_nextGate.position.x, trans_nextGate.position.y, this.transform.position.z);

                yield return new WaitForSeconds(2.0f);

                trans_nextGate.GetComponent<cGate>().canUse = true;
                stopPlayerCam = false;

                break;
            }

            this.transform.Translate(dir * speed * Time.deltaTime);
        }
    }

    public void Blind(float pWaitTime)
    {
        StartCoroutine(Blind_cor(pWaitTime));
    }

    IEnumerator Blind_cor(float pWaitTime)
    {
        fog_forPlayerBlind.color = new Color(fog_forPlayerBlind.color.r, fog_forPlayerBlind.color.g, fog_forPlayerBlind.color.b, 1.0f);

        yield return new WaitForSeconds(pWaitTime);

        float alpha = 1.0f;

        while (fog_forPlayerBlind.color.a > 0f)
        {
            yield return null;

            alpha -= 1.0f * Time.deltaTime;
            if (alpha <= 0f)
            {
                alpha = 0f;
            }

            fog_forPlayerBlind.color = new Color(fog_forPlayerBlind.color.r, fog_forPlayerBlind.color.g, fog_forPlayerBlind.color.b, alpha);
        }
        yield return new WaitForSeconds(0.1f);
    }

    public void CameraShake(float pAmount, float pDuration)
    {
        StartCoroutine(Shake(pAmount, pDuration));
    }

    IEnumerator Shake(float pAmount, float pDuration)
    {
        Vector3 originPos = this.gameObject.transform.localPosition;

        float timer = 0;
        while (timer <= pDuration)
        {
            transform.localPosition = (Vector3)Random.insideUnitCircle * pAmount + originPos;

            timer += Time.deltaTime;
            yield return wf;
        }
        transform.localPosition = originPos;
    }
}