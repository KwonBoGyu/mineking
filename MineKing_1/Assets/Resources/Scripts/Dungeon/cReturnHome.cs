﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cReturnHome : MonoBehaviour
{
    public Button[] b_buttons;

    void Start()
    {
        b_buttons[0].onClick.AddListener(() => ReturnHome());
        b_buttons[1].onClick.AddListener(() => ExitPopUp());
        b_buttons[2].onClick.AddListener(() => ExitPopUp());
    }

    private void ReturnHome()
    {
        //클리어하지 않았는데, 점령지만 점령한 상태에서 집을 간다라..
        if(cUtil._user.GetFlaged((byte)(cUtil._sm._scene - 3)).Equals(true) && 
            cUtil._user.GetMaxClearedStage() <(byte)(cUtil._sm._scene - 2))
        {
            //바로 초기화죠?
            cUtil._user.SetFlaged((byte)(cUtil._sm._scene - 3), false);
        }

        cUtil._sm.ChangeScene("Main");
        this.gameObject.SetActive(false);
    }
    private void ExitPopUp()
    {
        this.gameObject.SetActive(false);
    }
}
