﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class cDungeonNormal_processor : MonoBehaviour
{
    public bool isSkin;

    //교체시 초기화 해줄것들
    public cTileMng chg_tileMng;
    public GameObject chg_obj_coolTime;
    public Text chg_t_coolTime;
    public Image chg_img_curHp;
    public Text chg_t_hp;
    public Image chg_img_curDp;
    public Text chg_t_dp;
    public cSoundMng chg_sm;
    public GameObject chg_indicator;
    public Sprite img_nextIndicator;
    public cFloatingText chg_floatingText;
    public cUseManager chg_useMng;
    public cJoystick chg_joystick;
    public cBtn_item chg_quickSlot;
    public Transform chg_flagPos;
    public cFlag chg_Flag;
    public Transform nextDoorPos;
    public cGate chg_nextDoor;
    public cGate chg_bossDoor;
    //교체시 넣어줘야 할것들
    public cDungeonNormal_processor chg_dp;
    public cCamera chg_camera;
    public cBtn_attack chg_attack;
    public cBtn_jump chg_jump;
    public cBtn_dash chg_dash;
    public cHeadLight chg_light;

    public GameObject playerParent;
    public GameObject _player;
    public GameObject _enemy;
    private cPlayer _p;
    private cEnemy_monster[] enemy;
    public GameObject _enemyPool;
    public GameObject _hintObj;
    private string s_keymonsterHint;

    public Button b_bag;
    public Button[] b_exitBag;
    public cBag _bag;

    public GameObject obj_values;

    public Button b_goHome;
    public GameObject obj_returnHome;
    public cTileMng tileMng;

    //보스전일 때
    public cBoss_theme1_stage1 boss_slime;
    public cBoss_theme1_stage2 boss_skeleton;
    public cBoss_theme1_stage3 boss_goblin;
    public cBoss_theme1_stage5 boss_devil;

    //기본 횃불 세팅 부모
    public GameObject torchParent;

    //보상 ui
    public GameObject rewardItemParent;
    private const int rewardFactor = 1000;
    private cProperty[] rewardValues;
    public Sprite[] soulSprites;

    //키스톤
    public GameObject keystone;

    //안개
    public SpriteMaskScript scr_fog;

    //아래 팝업 프레임
    public Animator animator_bottomFrame;
    public byte curGateChar; //0 : 이전, 1 : 보스, 2 : 다음

    //적색 프레임
    public GameObject img_lowHp;

    //몬스터 이닛 여부
    private bool isMonsterInited;

    //현재 스테이지 텍스트
    public Text t_curStage;
    private int curSceneNum;

    //필드 던전    
    private void Start()
    {
        cUtil._soundMng = chg_sm;
        isMonsterInited = false;
        curSceneNum = (int)cUtil._sm._scene;

        if (curSceneNum != 2)
        {
            //라이트 매니저
            cUtil._lightMng = scr_fog;

            //일반 스테이지
            if (curSceneNum > 2 && curSceneNum < 15)
            {
                InitDungeon();
                t_curStage.text = string.Format("지하 {0}층", curSceneNum - 2);
            }
            //보스
            else
            {
                InitDungeon(true);
            }
        }
    }

    //형상변환 ui
    public void Init()
    {
        _p = _player.transform.GetChild(0).GetComponent<cPlayer>();
        InitDungeon_skin();
    }

    public void InitDungeon(bool isBoss = false)
    {
        byte curWeaponId = cUtil._user.GetCurWeaponId();
        byte curClothId = cUtil._user.GetCurClothId();

        _player = Instantiate(Resources.Load<GameObject>("Prefabs/Skin_cloth/Player_skin_" + curClothId.ToString()));
        _player.transform.SetParent(playerParent.transform);
        _player.transform.localScale = new Vector3(1, 1, 1);
        _player.transform.position = new Vector3(playerParent.transform.position.x, playerParent.transform.position.y, 0.5f);
        _p = _player.transform.GetChild(0).GetComponent<cPlayer>();
        _p.img_curHp = chg_img_curHp;
        _p.t_hp = chg_t_hp;
        _p.img_curDp = chg_img_curDp;
        _p.t_dp = chg_t_dp;
        chg_light.HeadLight = _p.lightPos;
        _p.Init(cUtil._user.GetNickName(),
            cUtil._user.GetWeaponInfo().damage,
            250.0f,
            cUtil._user.GetWeaponInfo().hp,
            cUtil._user.GetWeaponInfo().hp);
        cUtil._player = _p;
        
        chg_tileMng.Init();
        _p.tileMng = chg_tileMng;
        _p.quickSlot = chg_quickSlot;
        chg_quickSlot.Init();
        _p.obj_coolTime = chg_obj_coolTime;
        _p.t_coolTime = chg_t_coolTime;
        _p.sm = chg_sm;
        _p.ft = chg_floatingText;
        chg_floatingText.Init();
        _p.joystick = chg_joystick;
        chg_joystick._player = _player.transform;
        chg_joystick.Init();
        _p.dp = chg_dp;
        chg_attack.scr_player = _p;
        chg_attack.Init();
        chg_jump.scr_player = _p;
        chg_dash.scr_player = _p;
        chg_camera._player = _player;
        chg_jump.Init();
        chg_dash.Init();

        //일반 던전
        //플레이어 초기화
        if (isBoss.Equals(false))
        {
            _p.nextDoorPos = nextDoorPos;
            _p.flagPos = chg_flagPos;
            chg_Flag.dp = this;
            chg_Flag.Init();
            _p.indicator = chg_indicator;
            _p.useMng = chg_useMng;
            
            //ui 초기화
            b_bag.onClick.AddListener(() => OpenBag());
            for (byte i = 0; i < b_exitBag.Length; i++)
                b_exitBag[i].onClick.AddListener(() => ExitBag());

            b_goHome.onClick.AddListener(() => GoHome());
            SetEnemySpriteOrder();

            // 점령지,게이트 버튼 연결 ( 던전UI 매니저 만들기 전까지 임시)
            animator_bottomFrame.transform.GetChild(0).GetChild(0).GetComponent<Button>().onClick.AddListener(() => OnGateButtonClicked());
            animator_bottomFrame.transform.GetChild(1).GetChild(0).GetComponent<Button>().onClick.AddListener(() => OnFlagButtonClicked(true));

        }
        //보스전
        else
        {
            //횃불 세팅
            for (byte i = 0; i < torchParent.transform.childCount; i++)
            {
                torchParent.transform.GetChild(i).GetComponent<cLight>().SetLightRange(400);
            }

            _p.useMng = chg_useMng;
            chg_light.HeadLight = _p.lightPos;
            chg_camera._player = _player;
            chg_camera.bossPlayerPosY = 200;

            //씬별로 보스 다르게 초기화해야함
            switch (curSceneNum)
            {
                case 15:
                    boss_slime.InitBoss();
                    boss_slime.dp = this;
                    break;
                case 16:
                    boss_skeleton.InitBoss();
                    boss_skeleton.dp = this;
                    break;
                case 17:
                    boss_goblin.InitBoss();
                    boss_goblin.dp = this;
                    break;
                case 18:
                    boss_devil.InitBoss();
                    boss_devil.dp = this;
                    break;
            }

            rewardValues = new cProperty[3];
            for (byte i = 0; i < rewardValues.Length; i++)
                rewardValues[i] = new cProperty("ADsf", 0);
        }

        UpdateValue();
    }

    public void InitDungeon_skin()
    {
        _p.Init(cUtil._user.GetNickName(),
             cUtil._user.GetWeaponInfo().damage,
             250.0f,
             cUtil._user.GetWeaponInfo().hp,
             cUtil._user.GetWeaponInfo().hp);
        cUtil._player = _p;

        chg_tileMng.Init();
        _p.tileMng = chg_tileMng;
        _p.obj_coolTime = chg_obj_coolTime;
        _p.t_coolTime = chg_t_coolTime;
        _p.sm = chg_sm;
        _p.ft = chg_floatingText;
        chg_floatingText.Init();
        _p.joystick = chg_joystick;
        chg_joystick._player = _player.transform;
        chg_joystick.Init();
        chg_attack.scr_player = _p;
        chg_attack.Init();
        chg_jump.scr_player = _p;
        chg_dash.scr_player = _p;
        chg_camera.sceneNum = 2;
        chg_jump.Init();
        chg_dash.Init();

        //ui 초기화
        b_bag.onClick.AddListener(() => OpenBag());
        for (byte i = 0; i < b_exitBag.Length; i++)
            b_exitBag[i].onClick.AddListener(() => ExitBag());

        b_goHome.onClick.AddListener(() => GoHome());
        UpdateValue();
    }

    private void BossVictory()
    {
        //보스 잡았음
        cUtil._user.SetBossDone((byte)(curSceneNum - cUtil._sm.maxSceneNum.max_dungeonNormal), true);

        //영혼석
        rewardValues[0].value = Random.Range((int)0, (int)10);
        //확률 : 30%
        if (rewardValues[0].value < 3)
            rewardValues[0].value = 1;
        else
            rewardValues[0].value = 0;
        //다이아
        rewardValues[1].value = Random.Range((int)0, (int)11);
        //광석
        rewardValues[2].value = Random.Range((int)1, (int)5) * rewardFactor * (curSceneNum - 14);

        for (byte i = 0; i < rewardValues.Length; i++)
        {
            if (rewardValues[i].value.Equals(0))
            {
                rewardItemParent.transform.GetChild(i).gameObject.SetActive(false);
                continue;
            }
            //보상 아이템이 있으면..
            else
            {
                rewardItemParent.transform.GetChild(i).gameObject.SetActive(true);

                //아이템 지급
                switch (i)
                {
                    case 0: cUtil._user.GetInventory().GetSoul()[curSceneNum - cUtil._sm.maxSceneNum.max_dungeonNormal].value += rewardValues[0].value; break;
                    case 1: cUtil._user.GetInventory().GetDia().value += rewardValues[1].value; break;
                    case 2: cUtil._user.GetInventory().GetRock().value += rewardValues[2].value; break;
                }
            }

            //영혼석
            if (i.Equals(0))
                rewardItemParent.transform.GetChild(i).GetChild(0).GetComponent<Image>().sprite = soulSprites[curSceneNum - 15];
            //나머지
            rewardItemParent.transform.GetChild(i).GetChild(1).GetComponent<Text>().text = cUtil.GetValueToString(rewardValues[i].value);
        }

        //확인버튼
        rewardItemParent.transform.parent.GetChild(4).GetComponent<Button>().onClick.AddListener(() => CloseVictoryUi());
        rewardItemParent.transform.parent.gameObject.SetActive(true);
    }

    public void StartBossVictory() { StartCoroutine("BossVictory_cor"); }
    IEnumerator BossVictory_cor()
    {
        yield return new WaitForSeconds(3.0f);

        BossVictory();
    }
    private void CloseVictoryUi()
    {
        UpdateValue();
        rewardItemParent.transform.parent.gameObject.SetActive(false);
        //밖으로
        cUtil._sm.GoToBossGate(false);
    }

    private void OpenBag()
    {
        chg_sm.PlayBag(true);
        _bag.OpenBag();
    }
    private void ExitBag()
    {
        chg_sm.PlayBag(false);
        _bag.obj_content.SetActive(false);
    }

    private void GoHome()
    {
        if (obj_returnHome.activeSelf.Equals(false))
            obj_returnHome.SetActive(true);
    }

    public void UpdateValue()
    {
        obj_values.transform.GetChild(0).GetChild(0).GetComponent<Text>().text =
                cUtil.GetValueToString(cUtil._user.GetInventory().GetMoney().value);
        obj_values.transform.GetChild(1).GetChild(0).GetComponent<Text>().text =
        cUtil.GetValueToString(cUtil._user.GetInventory().GetRock().value);
        obj_values.transform.GetChild(2).GetChild(0).GetComponent<Text>().text =
        cUtil.GetValueToString(cUtil._user.GetInventory().GetDia().value);
    }

    private void SetEnemySpriteOrder()
    {
        int enemyNum = _enemyPool.transform.childCount;
        int eachSpriteNum = 10;

        for (int i = 0; i < enemyNum; i++)
        {
            GameObject enemySprite = _enemyPool.transform.GetChild(i).transform.GetChild(0).transform.GetChild(0).gameObject;

            for (int j = 0; j < enemySprite.transform.childCount; j++)
            {
                enemySprite.transform.GetChild(j).GetComponent<Anima2D.SpriteMeshInstance>().sortingOrder += 100 + i * eachSpriteNum;
            }

            //몬스터 밸런스 조절
            //switch(curSceneNum)
            //{
            //    case 6:
            //        _enemyPool.transform.GetChild(i).transform.GetChild(0).GetComponent<cEnemy_monster>().modifyInfo(2500, 5);
            //        break;
            //    case 7:
            //        _enemyPool.transform.GetChild(i).transform.GetChild(0).GetComponent<cEnemy_monster>().modifyInfo(10000, 5);
            //        break;
            //    case 8:
            //        _enemyPool.transform.GetChild(i).transform.GetChild(0).GetComponent<cEnemy_monster>().modifyInfo(62500, 10);
            //        break;
            //    case 9:
            //        _enemyPool.transform.GetChild(i).transform.GetChild(0).GetComponent<cEnemy_monster>().modifyInfo(312500, 10);
            //        break;
            //    case 10:
            //        _enemyPool.transform.GetChild(i).transform.GetChild(0).GetComponent<cEnemy_monster>().modifyInfo(1562500, 10);
            //        break;
            //    case 11:
            //        _enemyPool.transform.GetChild(i).transform.GetChild(0).GetComponent<cEnemy_monster>().modifyInfo(7812500, 10);
            //        break;
            //    case 12:
            //        _enemyPool.transform.GetChild(i).transform.GetChild(0).GetComponent<cEnemy_monster>().modifyInfo(39062500, 10);
            //        break;
            //    case 13:
            //        _enemyPool.transform.GetChild(i).transform.GetChild(0).GetComponent<cEnemy_monster>().modifyInfo(195312500, 15);
            //        break;
            //    case 14:
            //        _enemyPool.transform.GetChild(i).transform.GetChild(0).GetComponent<cEnemy_monster>().modifyInfo(976562500, 15);
            //        break;
            //    case 15:
            //        _enemyPool.transform.GetChild(i).transform.GetChild(0).GetComponent<cEnemy_monster>().modifyInfo(4882812500, 15);
            //        break;
            //    case 16:
            //        _enemyPool.transform.GetChild(i).transform.GetChild(0).GetComponent<cEnemy_monster>().modifyInfo(24414062500, 15);
            //        break;
            //    case 17:
            //        _enemyPool.transform.GetChild(i).transform.GetChild(0).GetComponent<cEnemy_monster>().modifyInfo(122070312500, 15);
            //        break;
            //    case 18:
            //        _enemyPool.transform.GetChild(i).transform.GetChild(0).GetComponent<cEnemy_monster>().modifyInfo(549316406264, 20);
            //        break;
            //    case 19:
            //        _enemyPool.transform.GetChild(i).transform.GetChild(0).GetComponent<cEnemy_monster>().modifyInfo(2471923828188, 20);
            //        break;
            //    case 20:
            //        _enemyPool.transform.GetChild(i).transform.GetChild(0).GetComponent<cEnemy_monster>().modifyInfo(11123657226860, 20);
            //        break;
            //    case 21:
            //        _enemyPool.transform.GetChild(i).transform.GetChild(0).GetComponent<cEnemy_monster>().modifyInfo(50056457520884, 20);
            //        break;
            //    case 22:
            //        _enemyPool.transform.GetChild(i).transform.GetChild(0).GetComponent<cEnemy_monster>().modifyInfo(350395202646188, 20);
            //        break;
            //}
        }
    }

    private void SetEnemyInfo()
    {

    }

    //키 몬스터 힌트 이벤트
    public void SetKeyMonster()
    {
        //키 몬스터 id
        int tempKeyMonsterId = Random.Range((int)0, (int)_enemyPool.transform.childCount);
        _enemyPool.transform.GetChild(tempKeyMonsterId).GetChild(0).GetComponent<cEnemy_monster>().isKeyMonster = true;
        _enemyPool.transform.GetChild(tempKeyMonsterId).GetChild(0).GetComponent<cEnemy_monster>().dp = this;
        tempKeyMonsterId = _enemyPool.transform.GetChild(tempKeyMonsterId).GetChild(0).GetComponent<cEnemy_monster>().id;

        switch (tempKeyMonsterId)
        {
            //작골
            case 0:
                s_keymonsterHint = "바위들이 살아움직인다면 얼마나 귀여울까?";
                break;
            //슬라임
            case 1:
                s_keymonsterHint = "저를 처치하고 싶다구요? 제 뱃속에 아이가 둘인데ㅠㅠ";
                break;
        }

        _hintObj.transform.GetChild(1).GetComponent<Text>().text = s_keymonsterHint;
        _hintObj.SetActive(true);

        //사운드

    }

    public void PresetKeystone()
    {
        keystone.SetActive(true);
        keystone.GetComponent<Animator>().SetTrigger("Exist");
        chg_bossDoor.canUse = true;
    }

    public void GetKeystone()
    {
        switch ((int)cUtil._sm._scene)
        {
            case 5: cUtil._user.GetInventory().AddItem(2, 50); chg_bossDoor.canUse = true; break;
            case 8: cUtil._user.GetInventory().AddItem(2, 51); chg_bossDoor.canUse = true; break;
            case 11: cUtil._user.GetInventory().AddItem(2, 52); chg_bossDoor.canUse = true; break;
            case 14: cUtil._user.GetInventory().AddItem(2, 53); chg_bossDoor.canUse = true; break;
        }

        keystone.SetActive(true);
        keystone.GetComponent<Animator>().SetTrigger("GetKeystone");
        _hintObj.SetActive(false);
        chg_bossDoor.canUse = true;
    }

    // 컨트롤러 작동 On,Off
    public void DisableController()
    {
        chg_joystick.SetIsJoystickEnable(false);
        chg_attack.isButtonEnable = false;
        chg_jump.isButtonEnable = false;
        chg_dash.isButtonEnable = false;
        chg_quickSlot.isButtonEnable = false;
    }
    public void EnableController()
    {
        chg_joystick.SetIsJoystickEnable(true);
        chg_attack.isButtonEnable = true;
        chg_jump.isButtonEnable = true;
        chg_dash.isButtonEnable = true;
        chg_quickSlot.isButtonEnable = true;
    }

    public void ActiveFlagAlert()
    {
        animator_bottomFrame.SetBool("FlagOff", false);
        animator_bottomFrame.SetTrigger("FlagOn");
    }
    public void InActiveFlagAlert()
    {
        animator_bottomFrame.SetBool("FlagOn", false);
        animator_bottomFrame.SetTrigger("FlagOff");
    }
    public void ActiveGateAlert(byte pCurGateChar)
    {
        curGateChar = pCurGateChar;
        animator_bottomFrame.SetBool("GateOff", false);
        animator_bottomFrame.SetTrigger("GateOn");

    }
    public void InActiveGateAlert()
    {
        animator_bottomFrame.SetBool("GateOn", false);
        animator_bottomFrame.SetTrigger("GateOff");
    }

    private void OnFlagButtonClicked(bool pBool)
    {
        if (pBool)
        {
            chg_Flag.FlagOn(0);
            InActiveFlagAlert();
            _p.indicator.GetComponent<Image>().sprite = img_nextIndicator;
            _p.indicator.GetComponent<Image>().SetNativeSize();
        }
        else
        {
            InActiveFlagAlert();
        }
    }

    private void OnGateButtonClicked()
    {
        switch(curGateChar)
        {
            case 1:
                chg_bossDoor.OpenGate();
                break;

            case 2:
                chg_nextDoor.OpenGate();
                break;
        }
    }
}
