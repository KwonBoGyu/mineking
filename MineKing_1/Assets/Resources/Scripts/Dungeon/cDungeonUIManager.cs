﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cDungeonUIManager : MonoBehaviour
{
    private cSoundMng sm;

    private cFlag flag;
    private cGate nextDoor;

    public GameObject flagAlertObj;
    public Button b_flagAlert;
    public GameObject gateAlertObj;
    public Button b_gateAlert;

    public Button b_bag;
    public Button[] b_exitBag;
    public cBag _bag;

    public Button b_goHome;

    public GameObject obj_values;

    //보상 ui
    public GameObject rewardItemParent;
    private const int rewardFactor = 1000;
    private cProperty[] rewardValues;
    public Sprite[] soulSprites;

    public void Init_forNormal(cFlag pFlag, cGate pNextDoor)
    {
        sm = cUtil._soundMng;

        b_bag.onClick.AddListener(() => OpenBag());
        for (byte i = 0; i < b_exitBag.Length; i++)
            b_exitBag[i].onClick.AddListener(() => ExitBag());

        b_goHome.onClick.AddListener(() => GoHome());

        flag = pFlag;
        nextDoor = pNextDoor;

        b_flagAlert.onClick.AddListener(() => OnFlagButtonClicked());
        b_gateAlert.onClick.AddListener(() => OnGateButtonClicked());
    }

    public void Init_forBoss(GameObject pRewardItemParent, cProperty[] pRewardValues, Sprite[] pSoulSprites)
    {
        rewardItemParent = pRewardItemParent;
        rewardValues = pRewardValues;
        soulSprites = pSoulSprites;
        
        sm = cUtil._soundMng;

        b_bag.onClick.AddListener(() => OpenBag());
        for (byte i = 0; i < b_exitBag.Length; i++)
            b_exitBag[i].onClick.AddListener(() => ExitBag());

        b_goHome.onClick.AddListener(() => GoHome());
    }

    public void Init_forSkin()
    {
        sm = cUtil._soundMng;

        b_bag.onClick.AddListener(() => OpenBag());
        for (byte i = 0; i < b_exitBag.Length; i++)
            b_exitBag[i].onClick.AddListener(() => ExitBag());

        b_goHome.onClick.AddListener(() => GoHome());
    }

    private void OpenBag()
    {
        sm.PlayBag(true);
        _bag.OpenBag();
    }
    private void ExitBag()
    {
        sm.PlayBag(false);
        _bag.obj_content.SetActive(false);
    }
    private void GoHome() { cUtil._sm.ChangeScene("Main"); }

    private void OnFlagButtonClicked()
    {
        flag.FlagOn(0);
        InActiveFlagAlert();
    }
    private void OnGateButtonClicked()
    {
        ActiveGateAlert();
        Debug.Log(nextDoor.gameObject.name);
        if (nextDoor.gameObject.tag.Equals("Gate_boss"))
        {
            if (nextDoor.isIn.Equals(false))
                nextDoor.OpenGate();
            else
            {
                switch ((int)cUtil._sm._scene)
                {
                    case 5:
                        if (cUtil._user.GetInventory().IsItemExist(2, 50))
                            nextDoor.OpenAnimation();
                        break;
                    case 8:
                        if (cUtil._user.GetInventory().IsItemExist(2, 51))
                            nextDoor.OpenAnimation();
                        break;
                    case 11:
                        if (cUtil._user.GetInventory().IsItemExist(2, 52))
                            nextDoor.OpenAnimation();
                        break;
                    case 14:
                        if (cUtil._user.GetInventory().IsItemExist(2, 53))
                            nextDoor.OpenAnimation();
                        break;
                }
            }
        }
        else if (nextDoor.gameObject.tag.Equals("Gate_normal"))
        {
            nextDoor.OpenGate();
        }
    }

    public void ActiveFlagAlert()
    {
        flagAlertObj.gameObject.SetActive(true);
    }
    public void InActiveFlagAlert()
    {
        flagAlertObj.gameObject.SetActive(false);
    }
    public void ActiveGateAlert()
    {
        gateAlertObj.gameObject.SetActive(true);
    }
    public void InActiveGateAlert()
    {
        gateAlertObj.gameObject.SetActive(false);
    }

    public void UpdateValue()
    {
        obj_values.transform.GetChild(0).GetChild(0).GetComponent<Text>().text =
                cUtil.GetValueToString(cUtil._user.GetInventory().GetMoney().value);
        obj_values.transform.GetChild(1).GetChild(0).GetComponent<Text>().text =
        cUtil.GetValueToString(cUtil._user.GetInventory().GetRock().value); 
        obj_values.transform.GetChild(2).GetChild(0).GetComponent<Text>().text =
        cUtil.GetValueToString(cUtil._user.GetInventory().GetDia().value);
    }
}
