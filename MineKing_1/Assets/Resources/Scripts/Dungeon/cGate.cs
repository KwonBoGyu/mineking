﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cGate : MonoBehaviour
{
    public bool isStage; //true : 스테이지, false: 보스
    public bool isIn;
    public bool canUse;
    private cLight lightObj;
    private cDungeonNormal_processor dp;
    public Sprite[] img_gate;

    public virtual void Init()
    {
        lightObj = this.transform.Find("Light").GetComponent<cLight>();
        lightObj.SetLightRange(0);
    }

    public void OpenGate()
    {
        if (canUse.Equals(false))
            return;

        if (isStage.Equals(true))
        {
            cUtil._sm.GoToStageGate(isIn);
            
            //스테이지 클리어 확인
            if (isIn.Equals(true))
            {
                cUtil._user.PlusMaxClearedStage();
            }
        }
        else        
            cUtil._sm.GoToBossGate(isIn);
    }

    public void OpenAnimation()
    {
        if (canUse.Equals(true))
            return;

        if (isStage.Equals(true))
            this.GetComponent<Image>().sprite = img_gate[1];
        else
            this.GetComponent<Animator>().SetTrigger("OpenGate");

        cUtil._soundMng.PlayOpenNextGate();
        canUse = true;
    }
}
