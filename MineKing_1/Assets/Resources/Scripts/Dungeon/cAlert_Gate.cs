﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cAlert_Gate : MonoBehaviour
{
    private bool isStage;
    private bool isIn;

    private void Start()
    {
        isStage = this.transform.parent.GetComponent<cGate>().isStage;
        isIn = this.transform.parent.GetComponent<cGate>().isIn;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag.Equals("Player"))
        {
            //이전 게이트
            if(isStage.Equals(true) && isIn.Equals(false))
                cUtil._player.dp.ActiveGateAlert(0);


            if (cUtil._player.isFlagDone)
            {
                //보스 게이트
                if (isStage.Equals(false))
                    cUtil._player.dp.ActiveGateAlert(1);

                //이전 게이트
                if (isStage.Equals(true) && isIn.Equals(true))
                    cUtil._player.dp.ActiveGateAlert(2);
            }
        }
 
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))
        {
            cUtil._player.dp.InActiveGateAlert();
        }
    }
}
