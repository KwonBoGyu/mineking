﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class cBtn_attack : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public bool isButtonEnable { get; set; }
    public cPlayer scr_player;
    public Image img_gageBar;
    public Text t_gagePercent;
    public ParticleSystem p_gageEffect;
    public ParticleSystem p_gageCritEffect;
    public Image img_Critical;
    private float minChargePoint;
    private float maxChargePoint;
    private float chargeStartTimer;
    private bool isChargingOn;
    private float reduceFactor;

    private bool criticalOn;
    private float criticalReduceAmount;
    public float criticalMin;

    private Vector2 prevTouchPos;
    public ParticleSystem effect;

    private float chargeTimer;
    public Sprite[] img_attackBtn;

    IEnumerator ChargeAttack;

    public void Init()
    {
        //chargeTimer = 0;
        //minChargePoint = 0;
        //maxChargePoint = 2.0f;
        //img_gageBar.fillAmount = minChargePoint;

        //if(scr_player.tag.Equals("player_skin"))
        //    criticalMin = 0.8f;
        //else 
        //    criticalMin = 0.9f - (0.05f * cUtil._user._playerInfo.skillLevel[2]);

        //img_Critical.fillAmount = 1 - criticalMin;
        //criticalReduceAmount = 0.5f;
        //reduceFactor = 0.5f;
        //img_gageBar.transform.parent.gameObject.SetActive(false);

        isButtonEnable = true;

        //보스 디버깅용
        chargeTimer = 0;
        minChargePoint = 0;
        maxChargePoint = 2.0f;
        img_gageBar.fillAmount = minChargePoint;

        if (scr_player.tag.Equals("player_skin"))
            criticalMin = 0.8f;
        else
            criticalMin = 0.9f - (0.05f);

        img_Critical.fillAmount = 1 - criticalMin;
        criticalReduceAmount = 0.5f;
        reduceFactor = 0.5f;
        img_gageBar.transform.parent.gameObject.SetActive(false);
    }

    private void FixedUpdate()
    {
        if(isButtonEnable)
        {
            if (img_gageBar.fillAmount >= criticalMin)
            {
                if (criticalOn.Equals(false) && effect.isPlaying.Equals(false))
                {
                    effect.gameObject.SetActive(true);
                    this.GetComponent<Image>().sprite = img_attackBtn[1];
                }
            }

            //차치중
            if (isChargingOn.Equals(true))
            {
                Attack();

                if (scr_player.GetIsClimbing().Equals(false))
                {
                    chargeTimer += Time.deltaTime;

                    if (chargeTimer > maxChargePoint)
                        chargeTimer = maxChargePoint;

                    img_gageBar.fillAmount = chargeTimer / maxChargePoint;
                    t_gagePercent.text = string.Format("{0:F0}%", 100 * img_gageBar.fillAmount);

                    if (scr_player.tag.Equals("player_skin"))
                    {
                        p_gageEffect.transform.position = new Vector3(
    img_gageBar.transform.position.x + img_gageBar.GetComponent<RectTransform>().rect.width * 0.34f * 0.7f * img_gageBar.fillAmount,
    p_gageEffect.transform.position.y, p_gageEffect.transform.position.z);
                    }
                    else
                    {
                        p_gageEffect.transform.position = new Vector3(
    img_gageBar.transform.position.x + img_gageBar.GetComponent<RectTransform>().rect.width * 0.5f * 0.7f * img_gageBar.fillAmount,
    p_gageEffect.transform.position.y, p_gageEffect.transform.position.z);
                    }

                }
                else
                {
                    chargeTimer -= Time.deltaTime * reduceFactor;
                    if (chargeTimer < 0)
                    {
                        chargeTimer = 0;
                        return;
                    }
                    img_gageBar.fillAmount = chargeTimer / maxChargePoint;
                    t_gagePercent.text = string.Format("{0:F0}%", 100 * img_gageBar.fillAmount);
                    if (scr_player.tag.Equals("player_skin"))
                    {
                        p_gageEffect.transform.position = new Vector3(
    img_gageBar.transform.position.x + img_gageBar.GetComponent<RectTransform>().rect.width * 0.34f * 0.7f * img_gageBar.fillAmount,
    p_gageEffect.transform.position.y, p_gageEffect.transform.position.z);
                    }
                    else
                    {
                        p_gageEffect.transform.position = new Vector3(
    img_gageBar.transform.position.x + img_gageBar.GetComponent<RectTransform>().rect.width * 0.5f * 0.7f * img_gageBar.fillAmount,
    p_gageEffect.transform.position.y, p_gageEffect.transform.position.z);
                    }

                    if (img_gageBar.fillAmount < 0.03f)
                    {
                        chargeTimer = 0;
                        img_gageBar.fillAmount = 0;
                        t_gagePercent.text = string.Format("{0:F0}%", 100 * img_gageBar.fillAmount);
                        p_gageEffect.transform.position = new Vector3(
                    img_gageBar.transform.position.x, p_gageEffect.transform.position.y, p_gageEffect.transform.position.z);
                        img_gageBar.transform.parent.gameObject.SetActive(false);
                    }
                }
            }
            //손 뗐을 때
            else
            {
                chargeTimer -= Time.deltaTime * reduceFactor;
                if (chargeTimer < 0)
                {
                    chargeTimer = 0;
                    return;
                }
                img_gageBar.fillAmount = chargeTimer / maxChargePoint;
                t_gagePercent.text = string.Format("{0:F0}%", 100 * img_gageBar.fillAmount);
                if (scr_player.tag.Equals("player_skin"))
                {
                    p_gageEffect.transform.position = new Vector3(
    img_gageBar.transform.position.x + img_gageBar.GetComponent<RectTransform>().rect.width * 0.34f * 0.7f * img_gageBar.fillAmount,
    p_gageEffect.transform.position.y, p_gageEffect.transform.position.z);
                }
                else
                {
                    p_gageEffect.transform.position = new Vector3(
    img_gageBar.transform.position.x + img_gageBar.GetComponent<RectTransform>().rect.width * 0.5f * 0.7f * img_gageBar.fillAmount,
    p_gageEffect.transform.position.y, p_gageEffect.transform.position.z);
                }

                if (img_gageBar.fillAmount < 0.03f)
                {
                    chargeTimer = 0;
                    img_gageBar.fillAmount = 0;
                    t_gagePercent.text = string.Format("{0:F0}%", 100 * img_gageBar.fillAmount);
                    p_gageEffect.transform.position = new Vector3(
                img_gageBar.transform.position.x, p_gageEffect.transform.position.y, p_gageEffect.transform.position.z);
                    img_gageBar.transform.parent.gameObject.SetActive(false);
                }
            }

            if (img_gageBar.fillAmount < criticalMin)
            {
                this.GetComponent<Image>().sprite = img_attackBtn[0];
                effect.gameObject.SetActive(false);
                criticalOn = false;
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (scr_player.GetIsCritAttack().Equals(true))
        {
            //족장, 나무늘보, 눈사람
            if(scr_player.skinId.Equals(3) || scr_player.skinId.Equals(6) || scr_player.skinId.Equals(8))
            {
                scr_player.EndCriticalAttack();
            }

            return;
        }

       isChargingOn = true;
        Attack();
    }

    public void OnPointerUp(PointerEventData eventData)
    {       
        isChargingOn = false;

        if (img_gageBar.fillAmount >= criticalMin)
            criticalOn = true;
    }

    public void GetHit(float pValue)
    {
        isChargingOn = false;
        chargeTimer -= pValue;

        if (chargeTimer < 0)
            chargeTimer = 0;
    }

    private void Attack()
    {
        if (scr_player.GetStatus().Equals(CHARACTERSTATUS.ATTACK))
        {
            if (scr_player.isDash.Equals(true))
                return;

            //풀차지 일 때 크리티컬 공격
            if (criticalOn.Equals(true))
            {
                if (scr_player.GetIsClimbing().Equals(true))
                    return;

                //족장일 때 위아래 방향 공격 불가
                if(scr_player.skinId.Equals(3))
                {
                    if (scr_player.GetDirection().y != 0)
                        return;
                }

                //네모네모 마민킹은 콤보 중첩
                if(scr_player.skinId.Equals(4))
                {
                    chargeTimer -= maxChargePoint * criticalReduceAmount;
                    if (chargeTimer < 0)
                        chargeTimer = 0;
                    p_gageCritEffect.Play();
                    criticalOn = false;
                }
                else
                {
                    ChargeAttack = ChargeAttack_cor();
                    StartCoroutine(ChargeAttack);
                }

            }
        }
        //일반 공격
        else if (scr_player.GetStatus() != CHARACTERSTATUS.ATTACK)
        {
            if (scr_player.GetIsCritAttack().Equals(true))
                return;

            if (scr_player.GetDirection().Equals(Vector3.up))
                scr_player.Attack_up();
            else if (scr_player.GetDirection().Equals(Vector3.down) && scr_player.GetIsClimbing().Equals(false))
                scr_player.Attack_down();
            else
                scr_player.Attack_front();

            if (scr_player.GetIsGrounded().Equals(true) && scr_player.GetIsClimbing().Equals(false))
            {
                isChargingOn = true;
                img_gageBar.transform.parent.gameObject.SetActive(true);
            }
        }

    }

    IEnumerator ChargeAttack_cor()
    {
        yield return null;

        //단련된 광부 : 0.5초 딜레이 후 스매쉬 공격
        if (scr_player.skinId.Equals(0))
            yield return new WaitForSeconds(0.1f);

        chargeTimer -= maxChargePoint * criticalReduceAmount;

        if (chargeTimer < 0)
            chargeTimer = 0;
               
        if (scr_player.GetDirection().Equals(Vector3.up))
            scr_player.ChargeAttack_up();
        else if (scr_player.GetDirection().Equals(Vector3.down) && scr_player.GetIsClimbing().Equals(false))
            scr_player.ChargeAttack_down();
        else
            scr_player.ChargeAttack_front();
        p_gageCritEffect.Play();

        criticalOn = false;
    }
}
