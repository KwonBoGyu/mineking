﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cFlag : MonoBehaviour
{
    public cGate nextGate;
    public cCamera cam;

    public cDungeonNormal_processor dp;
    
    public void Init()
    {                
        cam = GameObject.Find("DungeonNormalScene").transform.Find("Cam_main").GetComponent<cCamera>();

        if(cUtil._sm._scene != SCENE.DUNGEON_FIRST_12)
        {
            cam.trans_nextGate = nextGate.transform;
            nextGate.Init();
        } 

        cUtil._player.isFlagDone = false;

        //스테이지 점령지를 모두 점령하였다면 게이트 오픈
        if (cUtil._user.GetFlaged((byte)((int)cUtil._sm._scene - 3)).Equals(true))
        {
            this.transform.GetChild(0).GetComponent<Animator>().SetTrigger("FlagOn");
            cUtil._player.isFlagDone = true;

            if (cUtil._sm._scene != SCENE.DUNGEON_FIRST_12)
                nextGate.OpenAnimation();

            KeyMonsterEvent();
        }
        else
        {
            if (cUtil._sm._scene != SCENE.DUNGEON_FIRST_12)
                nextGate.canUse = false;
        }

    }

    public void FlagOn(byte pNum)
    {
        if (cUtil._user.GetFlaged((byte)((int)cUtil._sm._scene - 3)).Equals(true))
        {
            return;
        }

        this.transform.GetChild(0).GetComponent<Animator>().SetTrigger("FlagOn");
        cUtil._soundMng.PlayBuildEffect();
        cUtil._user.SetFlaged((byte)((int)cUtil._sm._scene - 3), true);
        cUtil._player.isFlagDone = true;
        cUtil._player.RestoreDp(cUtil._player.GetMaxDp().value);
        
        KeyMonsterEvent();

        cam.ShowNextGate();
    }

    private void KeyMonsterEvent()
    {
        //보스 스테이지가 존재한다면 키 몬스터 힌트 이벤트 발생
        if (((int)cUtil._sm._scene % 3).Equals(2))
        {
            switch ((int)cUtil._sm._scene)
            {
                case 5:
                    //키스톤이 있다면..
                    if (cUtil._user.GetInventory().IsItemExist(2, 50).Equals(true))
                    {
                        dp.PresetKeystone();                            
                        return;
                    }

                    break;
                case 8:

                    if (cUtil._user.GetInventory().IsItemExist(2, 51).Equals(true))
                    {
                        dp.PresetKeystone();
                        return;
                    }

                    break;
                case 11:
                    
                    if (cUtil._user.GetInventory().IsItemExist(2, 52).Equals(true))
                    {
                        dp.PresetKeystone();
                        return;
                    }

                    break;
                case 14:

                    if (cUtil._user.GetInventory().IsItemExist(2, 53).Equals(true))
                    {
                        dp.PresetKeystone();
                        return;
                    }

                    break;
            }
            dp.SetKeyMonster();
        }
    }
    
}
