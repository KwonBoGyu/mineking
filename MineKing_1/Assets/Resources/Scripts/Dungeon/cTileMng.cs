﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Tilemaps;

public class cTileMng : MonoBehaviour
{
    public struct Tile
    {
        public Vector3Int location { get; set; }
        public TileBase tileBase;
        public cProperty level { get; set; }
        public cProperty maxHp { get; set; }
        public cProperty curHp { get; set; }
        public cProperty rocks { get; set; }
        private long values { get; set; }
        public long Values
        {
            get
            {
                return values;
            }
            set
            {
                values = value;
            }
        }

        public Tile(Vector3Int pLocation, TileBase pTileBase, cProperty pLevel, cProperty pMaxHp, cProperty pCurHp, cProperty pRocks, long pValues)
        {
            location = pLocation;
            tileBase = pTileBase;
            level = new cProperty(pLevel);
            maxHp = new cProperty(pMaxHp);
            curHp = new cProperty(pCurHp);
            rocks = new cProperty(pRocks);
            values = pValues;
        }
    }

    public TileBase[] tileChars;

    public Color HitColor;

    public Grid grid;
    public int tileSize;
    public int numX;
    public int numY;
    public Dictionary<Vector3, Tile> dic_canHit;
    private Tilemap tileMap_cannotHit;
    private Tilemap tileMap_canHit;
    Vector3Int[] cellPos;

    Tile? tempTile;
    public GameObject effect_destroy;

    public GameObject obj_itemDrop;
    public cSoundMng soundMng;

    private IEnumerator CheckAtTile_delay;
    private bool tileCheckingBool;

    private GameObject bombEffect;

    public RectTransform camRect;
    
    public void Init()
    {
        tileMap_canHit = this.transform.GetChild(0).GetComponent<Tilemap>();
        tileMap_cannotHit = this.transform.GetChild(1).GetComponent<Tilemap>();
        tempTile = null;

        bombEffect = Resources.Load<GameObject>("Prefabs/reuse/effect_tileExplosion");
        
        if (cUtil._sm._scene.Equals(SCENE.SKIN))
        {
            obj_itemDrop = GameObject.Find("SkinScene").transform.Find("Canvas_main").Find("ItemDrop").gameObject;
            soundMng = GameObject.Find("SkinScene").transform.Find("Cam_main").GetComponent<cSoundMng>();
        }

        SetEntireTiles();

    }

    //pChar - 0 : 일반, 1 : 반사, 2 : 숫돌, 3 : 스위치, 4 : 폭탄, 5 : 금광
    public void ChangeTile(Vector3 pWorldPos, byte pChar)
    {
        Vector3Int worldToCellPos = tileMap_canHit.WorldToCell(pWorldPos);
        Vector3 cellToWorldPos = tileMap_canHit.CellToWorld(worldToCellPos);

        if (!dic_canHit.ContainsKey(cellToWorldPos) || dic_canHit[cellToWorldPos].curHp.value <= 0)
        {
            return;
        }

        //금광타일로 교체
        if(pChar.Equals(5))
        {
            Tile tempTileToUse;
            tempTileToUse = dic_canHit[cellToWorldPos];
            tempTileToUse.tileBase = tileChars[pChar];
            dic_canHit[cellToWorldPos] = tempTileToUse;

            tileMap_canHit.SetTile(worldToCellPos, tileChars[pChar]);
        }
    }

    //pChar - 0 : 일반, 1 : 반사, 2 : 숫돌, 3 : 스위치
    public bool CheckAttackedTile(Vector3 pWorldPos, long pDamage, out float pCurHpPercent, bool pIsPlayer)
    {
        bool isChecked = false;
        Vector3Int worldToCellPos = tileMap_canHit.WorldToCell(pWorldPos);
        Vector3 convertedWorldPos = tileMap_canHit.CellToWorld(worldToCellPos);
        
        //해당 위치에 타일이 있다면
        isChecked = UpdateAttackedTile(convertedWorldPos, pDamage, out pCurHpPercent, pIsPlayer);

        return isChecked;
    }
    //더블어택 전용
    public void CheckAttackedTile_delayed(Vector3 pWorldPos, long pDamage, float pDelay)
    {
        CheckAtTile_delay = CheckTile_cor(pWorldPos, pDamage, pDelay);
        StartCoroutine(CheckAtTile_delay);
    }
    IEnumerator CheckTile_cor(Vector3 pWorldPos, long pDamage, float pDelay)
    {
        yield return new WaitForSeconds(pDelay);

        float pCurHpPercent = 0;
        Vector3Int worldToCellPos = tileMap_canHit.WorldToCell(pWorldPos);
        Vector3 convertedWorldPos = tileMap_canHit.CellToWorld(worldToCellPos);

        //해당 위치에 타일이 있다면
        UpdateAttackedTile(convertedWorldPos, pDamage, out pCurHpPercent, true);
    }

    public void CheckCanGroundTile(cObject pObj)
    {
        pObj.notUpBlocked = true;
        pObj.notGrounded = true;
        pObj.notRightBlocked = true;
        pObj.notLeftBlocked = true;

        CheckTiles(tileMap_canHit, pObj, pObj.rt.size);
        CheckTiles(tileMap_cannotHit, pObj, pObj.rt.size);

        if (pObj.notUpBlocked)
            pObj.isUpBlocked = false;
        if (pObj.notGrounded)
            pObj.SetIsGrounded(false);
        if (pObj.notLeftBlocked)
            pObj.isLeftBlocked = false;
        if (pObj.notRightBlocked)
            pObj.isRightBlocked = false;
    }

    public void CheckCanGroundTile(cObject pObj, out bool isChecked)
    {
        isChecked = false;

        pObj.notUpBlocked = true;
        pObj.notGrounded = true;
        pObj.notRightBlocked = true;
        pObj.notLeftBlocked = true;
        
        CheckTiles(tileMap_canHit, pObj, pObj.rt.size);
        CheckTiles(tileMap_cannotHit, pObj, pObj.rt.size);
        
        if (pObj.notUpBlocked)
            pObj.isUpBlocked = false;
        else
            isChecked = true;
        if (pObj.notGrounded)
            pObj.SetIsGrounded(false);
        else
            isChecked = true;
        if (pObj.notLeftBlocked)
            pObj.isLeftBlocked = false;
        else
            isChecked = true;
        if (pObj.notRightBlocked)
            pObj.isRightBlocked = false;
        else
            isChecked = true;
    }

    private void CheckTiles(Tilemap pTileMap, cObject pObj, Vector2 pSize)
    {
        Vector3 originTPos = pObj.originObj.transform.position;
        float originRtXLenHalf = pObj.rt.size.x * 0.5f;
        float originRtYLenHalf = pObj.rt.size.y * 0.5f;

        cellPos = new Vector3Int[]
            {
                new Vector3Int((int)originTPos.x, (int)originTPos.y + (int)(pSize.y * 0.5f) + (int)(tileSize * 0.5f), 0),
                new Vector3Int((int)originTPos.x + (int)(pSize.x * 0.4f), (int)originTPos.y + (int)(pSize.y * 0.7f), 0),
                new Vector3Int((int)originTPos.x + (int)(pSize.x * 0.8f), (int)originTPos.y, 0),
                new Vector3Int((int)originTPos.x + (int)(pSize.x * 0.4f), (int)originTPos.y - (int)(pSize.y * 0.7f), 0),
                new Vector3Int((int)originTPos.x, (int)originTPos.y - (int)(pSize.y * 0.5f) - (int)(tileSize * 0.5f), 0),
                new Vector3Int((int)originTPos.x - (int)(pSize.x * 0.4f), (int)originTPos.y - (int)(pSize.y * 0.7f), 0),
                new Vector3Int((int)originTPos.x - (int)(pSize.x * 0.8f), (int)originTPos.y, 0),
                new Vector3Int((int)originTPos.x - (int)(pSize.x * 0.4f), (int)originTPos.y + (int)(pSize.y * 0.7f), 0),
            };

        for (short i = 0; i < cellPos.Length; i++)
        {
            Vector3Int worldToCellPos = pTileMap.WorldToCell(cellPos[i]);
            Vector3 cellToWorldPos = pTileMap.CellToWorld(worldToCellPos);
            TileBase t_tile = pTileMap.GetTile(worldToCellPos);

            if (t_tile != null)
            {
                switch (i)
                {
                    //위쪽 충돌
                    case 0:
                        //충돌하였다면..
                        if (originTPos.y + originRtYLenHalf > cellToWorldPos.y)
                        {
                            pObj.notUpBlocked = false;
                            pObj.isUpBlocked = true;
                            pObj.originObj.transform.position = new Vector3(
                                originTPos.x,
                                cellToWorldPos.y - originRtYLenHalf,
                                originTPos.z
                                );
                        }
                        break;
                    //오른쪽 위 충돌
                    case 1:
                        //충돌하였다면..
                        if (originTPos.x + originRtXLenHalf > cellToWorldPos.x - 1 &&
                           originTPos.y + originRtYLenHalf > cellToWorldPos.y)
                        {
                            float distX = Mathf.Abs((originTPos.x + originRtXLenHalf) -
                                cellToWorldPos.x);
                            float distY = Mathf.Abs((originTPos.y + originRtYLenHalf) -
                                cellToWorldPos.y);

                            //가로 면적이 크다면 아래로
                            if (distX > distY)
                            {
                                pObj.notUpBlocked = false;
                                pObj.isUpBlocked = true;
                                pObj.originObj.transform.position = new Vector3(
                                    originTPos.x,
                                    cellToWorldPos.y - originRtYLenHalf,
                                    originTPos.z
                                    );
                            }
                            //세로 면적이 크다면 왼쪽으로
                            else
                            {
                                pObj.notRightBlocked = false;
                                pObj.isRightBlocked = true;
                                pObj.originObj.transform.position = new Vector3(
                                cellToWorldPos.x - originRtXLenHalf,
                                originTPos.y,
                                originTPos.z
                                );
                            }
                        }
                        break;
                    //오른쪽 충돌
                    case 2:
                        //충돌하였다면..
                        if (originTPos.x + originRtXLenHalf > cellToWorldPos.x - 1)
                        {
                            pObj.notRightBlocked = false;
                            pObj.isRightBlocked = true;
                            pObj.originObj.transform.position = new Vector3(
                                cellToWorldPos.x - originRtXLenHalf,
                                originTPos.y,
                                originTPos.z
                                );
                        }

                        Vector3 checkUpRightTile = new Vector3(pObj.originObj.transform.position.x,
                            pObj.originObj.transform.position.y + tileSize,
                            pObj.originObj.transform.position.z);
                        break;
                    //오른쪽 아래 충돌
                    case 3:
                        //충돌하였다면..
                        if (originTPos.x + originRtXLenHalf > cellToWorldPos.x - 1 &&
                           originTPos.y - originRtYLenHalf < (cellToWorldPos.y + tileSize) + 1)
                        {
                            float distX = Mathf.Abs((originTPos.x + originRtXLenHalf) -
                                cellToWorldPos.x);
                            float distY = Mathf.Abs((originTPos.y - originRtYLenHalf) -
                               (cellToWorldPos.y + tileSize));

                            //가로 면적이 크다면 위로
                            if (distX > distY)
                            {
                                pObj.notGrounded = false;
                                pObj.SetIsGrounded(true);
                                pObj.originObj.transform.position = new Vector3(
                                    originTPos.x,
                                    (cellToWorldPos.y + tileSize) + originRtYLenHalf,
                                    originTPos.z
                                    );
                            }
                            //세로 면적이 크다면 왼쪽으로
                            else
                            {
                                pObj.notRightBlocked = false;
                                pObj.isRightBlocked = true;
                                pObj.originObj.transform.position = new Vector3(
                                  cellToWorldPos.x - originRtXLenHalf,
                                  originTPos.y,
                                  originTPos.z
                                  );
                            }
                        }
                        else if (originTPos.y - originRtYLenHalf < (cellToWorldPos.y + tileSize) + 1)
                        {
                            pObj.notGrounded = false;
                        }
                        break;
                    //아래쪽 충돌
                    case 4:
                        //충돌하였다면..
                        if (originTPos.y - originRtYLenHalf < (cellToWorldPos.y + tileSize))
                        {
                            pObj.notGrounded = false;
                            pObj.SetIsGrounded(true);
                            pObj.originObj.transform.position = new Vector3(
                                originTPos.x,
                                (cellToWorldPos.y + tileSize) + originRtYLenHalf,
                                originTPos.z
                                );
                        }
                        else if (originTPos.y - originRtYLenHalf < (cellToWorldPos.y + tileSize) + 1)
                        {
                            pObj.notGrounded = false;
                        }

                        break;
                    //왼쪽 아래 충돌
                    case 5:
                        //충돌하였다면..
                        if (originTPos.x - originRtXLenHalf < (cellToWorldPos.x + tileSize) - 1 &&
                           originTPos.y - originRtYLenHalf < (cellToWorldPos.y + tileSize) - 1)
                        {
                            float distX = Mathf.Abs((originTPos.x - originRtXLenHalf) -
                                (cellToWorldPos.x + tileSize));
                            float distY = Mathf.Abs((originTPos.y - originRtYLenHalf) -
                               (cellToWorldPos.y + tileSize));

                            //가로 면적이 크다면 위로
                            if (distX > distY)
                            {
                                pObj.notGrounded = false;
                                pObj.SetIsGrounded(true);
                                pObj.originObj.transform.position = new Vector3(
                                    originTPos.x,
                                    (cellToWorldPos.y + tileSize) + originRtYLenHalf,
                                    originTPos.z
                                    );
                            }
                            //세로 면적이 크다면 오른쪽으로
                            else
                            {
                                pObj.notLeftBlocked = false;
                                pObj.isLeftBlocked = true;
                                pObj.originObj.transform.position = new Vector3(
                                (cellToWorldPos.x + tileSize) + originRtXLenHalf,
                                originTPos.y,
                                originTPos.z
                                );
                            }
                        }
                        else if (originTPos.y - originRtYLenHalf < (cellToWorldPos.y + tileSize) + 1)
                        {
                            pObj.notGrounded = false;
                        }

                        break;
                    //왼쪽 충돌
                    case 6:
                        //충돌하였다면..
                        if (originTPos.x - originRtXLenHalf < (cellToWorldPos.x + tileSize) + 1)
                        {
                            pObj.notLeftBlocked = false;
                            pObj.isLeftBlocked = true;
                            pObj.originObj.transform.position = new Vector3(
                                (cellToWorldPos.x + tileSize) + originRtXLenHalf,
                                originTPos.y,
                                originTPos.z
                                );
                        }
                        break;
                    //왼쪽 위 충돌
                    case 7:
                        //충돌하였다면..
                        if (originTPos.x - originRtXLenHalf < (cellToWorldPos.x + tileSize) + 1 &&
                           originTPos.y + originRtYLenHalf > cellToWorldPos.y)
                        {
                            float distX = Mathf.Abs((originTPos.x - originRtXLenHalf) -
                                (cellToWorldPos.x + tileSize));
                            float distY = Mathf.Abs((originTPos.y + originRtYLenHalf) -
                               (cellToWorldPos.y));

                            //가로 면적이 크다면 아래로
                            if (distX > distY)
                            {
                                pObj.notUpBlocked = false;
                                pObj.isUpBlocked = true;
                                pObj.originObj.transform.position = new Vector3(
                                    originTPos.x,
                                    cellToWorldPos.y - originRtYLenHalf,
                                    originTPos.z
                                    );
                            }
                            //세로 면적이 크다면 오른쪽으로
                            else
                            {
                                pObj.notLeftBlocked = false;
                                pObj.isLeftBlocked = true;
                                pObj.originObj.transform.position = new Vector3(
                                (cellToWorldPos.x + tileSize) + originRtXLenHalf,
                                originTPos.y,
                                originTPos.z
                                );
                            }
                        }
                        break;
                }
                originTPos = pObj.originObj.transform.position;
            }
        }
        //========for문 종료========//
    }

    private void SetEntireTiles()
    {
        dic_canHit = new Dictionary<Vector3, Tile>();

        foreach (Vector3Int pos in tileMap_canHit.cellBounds.allPositionsWithin)
        {
            Vector3Int localPlace = new Vector3Int(pos.x, pos.y, pos.z);

            if (!tileMap_canHit.HasTile(localPlace))
                continue;
            if (localPlace.x < 0)
                continue;

            Tile tile;

            long rockVal = 1;
            long rockHp = 1;
            int curSceneNum = (int)cUtil._sm._scene;
                        
            //타일 Hp. 광석량 세팅
            switch (curSceneNum)
            {
                case 3:
                    rockHp = 26;
                    rockVal = Random.Range((int)1, (int)4);
                    break;
                case 4:
                    rockHp = 50;
                    rockVal = Random.Range((int)2, (int)5);
                    break;
                case 5:
                    rockHp = 120;
                    rockVal = Random.Range((int)3, (int)7);
                    break;
                case 6:
                    rockHp = 800;
                    rockVal = Random.Range((int)10, (int)21);
                    break;
                case 7:
                    rockHp = 4000;
                    rockVal = Random.Range((int)15, (int)26);
                    break;
                case 8:
                    rockHp = 10000;
                    rockVal = Random.Range((int)20, (int)31);
                    break;
                case 9:
                    rockHp = 26000;
                    rockVal = Random.Range((int)30, (int)51);
                    break;
                case 10:
                    rockHp = 72000;
                    rockVal = Random.Range((int)40, (int)61);
                    break;
                case 11:
                    rockHp = 200000;
                    rockVal = Random.Range((int)50, (int)71);
                    break;
                case 12:
                    rockHp = 550000;
                    rockVal = Random.Range((int)70, (int)101);
                    break;
                case 13:
                    rockHp = 1800000;
                    rockVal = Random.Range((int)80, (int)111);
                    break;
                case 14:
                    rockHp = 8000000;
                    rockVal = Random.Range((int)90, (int)121);
                    break;
            }

            //숫돌 타일
            if (tileMap_canHit.GetTile(localPlace).name.Contains("img_hone"))
            {
                long tempDp = (long)(cUtil._player.GetMaxDp().value * 0.2);

                if (tempDp.Equals(0))
                    tempDp = 1;

                tile = new Tile(localPlace,
                tileMap_canHit.GetTile(localPlace),
                new cProperty("Level", 1),
                new cProperty("MaxHp", rockHp),
                new cProperty("CurHp", rockHp),
                new cProperty("Rocks", rockVal), 
                       tempDp       
                );
            }
            //나머지 타일
            else
            {
                tile = new Tile(localPlace,
                    tileMap_canHit.GetTile(localPlace),
                    new cProperty("Level", 1),
                    new cProperty("MaxHp", rockHp),
                    new cProperty("CurHp", rockHp),
                    new cProperty("Rocks", rockVal), rockVal);
            }

            dic_canHit.Add(tileMap_canHit.CellToWorld(localPlace), tile);
        }
        
        cUtil._tileMng = this;
    }

    private bool UpdateAttackedTile(Vector3 pCurPos, long pDamage, out float pCurHpPercent, bool pIsPlayer = false)
    {
        bool isChecked = false;
        Vector3Int worldToCellPos = tileMap_canHit.WorldToCell(pCurPos);
        Vector3 cellToWorldPos = tileMap_canHit.CellToWorld(worldToCellPos);
        if (!dic_canHit.ContainsKey(cellToWorldPos))
        {
            pCurHpPercent = 0;
            return false;
        }
        if (dic_canHit[cellToWorldPos].curHp.value <= 0)
        {
            pCurHpPercent = 0;
            return false;
        }

        Tile tempTileToUse;
        tempTileToUse = dic_canHit[cellToWorldPos];
        tempTileToUse.curHp.value -= pDamage;
        dic_canHit[cellToWorldPos] = tempTileToUse;
        pCurHpPercent = (float)tempTileToUse.curHp.value / (float)dic_canHit[cellToWorldPos].maxHp.value;
        
        if (tempTileToUse.curHp.value <= 0)
        {
            tempTileToUse.curHp.value = 0;
            pCurHpPercent = 0;
            isChecked = true;

            //금광 타일
            if (dic_canHit[cellToWorldPos].tileBase.name.Contains("img_tile3"))
            {
                PlayTileEffect(1, new Vector3(cellToWorldPos.x + tileSize / 2,
       cellToWorldPos.y + tileSize / 2, cellToWorldPos.z));

                //아이템 드롭
                byte itemNum = (byte)Random.Range(0, citemTable.GetUseItemTotalNum());
                
                for (byte k = 0; k < obj_itemDrop.transform.GetChild(itemNum).childCount; k++)
                {
                    if (obj_itemDrop.transform.GetChild(itemNum).GetChild(k).gameObject.activeSelf.Equals(false))
                    {
                        obj_itemDrop.transform.GetChild(itemNum).GetChild(k).transform.position = new Vector3(cellToWorldPos.x + tileSize / 2,
   cellToWorldPos.y + tileSize / 2, cellToWorldPos.z);
                        obj_itemDrop.transform.GetChild(itemNum).GetChild(k).gameObject.SetActive(true);
                        obj_itemDrop.transform.GetChild(itemNum).GetChild(k).GetComponent<cItemDrop>().Init();
                        obj_itemDrop.transform.GetChild(itemNum).GetChild(k).GetComponent<cItemDrop>().itemId = itemNum;
                        break;
                    }
                }

            }           
            //노말 타일
            else
            {
                PlayTileEffect(0, new Vector3(cellToWorldPos.x + tileSize / 2,
    cellToWorldPos.y + tileSize / 2, cellToWorldPos.z));

                if (pIsPlayer.Equals(true))
                {
                    //내구도 하락
                    cUtil._player.ReduceDp(1);

                    //반사데미지 타일
                    if (dic_canHit[cellToWorldPos].tileBase.name.Contains("img_cactus"))
                    {
                        long tempDmg = (long)(cUtil._player.GetMaxHp().value * 0.05f);
                        //플레이어 최대 체력의 5%가 0이라면 데미지를 1로 바꾼다.
                        if (tempDmg.Equals(0))
                            tempDmg = 1;

                        cUtil._player.ReduceHp(tempDmg,Vector3.zero,true);
                    }
                    //숫돌 타일
                    else if (dic_canHit[cellToWorldPos].tileBase.name.Contains("img_hone"))
                    {
                        if (tempTileToUse.Values > 0)
                        {
                            Debug.Log("tempTileToUse.Values : " + tempTileToUse.Values);
                            cUtil._player.RestoreDp(tempTileToUse.Values);
                        }
                        cUtil._player.RestoreDp(1);
                    }
                    //스위치 타일
                    else if (dic_canHit[cellToWorldPos].tileBase.name.Contains("img_switch"))
                    {
                        Vector3 firstPos = new Vector3(camRect.position.x - camRect.sizeDelta.x * 0.5f,
                            camRect.position.y + camRect.sizeDelta.y * 0.5f, cellToWorldPos.z);

                        for(float i = firstPos.x; i <= camRect.position.x + camRect.sizeDelta.x * 0.5f; i += tileSize)
                        {
                            for (float k = firstPos.y; k >= camRect.position.y - camRect.sizeDelta.y * 0.5f; k -= tileSize)
                            {
                                ChangeTile(new Vector3(i, k, cellToWorldPos.z), 5);
                            }
                        }
                    }
                }

                //폭탄 타일
                if (dic_canHit[cellToWorldPos].tileBase.name.Contains("img_bomb"))
                {
                    float tempf = 0;

                    if(pIsPlayer.Equals(true))
                        cUtil._player.ReduceHp((long)(cUtil._player.GetMaxHp().value * 0.5f), Vector3.zero);

                    GameObject tempEffect = Instantiate(bombEffect);
                    tempEffect.transform.SetParent(this.transform.parent.Find("Canvas")); 
                    tempEffect.transform.position = new Vector3(cellToWorldPos.x - tileSize * 3.5f, cellToWorldPos.y - tileSize * 1.5f, 0);
                    tempEffect.transform.localScale = new Vector3(1, 1, 1);
                    tempEffect.transform.GetChild(0).localScale = new Vector3(4, 4, 4);


                    //왼쪽위 -> 오른아래 순
                    for (byte i = 0; i < 3; i++)
                    {
                        for (byte k = 0; k < 3; k++)
                        {
                            bool tt = CheckAttackedTile(new Vector3((cellToWorldPos.x - tileSize * 0.5f) + k * tileSize,
                                (cellToWorldPos.y + tileSize + tileSize * 0.5f) - i * tileSize, 0),
                                 1000000, out tempf, false);
                        }
                    }
                }
            }

            //장애물 삭제
            {
                //왼쪽 장애물
                RaycastHit2D[] hit = Physics2D.RaycastAll(new Vector2(cellToWorldPos.x - 20, cellToWorldPos.y + tileSize * 0.5f), Vector3.forward);
                for (byte i = 0; i < hit.Length; i++)
                {
                    if (hit[i].transform.tag.Equals("obstacle_attached"))
                    {
                        Destroy(hit[i].transform.gameObject);
                        break;
                    }
                }
                //위쪽 장애물            
                hit = Physics2D.RaycastAll(new Vector2(cellToWorldPos.x + tileSize * 0.5f, cellToWorldPos.y + tileSize + 20), Vector3.forward);
                for (byte i = 0; i < hit.Length; i++)
                {
                    if (hit[i].transform.tag.Equals("obstacle_attached"))
                    {
                        Destroy(hit[i].transform.gameObject);
                        break;
                    }
                }
                //오른쪽 장애물            
                hit = Physics2D.RaycastAll(new Vector2(cellToWorldPos.x + tileSize + 20, cellToWorldPos.y + tileSize * 0.5f), Vector3.forward);
                for (byte i = 0; i < hit.Length; i++)
                {
                    if (hit[i].transform.tag.Equals("obstacle_attached"))
                    {
                        Destroy(hit[i].transform.gameObject);
                        break;
                    }
                }
                //아래쪽 장애물            
                hit = Physics2D.RaycastAll(new Vector2(cellToWorldPos.x + tileSize * 0.5f, cellToWorldPos.y - 20), Vector3.forward);
                for (byte i = 0; i < hit.Length; i++)
                {
                    if (hit[i].transform.tag.Equals("obstacle_attached"))
                    {
                        Destroy(hit[i].transform.gameObject);
                        break;
                    }
                }
            }

            tileMap_canHit.SetTile(worldToCellPos, null);
            cUtil._player.RootRocks(dic_canHit[cellToWorldPos].rocks.value);
            soundMng.playTileEffect();
        }
        else
        {
            if(pIsPlayer.Equals(true))
            {
                //내구도 하락
                cUtil._player.ReduceDp(1);

                //반사데미지 타일
                if (dic_canHit[cellToWorldPos].tileBase.name.Contains("img_cactus"))
                {
                    long tempDmg = (long)(cUtil._player.GetMaxHp().value * 0.05f);
                    //플레이어 최대 체력의 5%가 0이라면 데미지를 1로 바꾼다.
                    if (tempDmg.Equals(0))
                        tempDmg = 1;

                    cUtil._player.ReduceHp(tempDmg, Vector3.zero, true);
                }
                //숫돌 타일
                else if (dic_canHit[cellToWorldPos].tileBase.name.Contains("img_hone"))
                {
                    if(tempTileToUse.Values > 0)
                    {
                        float tempPer = (float)pDamage / (float)tempTileToUse.maxHp.value;
                        float tempDmg = tempTileToUse.Values * tempPer;

                        if (tempDmg.Equals(0))
                        {
                            tempDmg = 1;
                        }

                        tempTileToUse.Values = tempTileToUse.Values - (long)tempDmg;
                        if (tempTileToUse.Values < 0)
                            tempDmg += tempTileToUse.Values;

                        dic_canHit[cellToWorldPos] = tempTileToUse;
                        cUtil._player.RestoreDp((long)tempDmg);
                    }
                    cUtil._player.RestoreDp(1);
                }
            }

            StartCoroutine(TileReaction(tileMap_canHit, worldToCellPos));
            isChecked = true;
        }
        
        return isChecked;
    }

    // 함수명 수정 필요할듯? 함수명 != 기능
    public bool isTileExist(int pDir, Vector3 pCurPos)
    {
        Vector3Int calcPos = Vector3Int.zero;
        Vector3Int calcPos2 = Vector3Int.zero;

        bool t = false;
        Vector3Int worldToCellPos = tileMap_canHit.WorldToCell(pCurPos);
        Vector3Int worldToCellPos2 = tileMap_cannotHit.WorldToCell(pCurPos);

        switch (pDir)
        {
            //위
            case 0:
                calcPos = new Vector3Int(worldToCellPos.x, worldToCellPos.y + 1, 0);
                calcPos2 = new Vector3Int(worldToCellPos2.x, worldToCellPos2.y + 1, 0);
                break;
            //오른
            case 1:
                calcPos = new Vector3Int(worldToCellPos.x + 1, worldToCellPos.y, 0);
                calcPos2 = new Vector3Int(worldToCellPos2.x + 1, worldToCellPos2.y, 0);
                break;
            //아래
            case 2:
                calcPos = new Vector3Int(worldToCellPos.x, worldToCellPos.y - 1, 0);
                calcPos2 = new Vector3Int(worldToCellPos2.x, worldToCellPos2.y - 1, 0);
                break;
            //왼
            case 3:
                calcPos = new Vector3Int(worldToCellPos.x - 1, worldToCellPos.y, 0);
                calcPos2 = new Vector3Int(worldToCellPos2.x - 1, worldToCellPos2.y, 0);
                break;
        }

        TileBase t_tile1 = tileMap_canHit.GetTile(calcPos);
        TileBase t_tile2 = tileMap_cannotHit.GetTile(calcPos2);

        Debug.Log(t_tile1 + "    " + t_tile2);

        if (t_tile1 != null)
            t = true;
        if (t_tile2 != null)
            t = true;

        return t;
    }

    public bool isTileExist(Vector3 pPos)
    {
        bool isExist = false;
        Vector3Int worldToCellPos_canHit = tileMap_canHit.WorldToCell(pPos);
        Vector3Int worldToCellPos_cannotHit = tileMap_cannotHit.WorldToCell(pPos);

        TileBase tile_OnPos_canHit = tileMap_canHit.GetTile(worldToCellPos_canHit);
        TileBase tile_OnPos_cannotHit = tileMap_cannotHit.GetTile(worldToCellPos_cannotHit);

        if (tile_OnPos_canHit != null)
            isExist = true;
        if (tile_OnPos_cannotHit != null)
            isExist = true;

        return isExist;
    }

    private void PlayTileEffect(byte pChar, Vector3 pPos)
    {
        for (byte i = 0; i < effect_destroy.transform.GetChild(pChar).childCount; i++)
        {
            if (effect_destroy.transform.GetChild(pChar).GetChild(i).GetComponent<ParticleSystem>().isPlaying.Equals(true))
                continue;

            effect_destroy.transform.GetChild(pChar).GetChild(i).position = pPos;
            effect_destroy.transform.GetChild(pChar).GetChild(i).GetComponent<ParticleSystem>().Play();
            break;
        }
    }

    private IEnumerator TileReaction(Tilemap pTilemap, Vector3Int pPos)
    {
        float timer = 0;
        float MaxTime = 1;

        pTilemap.SetTileFlags(pPos, TileFlags.None);
        pTilemap.SetColor(pPos, HitColor);
        Color tC = new Color(1 - HitColor.r, 1 - HitColor.g, 1 - HitColor.b, 1);

        while (true)
        {
            yield return new WaitForFixedUpdate();

            if(timer > MaxTime)
            {
                timer = 0;
                pTilemap.SetColor(pPos, Color.white);
                break;
            }
            pTilemap.SetColor(pPos, new Color(HitColor.r + tC.r * timer, HitColor.g + tC.g * timer, HitColor.b + tC.b * timer, 1));

            timer += Time.deltaTime * 3;
        }
    }

    //타일 종류 체크용
    //0 : 금광타일, 1 : 선인장 타일
    public bool CheckTileCharacter(Vector3 pWorldPos, byte pChar)
    {
        tileCheckingBool = false;

        Vector3 cellToWorldPos = tileMap_cannotHit.CellToWorld(tileMap_cannotHit.WorldToCell(pWorldPos));
               
        switch(pChar)
        {
            case 0:
                if (dic_canHit[cellToWorldPos].tileBase.name.Contains("img_tile3"))
                    tileCheckingBool = true;
                
                break;

            case 1:
                if (dic_canHit[cellToWorldPos].tileBase.name.Contains("img_obstacle"))
                    tileCheckingBool = true;

                break;
        }

        return tileCheckingBool;
    }

    public Vector2 WorldPosToTilePos(Vector2 pWorldPos)
    {
        Vector3Int worldToCellPos = grid.WorldToCell(pWorldPos);
        return new Vector2(grid.CellToWorld(worldToCellPos).x, grid.CellToWorld(worldToCellPos).y);
    }
}
