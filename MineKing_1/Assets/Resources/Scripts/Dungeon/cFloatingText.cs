﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cFloatingText : MonoBehaviour
{
    private bool isInit;

    public Text[] t_text;

    private float[] timer;
    private bool[] isDone;
    public float moveSpeed;

    public GameObject DmgImg;
    public Sprite[] DmgFont;
    public Sprite[] DmgFont2;
    public Sprite missFont;

    private IEnumerator textOnDelay;
    
    public void Init()
    {
        timer = new float[DmgImg.transform.childCount];
        isDone = new bool[DmgImg.transform.childCount];
        moveSpeed = 100;
        isInit = true;
    }
        
    void Update()
    {
        if(isInit.Equals(true) && cUtil._sm._scene != SCENE.SKIN)
        {
            for (byte i = 0; i < DmgImg.transform.childCount; i++)
            {
                if (isDone[i].Equals(false))
                {
                    timer[i] += Time.deltaTime;
                    DmgImg.transform.GetChild(i).Translate(Vector3.up * moveSpeed * Time.deltaTime);
                    for (byte k = 0; k < DmgImg.transform.GetChild(i).childCount; k++)
                    {
                        DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().color
                            = new Color(DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().color.r,
                            DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().color.g,
                            DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().color.b, 1 - timer[i]);
                    }

                    if (timer[i] > 1.0f)
                    {
                        for (byte k = 0; k < DmgImg.transform.GetChild(i).childCount; k++)
                        {
                            DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().color
        = new Color(DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().color.r,
        DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().color.g,
        DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().color.b, 1);
                            DmgImg.transform.GetChild(i).GetChild(k).gameObject.SetActive(false);
                        }
                        timer[i] = 0;
                        isDone[i] = true;
                    }
                }
            }
        }
        
    }

    public void DamageTextOn(string pDamageText, Vector3 pPos, bool pIsCrit = false, bool isPlayerAttacked = false, float pDelay = 0)
    {
        if (pDelay != 0)
        {
            textOnDelay = TextOnDelay_cor(pDamageText, pPos, pIsCrit, pDelay);
            StartCoroutine(textOnDelay);
        }

        //miss
        if (pDamageText.Equals("0"))
        {
            for (byte i = 0; i < DmgImg.transform.childCount; i++)
            {
                if (isDone[i].Equals(true))
                {
                    DmgImg.transform.GetChild(i).GetChild(0).GetComponent<Image>().sprite =
                            missFont;
                    DmgImg.transform.GetChild(i).GetChild(0).localScale = new Vector3(0.4f, 0.4f, 0.4f);

                    DmgImg.transform.GetChild(i).GetChild(0).GetComponent<Image>().SetNativeSize();
                    DmgImg.transform.GetChild(i).GetChild(0).gameObject.SetActive(true);

                    DmgImg.transform.GetChild(i).position = new Vector3(pPos.x, pPos.y + 30, pPos.z);
                    isDone[i] = false;

                    if (isPlayerAttacked.Equals(true))
                    {
                        DmgImg.transform.GetChild(i).GetChild(0).GetComponent<Image>().color = new Color(1, 0.37f, 0.37f, 1);
                    }
                    else
                        DmgImg.transform.GetChild(i).GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 1);

                    break;
                }
            }
        }
        //일반 데미지
        else
        {
            char tempC = ' ';
            for (byte i = 0; i < DmgImg.transform.childCount; i++)
            {
                if (isDone[i].Equals(true))
                {
                    for (byte k = 0; k < pDamageText.Length; k++)
                    {
                        tempC  = pDamageText[k];

                        if (System.Convert.ToInt32(tempC).Equals(46))
                        {
                            if (pIsCrit.Equals(false))
                            {
                                DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().sprite =
                                    DmgFont[16];
                                DmgImg.transform.GetChild(i).GetChild(k).localScale = new Vector3(0.4f, 0.4f, 0.4f);
                            }
                            else
                            {
                                DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().sprite =
                                    DmgFont2[16];
                                DmgImg.transform.GetChild(i).GetChild(k).localScale = new Vector3(0.6f, 0.6f, 0.6f);
                            }
                        }
                        else if (System.Convert.ToInt32(tempC).Equals(97))
                        {
                            if (pIsCrit.Equals(false))
                            {
                                DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().sprite =
                                    DmgFont[10];
                                DmgImg.transform.GetChild(i).GetChild(k).localScale = new Vector3(0.4f, 0.4f, 0.4f);
                            }
                            else
                            {
                                DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().sprite =
                                    DmgFont2[10];
                                DmgImg.transform.GetChild(i).GetChild(k).localScale = new Vector3(0.6f, 0.6f, 0.6f);
                            }
                        }
                        else if (System.Convert.ToInt32(tempC).Equals(98))
                        {
                            if (pIsCrit.Equals(false))
                            {
                                DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().sprite =
                                    DmgFont[11];
                                DmgImg.transform.GetChild(i).GetChild(k).localScale = new Vector3(0.4f, 0.4f, 0.4f);
                            }
                            else
                            {
                                DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().sprite =
                                    DmgFont2[11];
                                DmgImg.transform.GetChild(i).GetChild(k).localScale = new Vector3(0.6f, 0.6f, 0.6f);
                            }
                        }
                        else if (System.Convert.ToInt32(tempC).Equals(99))
                        {
                            if (pIsCrit.Equals(false))
                            {
                                DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().sprite =
                                    DmgFont[12];
                                DmgImg.transform.GetChild(i).GetChild(k).localScale = new Vector3(0.4f, 0.4f, 0.4f);
                            }
                            else
                            {
                                DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().sprite =
                                    DmgFont2[12];
                                DmgImg.transform.GetChild(i).GetChild(k).localScale = new Vector3(0.6f, 0.6f, 0.6f);
                            }
                        }
                        else if (System.Convert.ToInt32(tempC).Equals(100))
                        {
                            if (pIsCrit.Equals(false))
                            {
                                DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().sprite =
                                    DmgFont[13];
                                DmgImg.transform.GetChild(i).GetChild(k).localScale = new Vector3(0.4f, 0.4f, 0.4f);
                            }
                            else
                            {
                                DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().sprite =
                                    DmgFont2[13];
                                DmgImg.transform.GetChild(i).GetChild(k).localScale = new Vector3(0.6f, 0.6f, 0.6f);
                            }
                        }
                        else if (System.Convert.ToInt32(tempC).Equals(101))
                        {
                            if (pIsCrit.Equals(false))
                            {
                                DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().sprite =
                                    DmgFont[14];
                                DmgImg.transform.GetChild(i).GetChild(k).localScale = new Vector3(0.4f, 0.4f, 0.4f);
                            }
                            else
                            {
                                DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().sprite =
                                    DmgFont2[14];
                                DmgImg.transform.GetChild(i).GetChild(k).localScale = new Vector3(0.6f, 0.6f, 0.6f);
                            }
                        }
                        else if (System.Convert.ToInt32(tempC).Equals(102))
                        {
                            if (pIsCrit.Equals(false))
                            {
                                DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().sprite =
                                    DmgFont[15];
                                DmgImg.transform.GetChild(i).GetChild(k).localScale = new Vector3(0.4f, 0.4f, 0.4f);
                            }
                            else
                            {
                                DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().sprite =
                                    DmgFont2[15];
                                DmgImg.transform.GetChild(i).GetChild(k).localScale = new Vector3(0.6f, 0.6f, 0.6f);
                            }
                        }
                        //숫자 폰트
                        else
                        {
                            byte temp = (byte)char.GetNumericValue(pDamageText[k]);

                            if (pIsCrit.Equals(false))
                            {
                                DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().sprite =
                                    DmgFont[temp];
                                DmgImg.transform.GetChild(i).GetChild(k).localScale = new Vector3(0.4f, 0.4f, 0.4f);
                            }
                            else
                            {
                                DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().sprite =
                                    DmgFont2[temp];
                                DmgImg.transform.GetChild(i).GetChild(k).localScale = new Vector3(0.6f, 0.6f, 0.6f);
                            }
                        }

                        
                        DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().SetNativeSize();

                        if (isPlayerAttacked.Equals(true))
                        {
                            DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().color = new Color(1, 0.37f, 0.37f, 1);
                        }
                        else
                            DmgImg.transform.GetChild(i).GetChild(k).GetComponent<Image>().color = new Color(1, 1, 1, 1);

                        DmgImg.transform.GetChild(i).GetChild(k).gameObject.SetActive(true);
                    }
                    DmgImg.transform.GetChild(i).position = new Vector3(pPos.x, pPos.y + 30, pPos.z);
                    isDone[i] = false;
                    break;
                }
            }
        }
    }

    IEnumerator TextOnDelay_cor(string pDamageText, Vector3 pPos, bool pIsCrit, float pDelay)
    {
        yield return new WaitForSeconds(pDelay);

        DamageTextOn(pDamageText, pPos, pIsCrit, false, 0);
    }
}
