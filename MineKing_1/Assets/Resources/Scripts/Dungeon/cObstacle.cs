﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cObstacle : MonoBehaviour
{
    //On?
    public bool isOn;
    //On 지속 시간
    public float onCoolTime;
    public float onCoolTimer;
    //Off 지속 시간
    public float offCoolTime;
    public float offCoolTimer;
    //콜라이더 지속 충돌시 Hit 쿨타임
    public float hitCoolTime;
    public float hitCoolTimer;
    //플레이가 안에 있냐?
    protected bool isPlayerIn;
    public Animator _animator;
    //타일에 붙어있는 장애물이냐?
    protected bool isAttached;
    
    public virtual void Init()
    {
        isOn = false;
        isPlayerIn = false;
        _animator = this.GetComponent<Animator>();
    }

    public virtual void ObstacleOn()
    {
        isOn = true;
        _animator.SetTrigger("On");
    }

    public virtual void ObstacleOff()
    {
        isOn = false;
        _animator.SetTrigger("Off");
    }

    public virtual void AlreadyExistCheck()
    {

    }
}
