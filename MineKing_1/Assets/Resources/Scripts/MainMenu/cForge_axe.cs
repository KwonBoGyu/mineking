﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cForge_axe : MonoBehaviour
{
    public Image img_axe;
    public Text t_level;
    public Text[] t_curValue;
    public Text[] t_nextValue;
    public Slider slider_upgrade;
    public Text t_curUpgradeNum;
    public Text t_upgradeGold;
    public Text t_upgradeGold2;
    public Image img_upgradeGold3;
    public Button b_upgrade;
    public Button[] b_plusMinusOne;

    private long upLevel;
    private long upGold;
    
    public ParticleSystem[] effects;
    public cMain_processor mp;

    public GameObject upgradePopUp;
    public Button b_exitPopUp;

    long weaponLevel ;
    byte maxStageNum ;

    void Start()
    {
        maxStageNum = cUtil._user.GetMaxClearedStage();
        b_upgrade.onClick.AddListener(() => UpgradeAxe());
        b_exitPopUp.onClick.AddListener(() => ExitPopUp());
        weaponLevel = cUtil._user.GetWeaponInfo().level.value;
        b_plusMinusOne[0].onClick.AddListener(() => OnClickedArrow(false));
        b_plusMinusOne[1].onClick.AddListener(() => OnClickedArrow(true));

        UpdateValues();
    }

    private void OnEnable()
    {
        UpdateValues();
    }

    private void ExitPopUp()
    {
        upgradePopUp.SetActive(false);
    }

    private void OnClickedArrow(bool isPlus)
    {
        //왼쪽 화살표
        if(isPlus.Equals(false))
        {
            if(slider_upgrade.value > slider_upgrade.minValue)
                slider_upgrade.value -= 1;
        }
        //오른쪽 화살표
        else
        {
            if (slider_upgrade.value < slider_upgrade.maxValue)
                slider_upgrade.value += 1;
        }
    }

    private void UpgradeAxe()
    {
        //강화 횟수가 0이라면..
        if(slider_upgrade.maxValue.Equals(0) || slider_upgrade.value.Equals(0))        
            return;

        //돈 없다면..
        if (cUtil._user.GetInventory().GetMoney().value < upGold)
            return;

        weaponLevel = cUtil._user.GetWeaponInfo().level.value;
        
        if (weaponLevel == 99)
            return;

        //돈이 있다면..
        cUtil._user.GetInventory().GetMoney().value -= upGold;

        //팝업 창 정보 입력 - 강화 전
        upgradePopUp.transform.GetChild(2).GetComponent<Text>().text = cUtil.GetValueToString(cUtil._user.GetWeaponInfo().damage.value);
        upgradePopUp.transform.GetChild(4).GetComponent<Text>().text = cUtil.GetValueToString(cUtil._user.GetWeaponInfo().hp.value);
        upgradePopUp.transform.GetChild(6).GetComponent<Text>().text = string.Format("{0:F2}", cUtil._user.GetWeaponInfo().attackSpeed);
        upgradePopUp.transform.GetChild(8).GetComponent<Text>().text = cUtil.GetValueToString(cUtil._user.GetWeaponInfo().indurance.value);
        upgradePopUp.transform.GetChild(10).GetComponent<Text>().text = string.Format("Lv. {0}", cUtil._user.GetWeaponInfo().level.value);
           
        //레벨
        cUtil._user.GetWeaponInfo().level.value = upLevel;
        //현재 공격력
        cUtil._user.GetWeaponInfo().damage.value = cWeaponTable.GetAxeInfo(upLevel).damage.value;
        t_curValue[0].text = cUtil.GetValueToString(cUtil._user.GetWeaponInfo().damage.value);
        //현재 Hp
        cUtil._user.GetWeaponInfo().hp.value = cWeaponTable.GetAxeInfo(upLevel).hp.value;
        t_curValue[1].text = cUtil.GetValueToString(cUtil._user.GetWeaponInfo().hp.value);
        //현재 공격속도
        cUtil._user.GetWeaponInfo().attackSpeed = cWeaponTable.GetAxeInfo(upLevel).attackSpeed;
        t_curValue[2].text = string.Format("{0:F2}", cUtil._user.GetWeaponInfo().attackSpeed);
        //현재 내구도
        cUtil._user.GetWeaponInfo().indurance.value = cWeaponTable.GetAxeInfo(upLevel).indurance.value;
        t_curValue[3].text = cUtil.GetValueToString(cUtil._user.GetWeaponInfo().indurance.value);

        //팝업 창 정보 입력 - 강화 후
        upgradePopUp.transform.GetChild(3).GetComponent<Text>().text = cUtil.GetValueToString(cUtil._user.GetWeaponInfo().damage.value);
        upgradePopUp.transform.GetChild(5).GetComponent<Text>().text = cUtil.GetValueToString(cUtil._user.GetWeaponInfo().hp.value);
        upgradePopUp.transform.GetChild(7).GetComponent<Text>().text = string.Format("{0:F2}", cUtil._user.GetWeaponInfo().attackSpeed);
        upgradePopUp.transform.GetChild(9).GetComponent<Text>().text = cUtil.GetValueToString(cUtil._user.GetWeaponInfo().indurance.value);
        upgradePopUp.transform.GetChild(11).GetComponent<Text>().text = string.Format("Lv. {0}", cUtil._user.GetWeaponInfo().level.value);

        cUtil._soundMng.PlayAxeUpgradeEffect();

        for (byte i = 0; i < effects.Length; i++)
        {
            effects[i].Stop();
            effects[i].Play();
        }

        weaponLevel = cUtil._user.GetWeaponInfo().level.value;

        //팝업창 띄우기
        upgradePopUp.SetActive(true);

        mp.UpdateValues();
        UpdateValues();
    }

    public void UpdateValues()
    {
        //레벨
        t_level.text = string.Format("Lv. {0}", cUtil.GetValueToString(cUtil._user.GetWeaponInfo().level.value));
        //현재 공격력
        t_curValue[0].text = cUtil.GetValueToString(cUtil._user.GetWeaponInfo().damage.value);
        //현재 Hp
        t_curValue[1].text = cUtil.GetValueToString(cUtil._user.GetWeaponInfo().hp.value);
        //현재 공격속도
        t_curValue[2].text = string.Format("{0:F2}", cUtil._user.GetWeaponInfo().attackSpeed);
        //현재 내구도
        t_curValue[3].text = cUtil.GetValueToString(cUtil._user.GetWeaponInfo().indurance.value);
                
        //스테이지 레벨 제한
        //스테이지 9 이하
        if (maxStageNum < 12)        
            slider_upgrade.maxValue = (((maxStageNum + 1) * 7) + 1) - weaponLevel;        
        //막보스 진행
        else
            slider_upgrade.maxValue = 99 - weaponLevel;

        slider_upgrade.onValueChanged.AddListener(SliderChanged);
        slider_upgrade.value = 0;
        SliderChanged(slider_upgrade.value);
    }

    private void SliderChanged(float pVal)
    {
        long val = (long)pVal;

        upLevel = 0;
        upGold = 0;
               
        //강화 횟수
        t_curUpgradeNum.text = val.ToString();

        //강화 레벨
        upLevel = weaponLevel;
        upLevel += val;
        //강화 골드
        upGold = cWeaponTable.GetUpgradeGoldByLevel(weaponLevel, upLevel).value;
        //돈 없으면 빨간색
        if (upGold > cUtil._user.GetInventory().GetMoney().value)
        {
            b_upgrade.interactable = false;
            t_upgradeGold2.color = new Color(1, 0, 0, 1);
            img_upgradeGold3.color = new Color(1, 0, 0, 1);
            t_upgradeGold.color = new Color(1, 0, 0, 1);
        }
        //가능하면 하얀색
        else
        {
            b_upgrade.interactable = true;
            t_upgradeGold2.color = new Color(1, 1, 1, 1);
            img_upgradeGold3.color = new Color(1, 1, 1, 1);
            t_upgradeGold.color = new Color(1, 1, 1, 1);
        }

        t_upgradeGold.text = cUtil.GetValueToString(upGold);
        //강화 공격력
        t_nextValue[0].text = cUtil.GetValueToString(cWeaponTable.GetAxeInfo(upLevel).damage.value);
        //강화 Hp
        t_nextValue[1].text = cUtil.GetValueToString(cWeaponTable.GetAxeInfo(upLevel).hp.value);
        //강화 공격속도
        t_nextValue[2].text = string.Format("{0:F2}", cWeaponTable.GetAxeInfo(upLevel).attackSpeed);
        //강화 내구도
        t_nextValue[3].text = cUtil.GetValueToString(cWeaponTable.GetAxeInfo(upLevel).indurance.value);
    }


}
