﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class cStore : cBuilding
{
    //판매버튼
    public Button[] b_sell;
    //시간이동 버튼
    public Button b_time;
    //전체 판매버튼
    public Button b_sellAll;
    //최고 판매가
    public Text[] t_bestSell;

    //보석 value 오브젝트들
    public GameObject[] obj_jewerly;

    public short curFrameIdx;

    public cMain_processor mp;

    public Text t_storeClock;

    //변동값 이미지
    public Sprite[] img_arrow;
    
    private void Start()
    {
        curFrameIdx = -1;
        b_click.onClick.AddListener(() => ActiveFrame());
        b_sellAll.onClick.AddListener(() => SellAllJewerly());

        for (byte i = 0; i < 5; i++)
        {
            byte n = i;
            b_sell[i].onClick.AddListener(() => SellJewerly(n));
        }

        b_time.onClick.AddListener(() => TimeCapsule());
    }

    private void OnEnable()
    {
        UpdateSellButton();
        UpdateMyValue();

        //최고 판매가 초기화
        for (byte i = 0; i < 5; i++)
            t_bestSell[i].text = cUtil.GetValueToString(cUtil._user.GetMaxJewerlySellPrice(i));
    }

    private void TimeCapsule()
    {
        cUtil._timeMng.StoreTimeCapsule();
    }


    //최고 판매가 r갱신
    private void CheckMaxSellPrice(byte pJewerlyIdx)
    {
        cUtil._user.SetMaxJewerlySellPrice(pJewerlyIdx, cUtil._user.GetCurStorePrice(pJewerlyIdx));

        t_bestSell[pJewerlyIdx].text = cUtil.GetValueToString(cUtil._user.GetMaxJewerlySellPrice(pJewerlyIdx));
    }
        
    //보석 판매
    private void SellJewerly(byte pChar)
    {
        //추가 완료했냐
        long amount = cUtil._user.GetInventory().GetJewerly()[pChar].value;

        cUtil._user.GetInventory().GetJewerly()[pChar].value -= amount;
        cUtil._user.GetInventory().GetMoney().value += cUtil._user.GetCurStorePrice(pChar) * amount;

        CheckMaxSellPrice(pChar);

        cUtil._soundMng.PlaySellJewerlyEffect();

        UpdateSellButton();
        UpdateMyValue();
    }

    private void SellAllJewerly()
    {
        for(byte i = 0; i < 5; i++)
        {
            //없으면 continue
            if (cUtil._user.GetInventory().GetJewerly()[i].value.Equals(0))
                continue;

            //추가 완료했냐
            long amount = cUtil._user.GetInventory().GetJewerly()[i].value;

            cUtil._user.GetInventory().GetJewerly()[i].value -= amount;
            cUtil._user.GetInventory().GetMoney().value += cUtil._user.GetCurStorePrice(i) * amount;
        }

        cUtil._soundMng.PlaySellJewerlyEffect();
        UpdateSellButton();
        UpdateMyValue();
    }

    //판매버튼 업데이트
    private void UpdateSellButton()
    {
        bool canSellAllOn = false;

        for(byte i = 0; i < 5; i++)
        {
            //보유량 없으면 하위 이미지 Active
            if (cUtil.GetValueToString(cUtil._user.GetInventory().GetJewerly()[i].value).Equals("0"))
                b_sell[i].transform.GetChild(0).gameObject.SetActive(true);
            //있으면 inActive
            else
            {
                b_sell[i].transform.GetChild(0).gameObject.SetActive(false);
                canSellAllOn = true;
            }
        }

        //전체 판매 버튼
        if (canSellAllOn.Equals(true))
            b_sellAll.transform.GetChild(0).gameObject.SetActive(false);
        else
            b_sellAll.transform.GetChild(0).gameObject.SetActive(true);
    }

    //보석 보유량 업데이트
    private void UpdateMyValue()
    {
        for (byte i = 0; i < 5; i++)
        {
            obj_jewerly[i].transform.GetChild(4).GetComponent<Text>().text = cUtil.GetValueToString(cUtil._user.GetInventory().GetJewerly()[i].value);
        }

        mp.UpdateValues();
        UpdateSellProperty();
    }

    //예상 수익 업데이트
    private void UpdateSellProperty()
    {
        long sellProperty;
        
        for (byte i = 0; i < 5; i++)
        {
            sellProperty = cUtil._user.GetCurStorePrice(i) *
                cUtil._user.GetInventory().GetJewerly()[i].value;

            obj_jewerly[i].transform.GetChild(5).GetComponent<Text>().text = cUtil.GetValueToString(sellProperty);
        }
    }

    //모든 Value 업데이트
    public void UpdateValue()
    {
        long tempJ;

        for (byte i = 0; i < 5; i++)
        {
            //현재값
            obj_jewerly[i].transform.GetChild(2).GetComponent<Text>().text = cUtil.GetValueToString(cUtil._user.GetCurStorePrice(i));

            //변동값
            tempJ = cUtil._user.GetCurStorePrice(i) - cUtil._user.GetPrevStorePrice(i);

            //변동값이 플러스일 때
            if (tempJ > 0)
            {
                obj_jewerly[i].transform.GetChild(3).GetChild(0).GetComponent<Image>().sprite = img_arrow[0];
                obj_jewerly[i].transform.GetChild(3).GetChild(0).GetComponent<Image>().SetNativeSize();

                if (tempJ > cUtil._user.GetAvgStorePrice(i) * 0.5f)
                {
                    obj_jewerly[i].transform.GetChild(3).GetChild(0).localScale = new Vector3(1, 1, 1);
                }
                else
                {
                    obj_jewerly[i].transform.GetChild(3).GetChild(0).localScale = new Vector3(0.5f, 0.5f, 1);
                }

                obj_jewerly[i].transform.GetChild(3).GetComponent<Text>().text = "+";
                obj_jewerly[i].transform.GetChild(3).GetComponent<Text>().color = new Color((float)(150.0f / 255.0f), 1, 0);
            }
            //변동값이 마이너스일 때
            else if(tempJ < 0)
            {
                obj_jewerly[i].transform.GetChild(3).GetChild(0).GetComponent<Image>().sprite = img_arrow[1];
                obj_jewerly[i].transform.GetChild(3).GetChild(0).GetComponent<Image>().SetNativeSize();

                if (tempJ < -(cUtil._user.GetAvgStorePrice(i) * 0.5f))
                {
                    obj_jewerly[i].transform.GetChild(3).GetChild(0).localScale = new Vector3(1, 1, 1);
                }
                else
                {
                    obj_jewerly[i].transform.GetChild(3).GetChild(0).localScale = new Vector3(0.5f, 0.5f, 1);
                }

                obj_jewerly[i].transform.GetChild(3).GetComponent<Text>().text = "";
                obj_jewerly[i].transform.GetChild(3).GetComponent<Text>().color = new Color(1, 0, 0);
            }            
            else
            {
                obj_jewerly[i].transform.GetChild(3).GetComponent<Text>().text = "";
                obj_jewerly[i].transform.GetChild(3).GetComponent<Text>().color = new Color(0.3f, 0.3f, 0.3f);
            }

            obj_jewerly[i].transform.GetChild(3).GetComponent<Text>().text +=
                cUtil.GetValueToString(tempJ);

            //보유량
            obj_jewerly[i].transform.GetChild(4).GetComponent<Text>().text =
                cUtil.GetValueToString(cUtil._user.GetInventory().GetJewerly()[i].value);                
        }

        UpdateSellProperty();
        UpdateSellButton();
    }

    private void ActiveFrame()
    {
        if (curFrameIdx.Equals(0))
        {
            return;
        }

        curFrameIdx = 0;
        obj_content.SetActive(true);

        UpdateValue();
        UpdateSellButton();
    }
    
    public void SetClock(string pStr)
    {
        t_storeClock.text = pStr;
    }
}
