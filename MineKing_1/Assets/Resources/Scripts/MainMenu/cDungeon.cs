﻿using UnityEngine;
using UnityEngine.UI;

public class cDungeon : cBuilding
{
    public Animator animator_main;
    public Button b_normal;
    private byte prevClickedStage;
    private byte maxStageNum; //최대 클리어 스테이지
    public Sprite[] img_gates; //게이트 이미지
    public GameObject obj_stageParent; //게이트 오브젝트
    public Button[] b_stage;
    public GameObject obj_curStage; //현재 스테이지 인디케이터
    public Text t_curStage;
    public Image img_skin;
    public Text[] t_skin;
    public Image img_weapon;
    public Text[] t_weapon;
    public Text t_weaponUpgrade;
    public Text[] t_stats;
    public Text t_charSkill;

    public void Init()
    {
        maxStageNum = cUtil._user.GetMaxClearedStage();
        b_normal.onClick.AddListener(() => OnClickStageStart());

        //현재 스테이지 텍스트
        t_curStage.text = string.Format("{0}층", maxStageNum + 1);

        byte curSkinId = cUtil._user.GetCurClothId();
        byte curWeaponId = cUtil._user.GetCurWeaponId();
        byte curWeaponLevel = (byte)cUtil._user.GetWeaponInfo().level.value;

        //스킨 초기화
        img_skin.sprite = cUtil._user.ClothImages[curSkinId];
        t_skin[0].text = cSkinTable.GetSkinInfo(curSkinId).skinName; //스킨 이름
        t_skin[1].text = cSkinTable.GetSkinInfo(curSkinId).skinDesc; //스킨 설명
        t_charSkill.text = cSkinTable.GetSkinInfo(curSkinId).skinSkillDesc; // 스킨 능력

        //장비 초기화
        img_weapon.sprite = cUtil._user.WeaponImages_dungeonEnter[curWeaponId];
        t_weapon[0].text = cWeaponTable.GetWeaponInfo(curWeaponId).weaponName;
        t_weapon[1].text = cWeaponTable.GetWeaponInfo(curWeaponId).weaponDesc;
        t_weaponUpgrade.text = string.Format("+{0}", curWeaponLevel);

        //스탯 초기화
        t_stats[0].text = cUtil.GetValueToString(cWeaponTable.GetAxeInfo(curWeaponLevel).damage.value);
        t_stats[1].text = cUtil.GetValueToString(cWeaponTable.GetAxeInfo(curWeaponLevel).hp.value);
        t_stats[2].text = string.Format("{0:F1}", cWeaponTable.GetAxeInfo(curWeaponLevel).attackSpeed);
        t_stats[3].text = cUtil.GetValueToString(cWeaponTable.GetAxeInfo(curWeaponLevel).indurance.value);

        //스테이지 오브젝트 초기화
        {
            for (byte i = 0; i < 12; i++)
            {
                bool isBoss = false;
                int k = i + 1;
                if ((k % 3).Equals(0))
                    isBoss = true;
                else
                    isBoss = false;

                b_stage[i].onClick.AddListener(() => OnClickStage((byte)k, isBoss));

                ////스테이지 스프라이트 초기화
                //이미 클리어한 스테이지라면
                if (k <= maxStageNum)
                {
                    if (isBoss.Equals(true))
                        obj_stageParent.transform.GetChild(i).GetComponent<Image>().sprite = img_gates[5];
                    else
                        obj_stageParent.transform.GetChild(i).GetComponent<Image>().sprite = img_gates[2];
                }
                //클리어하지 못한 스테이지라면
                else
                {
                    if (isBoss.Equals(true))
                        obj_stageParent.transform.GetChild(i).GetComponent<Image>().sprite = img_gates[3];
                    else
                        obj_stageParent.transform.GetChild(i).GetComponent<Image>().sprite = img_gates[0];
                }

                //현재 도전중인 스테이지라면
                if (k == maxStageNum + 1)
                {
                    prevClickedStage = (byte)k;

                    //인디케이터 위치
                    obj_curStage.transform.position = obj_stageParent.transform.GetChild(i).position;

                    if (isBoss.Equals(true))
                        obj_stageParent.transform.GetChild(i).GetComponent<Image>().sprite = img_gates[4];
                    else
                        obj_stageParent.transform.GetChild(i).GetComponent<Image>().sprite = img_gates[1];
                }
            }
        }




    }

    //스테이지 시작
    private void OnClickStageStart()
    {
        animator_main.SetTrigger("DungeonStart");
        cUtil._sm.ChangeScene(prevClickedStage + 2);
    }

    //스테이지 클릭시
    private void OnClickStage(byte stageNum, bool isBoss)
    {
        //진입할 수 없는 스테이지는 리턴
        if (stageNum > maxStageNum + 1)
        {
            return;
        }

        //똑같은 스테이지라면 리턴
        if (prevClickedStage.Equals(stageNum))
        {
            return;
        }

        //이전 스테이지 오브젝트 스프라이트 초기화
        if(prevClickedStage > maxStageNum)
        {
            if ((prevClickedStage % 3).Equals(0))
                obj_stageParent.transform.GetChild(prevClickedStage - 1).GetComponent<Image>().sprite = img_gates[3];
            else
                obj_stageParent.transform.GetChild(prevClickedStage - 1).GetComponent<Image>().sprite = img_gates[0];
        }
        
        //인디케이터 위치
        obj_curStage.transform.position = obj_stageParent.transform.GetChild(stageNum - 1).position;

        //현재 도전중인 스테이지라면
        if (stageNum.Equals((byte)(maxStageNum + 1)))
        {
            if (isBoss.Equals(true))
                obj_stageParent.transform.GetChild(stageNum - 1).GetComponent<Image>().sprite = img_gates[4];
            else
                obj_stageParent.transform.GetChild(stageNum - 1).GetComponent<Image>().sprite = img_gates[1];
        }

        //현재 스테이지 텍스트
        t_curStage.text = string.Format("{0}층", stageNum);

        prevClickedStage = stageNum;
    }
}
