﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class cItem_etc : cItem
{
    public byte amount;

    #region 생성자
    public cItem_etc(string pName, string pDesc, cProperty pPrice, byte pAmount, byte pId)
        : base(pName, pDesc, pPrice, pId)
    {
        amount = pAmount;
    }

    public cItem_etc(cItem_etc pIe)
        : base(pIe._name, pIe.desc, pIe.price, pIe.id)
    {
        amount = pIe.amount;
    }
    #endregion
}
