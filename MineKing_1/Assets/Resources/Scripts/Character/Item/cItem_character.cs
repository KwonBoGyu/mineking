﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class cItem_character : cItem
{

    #region 생성자
    public cItem_character(string pName, string pDesc, cProperty pPrice, byte pId)
        : base(pName, pDesc, pPrice, pId)
    {
    }

    public cItem_character(cItem_etc pIe)
        : base(pIe._name, pIe.desc, pIe.price, pIe.id)
    {
    }
    #endregion

}
