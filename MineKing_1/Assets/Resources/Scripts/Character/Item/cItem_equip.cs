﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class cItem_equip : cItem
{

    #region 생성자
    public cItem_equip(string pName, string pDesc, cProperty pPrice, byte pId)
        : base(pName, pDesc, pPrice, pId)
    {
    }

    public cItem_equip(cItem_equip pIe)
        : base(pIe._name, pIe.desc, pIe.price, pIe.id)
    {
    }
    #endregion
}
