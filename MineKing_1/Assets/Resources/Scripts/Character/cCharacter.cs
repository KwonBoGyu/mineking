﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum CHARACTERSTATUS
{
    NONE,
    MOVE,
    CROUCH,
    ATTACK,
    JUMP,
    JUMP_ATTACK,
    DASH,
    DASH_ATTACK,
    KNOCKBACK
}

public enum CHARDIRECTION
{
    NONE,
    UP,
    UPRIGHT,
    RIGHT,
    DOWNRIGHT,
    DOWN,
    DOWNLEFT,
    LEFT,
    UPLEFT,
}

public class cCharacter : cObject
{
    protected string nickName;
    protected cProperty damage;
    public cProperty damage_crit;
    protected float maxMoveSpeed;
    protected float dashMoveSpeed;
    protected const float dashTime = 0.7f;
    protected const float jumpTime = 0.5f;
    public bool isDash;
    protected float curMoveSpeed;
    protected cProperty maxHp;
    protected cProperty curHp;
    protected cProperty maxDp;
    protected cProperty curDp;
    protected float jumpHeight;
    public byte jumpCount;
    public bool isJumpStart;
    protected float maxDashCoolDown; // 실제 대쉬 쿨다운
    protected float dashCoolDown; // 쿨다운 계산용 (변화)
    protected bool isJetPackOn;
    protected bool isClimbing;
    public CHARACTERSTATUS status;
    protected CHARDIRECTION charDir;
    public CHARDIRECTION GetCharDir() { return charDir; }
    protected Vector3 dir;
    public GameObject obj_coolTime;
    public Text t_coolTime;
    protected IEnumerator delayHit;
    private IEnumerator _knockback;
    public cJoystick joystick;

    //크리티컬
    public bool isCritical;

    //hp
    public Image img_curHp;
    public Text t_hp;
    protected IEnumerator hpCor;
    protected IEnumerator hitCor;
    public Material[] materials;
    public List<Anima2D.SpriteMeshInstance> canChg_mats;
    public SpriteRenderer canChg_mats_forFinalBoss;

    //dp
    public Image img_curDp;
    public Text t_dp;
    protected IEnumerator dpCor;

    // 중력 적용에 필요한 변수
    protected float maxGravity = 1500;
    public float changingGravity;
    public float defaultGravity;

    private bool horizontalGroundJumpCheck;

    private cAlert_Flag flagAlert;
    public override void SetIsGrounded(bool pGrounded)
    {
        if (isGrounded.Equals(pGrounded))
            return;

        isGrounded = pGrounded;

        if (GetIsGrounded().Equals(true))
        {
            if (isJumpStart.Equals(false))
                jumpCount = 0;

            isJumpStart = false;
            if (jumpCount > 1)
                jumpCount = 0;
        }
    }
    public GameObject attackBox;
    public Vector3[] attackBoxPos; //오른, 아래, 왼, 위
    public GameObject doubleAttackBox;

    private IEnumerator cor_knockBack;
    protected Animator _animator;

    //이펙트
    public ParticleSystem[] effects;

    //사운드
    public cSoundMng sm;

    public virtual void Init(string pNickName, cProperty pDamage, float pMaxMoveSpeed, cProperty pMaxHp, cProperty pCurHp)
    {
        nickName = pNickName;
        damage = new cProperty(pDamage);
        damage_crit = new cProperty(pDamage);
        damage_crit.value = (long)(damage.value * 1.5f);
        maxMoveSpeed = pMaxMoveSpeed;
        curMoveSpeed = 0;
        maxHp = new cProperty(pMaxHp);
        curHp = new cProperty(pCurHp);
        jumpHeight = 200.0f;
        SetHp();
        dir = Vector3.right;
        attackBoxPos = new Vector3[4];
        status = CHARACTERSTATUS.NONE;
        defaultGravity = 500;

        materials = new Material[2];
        materials[0] = Resources.Load<Material>("Material/m_Character");
        materials[1] = Resources.Load<Material>("Material/m_CharacterHit");
        
        //막보스
        if (originObj.name.Equals("Monster_Boss_Devil"))
        {
            canChg_mats_forFinalBoss = this.transform.GetComponent<SpriteRenderer>();
        }
        else
        {
            for (byte i = 0; i < this.transform.GetChild(0).childCount; i++)
            {
                if (this.transform.GetChild(0).GetChild(i).GetComponent<Anima2D.SpriteMeshInstance>() != null)
                    canChg_mats.Add(this.transform.GetChild(0).GetChild(i).GetComponent<Anima2D.SpriteMeshInstance>());
            }
        }
    }

    public cProperty GetMaxHp() { return maxHp; }
    public cProperty GetCurHP() { return curHp; }
    public void modifyInfo(long pHp, long pDamage)
    {
        maxHp.value = pHp;
        curHp.value = maxHp.value;
        damage.value = pDamage;
        SetHp();
    }
    public cProperty GetMaxDp() { return maxDp; }
    public cProperty GetCurDp() { return curDp; }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        if (tileMng != null)
        {
            tileMng.CheckCanGroundTile(this);
        }
    }

    public cProperty GetDamage() { return damage; }

    public float GetDashCoolDown() { return dashCoolDown; }
    public float GetMaxDashCoolDown() { return maxDashCoolDown; }

    public CHARACTERSTATUS GetStatus() { return status; }
    public void SetStatus(CHARACTERSTATUS pCs) { status = pCs; }

    public Vector3 GetDirection() { return dir; }
    public void SetDir(Vector3 pDir) { dir = pDir; }
    public void SetDir(Vector3 pDir, CHARDIRECTION pCharDir) { dir = pDir; charDir = pCharDir; }

    public float GetMaxMoveSpeed() { return maxMoveSpeed; }
    public void SetMaxMoveSpeed(float pMaxMoveSpeed) { maxMoveSpeed = pMaxMoveSpeed; }

    public float GetCurMoveSpeed() { return curMoveSpeed; }
    public virtual void SetCurMoveSpeed(float pCurMoveSpeed) { curMoveSpeed = pCurMoveSpeed; }

    //점프 코루틴
    protected virtual IEnumerator Jump()
    {
        bool goBreak = false;
        if (jumpCount > 2)
        {
            jumpCount = 0;
            goBreak = true;
        }
        SetIsClimbing(false);

        float jumpTimer = 0;
        float factor;

        float currentHeight = originObj.transform.position.y;

        effects[0].Play();
        sm.playEffect(1);
        while (true)
        {
            if (goBreak || isClimbing)
            {
                break;
            }
            yield return new WaitForFixedUpdate();

            //Debug.Log("JUMPING" + jumpHeight);
                                    
            status = CHARACTERSTATUS.JUMP;
            isGrounded = false;

            factor = Mathf.PI * (jumpTimer / jumpTime) * 0.5f;

            originObj.transform.position = new Vector3(originObj.transform.position.x,
                currentHeight + jumpHeight * Mathf.Sin(factor), originObj.transform.position.z);

            if (jumpTimer >= jumpTime)
            {
                changingGravity = defaultGravity;
                break;
            }
            if (isUpBlocked == true)
            {
                changingGravity = defaultGravity;
                break;
            }
            jumpTimer += Time.deltaTime;
        }

        status = CHARACTERSTATUS.NONE;
    }

    // 대쉬 코루틴
    IEnumerator Dash()
    {
        yield return null;

        status = CHARACTERSTATUS.DASH;
        float DashTimer = 0;
        float factor = 0;
        Vector3 dashDir = new Vector3(-originObj.transform.localScale.x, 0, 0);

        //이펙트
        effects[6].gameObject.SetActive(true);
        
        isDash = true;
        _animator.SetTrigger("isDash");

        while (true)
        {
            yield return new WaitForFixedUpdate();

            dir = dashDir;

            factor = Mathf.PI * (DashTimer / dashTime);

            if (curMoveSpeed != 0)
                curMoveSpeed = maxMoveSpeed + dashMoveSpeed * Mathf.Sin(factor);
            else
                curMoveSpeed = dashMoveSpeed * Mathf.Sin(factor);

            if (curMoveSpeed > dashMoveSpeed)
                curMoveSpeed = dashMoveSpeed;

            DashTimer += Time.deltaTime;

            //타일에 붙는 행동 처리
            if(isRightBlocked.Equals(true) && dir.Equals(Vector3.right))
            {
                if(isGrounded.Equals(false))
                {
                    SetIsClimbing(true);
                    jumpCount = 0;
                    curMoveSpeed = 0;
                    isDash = false;
                    dashCoolDown = 0;
                    StartCoroutine(StopDashEffect_cor());
                    break;
                }
            }
            if (isLeftBlocked.Equals(true) && dir.Equals(Vector3.left))
            {
                if (isGrounded.Equals(false))
                {
                    SetIsClimbing(true);
                    jumpCount = 0;
                    curMoveSpeed = 0;
                    isDash = false;
                    dashCoolDown = 0;
                    StartCoroutine(StopDashEffect_cor());
                    break;
                }
            }

            if (DashTimer > dashTime)
            {
                if (charDir.Equals(CHARDIRECTION.RIGHT) || charDir.Equals(CHARDIRECTION.LEFT))
                {
                    if(joystick.isDrag.Equals(true))
                    {
                        curMoveSpeed = maxMoveSpeed;
                    }
                }
                else
                    curMoveSpeed = 0;

                StartCoroutine(StopDashEffect_cor());
                isDash = false;
                break;
            }
        }

        status = CHARACTERSTATUS.NONE;
    }

    IEnumerator StopDashEffect_cor()
    {
        yield return new WaitForSeconds(0.2f);

        effects[6].gameObject.SetActive(false);
    }

    IEnumerator DashCoolDown()
    {
        yield return null;

        obj_coolTime.SetActive(true);
        t_coolTime.gameObject.SetActive(true);
        t_coolTime.text = ((int)maxDashCoolDown).ToString();
        
        while (true)
        {
            yield return new WaitForFixedUpdate();
            
            dashCoolDown -= Time.deltaTime;
            t_coolTime.text = ((int)dashCoolDown + 1).ToString();

            if (dashCoolDown <= 0)
            {
                obj_coolTime.SetActive(false);
                t_coolTime.gameObject.SetActive(false);
                break;
            }
        }

        dashCoolDown = maxDashCoolDown;
    }

    IEnumerator JetPack()
    {
        float currentHeight = originObj.transform.position.y;

        while (isJetPackOn)
        {
            yield return new WaitForFixedUpdate();
            currentHeight += 5;
            originObj.transform.position = new Vector2(originObj.transform.position.x, currentHeight);

            if (isUpBlocked)
            {
                originObj.transform.position = new Vector2(originObj.transform.position.x, originObj.transform.position.y - 15.0f);
                break;
            }
        }
    }

    public virtual void SetJetPackOn()
    {
        isJetPackOn = true;
    }

    public virtual void SetJetPackOff()
    {
        isJetPackOn = false;
    }

    public virtual void SetGravity()
    {
        if (!isGrounded && !status.Equals(CHARACTERSTATUS.JUMP))
        {
            originObj.transform.Translate(Vector3.down * changingGravity * Time.deltaTime);

            if (changingGravity <= maxGravity)
                changingGravity *= 1.02f;
            if (changingGravity > maxGravity)
                changingGravity = maxGravity;

        }
        else
            changingGravity = defaultGravity;
    }

    //public virtual void ReduceHp(long pVal)
    //{
    //    Debug.Log(this.name + " hp : " + this.curHp.value);
    //    curHp.value -= pVal;

    //    if (curHp.value < 0)
    //        curHp.value = 0;

    //    hitCor = HitEffect_cor();
    //    StartCoroutine(hitCor);

    //    SetHp();
    //}

    public virtual void ReduceHp(long pVal, Vector3 pDir, float pVelocity = 7.5f, float pDelay = 0)
    {
        if(pDelay != 0)
        {
            delayHit = DelayHit_cor(pVal, pDir, pVelocity, pDelay);
            StartCoroutine(delayHit);
        }

        curHp.value -= pVal;

        if (curHp.value < 0)
            curHp.value = 0;


        hitCor = HitEffect_cor();
        StartCoroutine(hitCor);
        SetHp();
        
        /* 조건 왜 있는지 몰라서 지움 */
        //if (curHp.value >= 0)
        //{
            StopCoroutine(KnockBack(pDir, pVelocity));
            StartKnockBack(pDir, pVelocity);
        //}

        if (originObj.tag.Equals("Player"))
        {
            _animator.SetTrigger("getHit");
            SetStatus(CHARACTERSTATUS.NONE);
        }
    }

    public virtual void RestoreHp(long pVal, bool toFool = false)
    {
        if (toFool)
            curHp.value = maxHp.value;
        else
            curHp.value += pVal;

        if (curHp.value > maxHp.value)
            curHp.value = maxHp.value;
        
        SetHp();
    }

    public virtual void SetHp()
    {
        if (img_curHp == null)
            return;

        if(hpCor != null)
            StopCoroutine(hpCor);

        hpCor = HpInterpolation();
        StartCoroutine(hpCor);
        t_hp.text = string.Format("{0} / {1}", cUtil.GetValueToString(curHp.value), cUtil.GetValueToString(maxHp.value));
    }

    public virtual void SetDp()
    {
        if (originObj.tag.Equals("Player"))
        {
            if(dpCor != null)
                StopCoroutine(dpCor);

            dpCor = DpInterpolation();            
            StartCoroutine(dpCor);
            t_dp.text = string.Format("{0} / {1}", cUtil.GetValueToString(curDp.value), cUtil.GetValueToString(maxDp.value)); 
        }
    }

    protected void StartKnockBack(Vector3 pDir, float pVelocity = 7.5f)
    {
        if (_knockback != null)
            StopCoroutine(_knockback);

        if (pDir.Equals(Vector3.zero))
            return;

        _knockback = KnockBack(pDir, pVelocity);
        StartCoroutine(_knockback);
    }


    protected IEnumerator KnockBack(Vector3 pDir, float pVelocity = 7.5f)
    {
        CHARACTERSTATUS prevStatus = status; // 넉백이 끝나고 되돌릴 캐릭터 상태
        float velocity = pVelocity; // 넉백 속도

        Vector3 attackerDir;
        Vector3 currentPos = originObj.transform.position;

        if (pDir.x <= 0)
        {
            attackerDir = Vector3.left;
        }
        else
        {
            attackerDir = Vector3.right;
        }

        while (true)
        {
            status = CHARACTERSTATUS.KNOCKBACK; // 넉백상태로 스테이터스 변경
            yield return new WaitForFixedUpdate();

            float curPosY = originObj.transform.position.y;

            if (velocity <= 0)
                break;

            // 옆이 막힌 경우 넉백 종료
            if (isLeftBlocked)
            {
                originObj.transform.position = new Vector3(currentPos.x + 1f, curPosY, currentPos.z);
                break;
            }
            else if (isRightBlocked)
            {
                originObj.transform.position = new Vector3(currentPos.x - 1f, curPosY, currentPos.z);
                break;
            }

            // 피격자 기준 등 뒤에서 맞은 경우
            if (attackerDir == dir)
            {
                originObj.transform.position = new Vector3(currentPos.x + dir.x * velocity, curPosY, currentPos.z);
            }
            // 피격자 기준 정면에서 맞은 경우
            else
            {
                originObj.transform.position = new Vector3(currentPos.x + (-dir.x) * velocity, curPosY, currentPos.z);
            }

            currentPos = originObj.transform.position;
            velocity -= 0.5f;
        }

        status = prevStatus;
        Debug.Log("넉백 종료");
    }

    public bool GetIsClimbing() { return isClimbing; }
    public void SetIsClimbing(bool pbool)
    {
        if (isClimbing.Equals(pbool))
            return;

        if (isClimbing.Equals(false))
        {
            _animator.SetTrigger("Crawl");
        }

        isClimbing = pbool;
        _animator.SetBool("isCrawl", isClimbing);
    }

    protected IEnumerator HpInterpolation()
    {
        float prevAmount = img_curHp.fillAmount;
        float curAmount = (float)curHp.value / (float)maxHp.value;
        float tick = (curAmount - prevAmount) / 10f;

        while (true)
        {
            yield return new WaitForFixedUpdate();
            img_curHp.fillAmount = img_curHp.fillAmount + tick;
            if (Mathf.Abs(img_curHp.fillAmount - curAmount) <= Mathf.Abs(tick))
            {
                img_curHp.fillAmount = curAmount;
                break;
            }
        }
    }

    protected IEnumerator DpInterpolation()
    {
        float prevAmount = img_curDp.fillAmount;
        float curAmount = (float)curDp.value / (float)maxDp.value;
        float tick = (curAmount - prevAmount) / 10f;

        while (true)
        {
            yield return new WaitForFixedUpdate();
            img_curDp.fillAmount = img_curDp.fillAmount + tick;
            if (Mathf.Abs(img_curDp.fillAmount - curAmount) <= Mathf.Abs(tick))
            {
                img_curDp.fillAmount = curAmount;
                break;
            }
        }
    }

    protected IEnumerator HitEffect_cor()
    {
        if(originObj.gameObject.name.Equals("Monster_Boss_Devil"))
        {
            canChg_mats_forFinalBoss.material = materials[1];

            yield return new WaitForSeconds(0.1f);

            canChg_mats_forFinalBoss.material = materials[0];
        }
        else
        {
            for (byte i = 0; i < canChg_mats.Count; i++)
            {
                canChg_mats[i].sharedMaterial = materials[1];
            }

            yield return new WaitForSeconds(0.1f);

            for (byte i = 0; i < canChg_mats.Count; i++)
            {
                canChg_mats[i].sharedMaterial = materials[0];
            }
        }
        
    }

    protected IEnumerator DelayHit_cor(long pVal, Vector3 pDir, float pVelocity, float pDelay)
    {
        yield return new WaitForSeconds(pDelay);

        ReduceHp(pVal, pDir, pVelocity, 0);
    }

}
