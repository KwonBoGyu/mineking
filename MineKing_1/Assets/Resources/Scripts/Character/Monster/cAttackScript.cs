﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cAttackScript : MonoBehaviour
{
    private long damage;
    private Vector3 dir;
    public cEnemy_monster em;

    public void SetAttackParameter(long pDamage, Vector3 pDir)
    {
        damage = pDamage;
        dir = pDir;
        Debug.Log("damage : " + damage);
        Debug.Log("dir : " + dir);
    }
    
    public void Attack()
    {
        if (cUtil._player.originObj.transform.position.x >= em.originObj.transform.position.x)
            em.ChangeDir(Vector3.right);

        else if (cUtil._player.originObj.transform.position.x < em.originObj.transform.position.x)
            em.ChangeDir(Vector3.left);

        if (em.isInAttackRange.Equals(true))
        {
            cUtil._player.ReduceHp(damage, dir);
            Debug.Log("damage : " + damage +  " / Dir : " + dir);
        }
    }

    public void Dead()
    {
        this.transform.parent.localPosition = new Vector3(-1760, 1000, 1);
    }
}
