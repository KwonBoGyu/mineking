﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cEnemy_bossAttackBox : MonoBehaviour
{
    protected cEnemy_monster script;
    protected long damage;
    public void SetDamage(long pDamage) { damage = pDamage; }

    public virtual void Init(long pDamage)
    {
        script = this.transform.parent.GetChild(0).GetComponent<cEnemy_monster>();
        damage = pDamage;
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))
        {
            cUtil._player.ReduceHp(damage, script.GetDirection());
            this.gameObject.SetActive(false);
        }
    }
}
