﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cBullet : cProjectile
{
    public bool isInit;

    protected CircleCollider2D rangeCollider;
    protected float attackRange;

    public BULLET_TYPE type;
    protected float distance;
    public float damage;
    protected bool isCollide;
    // type : split인 경우 몇 번 분열했는지 카운트
    protected int splitCount;
    // type : grenade인 경우 폭발 관리를 위한 변수
    protected bool explodeOn;
    protected float explodeTime;
    
    public virtual void Init(BULLET_TYPE pType, float pPower, bool pUseGravity, Vector3 pDir, long pDamage,
        Vector3 pInitPos, float pGravity = 7.0f)
    {
        this.transform.position = pInitPos;
        //this.transform.localScale = new Vector3(1, 1, 1);

        type = pType;
        //rangeCollider = parentMonster.GetComponent<cEnemy_monster>().notizer.GetComponent<CircleCollider2D>();
        //attackRange = rangeCollider.radius;
        damage = pDamage;

        splitCount = 0;
        explodeOn = false;
        explodeTime = 3.0f;
        isCollide = false;
        upBlockedContinue = false;
        gravityAmount = pGravity;

        flyingTime = 0;
        defaultPower = pPower;
        changingPower = defaultPower;
        isReflectOn = true;
        isGravityOn = pUseGravity;
        SetDir(pDir);

        isInit = true;
    }

    public void SetTarget(Vector3 pTarget)
    {
        dir = (pTarget - this.originObj.transform.position).normalized;
    }

    public void SetType(BULLET_TYPE pType)
    {
        type = pType;

        //switch (pType)
        //{
        //    case BULLET_TYPE.NORMAL:
        //        maxSpeed = 300f;
        //        curSpeed = maxSpeed;
        //        break;
                
        //    case BULLET_TYPE.SPLIT:
        //        maxSpeed = 300f;
        //        curSpeed = maxSpeed;
        //        break;
                
        //    case BULLET_TYPE.GRENADE:
        //        maxSpeed = 500f;
        //        curSpeed = maxSpeed;
        //        break;
        //}
    }

    protected override void FixedUpdate()
    {
        if (isInit.Equals(false))
            return;

        if (dir.Equals(Vector3.zero))
            return;

        Move();

        //distance = new Vector2(this.transform.position.x - originObj.transform.position.x,
        //    this.transform.position.y - originObj.transform.position.y).magnitude;

        //// 총알 최대 범위 이상으로 벗어나면 소멸
        //if (distance >= attackRange)
        //{
        //    this.gameObject.SetActive(false);
        //}
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (isInit.Equals(false))
            return;

        if (collision.gameObject.tag.Equals("Tile_canHit"))
        {
            isCollide = true;
        }
        else if (collision.gameObject.tag.Equals("Tile_CannotHit"))
        {
            isCollide = true;
        }

        else if (collision.gameObject.tag.Equals("Player"))
        {                    
            cUtil._player.ReduceHp((long)damage, dir);
            this.gameObject.SetActive(false);
        }
        else if (collision.gameObject.tag.Equals("Tile_canHit"))
        {
            this.gameObject.SetActive(false);
        }
        else if (collision.gameObject.tag.Equals("Tile_cannotHit"))
        {
            this.gameObject.SetActive(false);
        }
    }
    
    IEnumerator SetGrenade()
    {
        yield return new WaitForSeconds(explodeTime);
        explodeOn = true;
        rangeCollider.radius = rangeCollider.radius * 3; // 충돌 범위 확장
        this.transform.localScale = new Vector3(3, 3, 3); // 임시적 시각효과

        yield return new WaitForSeconds(0.3f);
        this.gameObject.SetActive(false); // 0.3초 후 삭제
    }
}
