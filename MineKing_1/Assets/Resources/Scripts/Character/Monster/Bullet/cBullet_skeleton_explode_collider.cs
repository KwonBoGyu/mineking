﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cBullet_skeleton_explode_collider : MonoBehaviour
{
    public cBullet bullet;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag.Equals("Player"))
        {
            cUtil._player.ReduceHp((long)bullet.damage,Vector3.zero);
        }
    }
}
