﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cBullet_boss_hood : cBullet
{
    public cExplodeEffect explodeEffect;
    float fxAngle;

    public override void Init(BULLET_TYPE pType, float pPower, bool pUseGravity, Vector3 pDir, long pDamage, Vector3 pInitPos, float pGravity = 7)
    {
        base.Init(pType, pPower, pUseGravity, pDir, pDamage, pInitPos, pGravity);
        this.rt.enabled = true;
        fxAngle = Vector3.SignedAngle(transform.up, -pDir, -transform.forward);
        originObj.transform.GetChild(0).transform.rotation = Quaternion.AngleAxis(fxAngle, Vector3.forward);
        explodeEffect.originObj.SetActive(false);
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        if (isInit.Equals(false))
        {
            return;
        }

        if (collision.gameObject.tag.Equals("Player"))
        {
            cUtil._player.ReduceHp((long)damage,Vector3.zero);
            this.gameObject.SetActive(false);
        }
        else if (collision.gameObject.tag.Equals("Tile_canHit"))
        {
            dir = Vector3.zero;
            this.rt.enabled = false;
            explodeEffect.originObj.SetActive(true);
            explodeEffect.Init(damage);
        }
        else if (collision.gameObject.tag.Equals("Tile_cannotHit"))
        {
            dir = Vector3.zero;
            this.rt.enabled = false;
            explodeEffect.originObj.SetActive(true);
            explodeEffect.Init(damage);
        }
    }
}
