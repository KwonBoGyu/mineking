﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cBullet_boss_goblin : cBullet
{
    public cExplodeEffect explodeEffect;

    public override void Init(BULLET_TYPE pType, float pPower, bool pUseGravity, Vector3 pDir, long pDamage, Vector3 pInitPos, float pGravity = 7)
    {
        base.Init(pType, pPower, pUseGravity, pDir, pDamage, pInitPos, pGravity);
        this.rt.enabled = true;
        explodeEffect.originObj.SetActive(false);
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        if(isInit.Equals(false))
        {
            return;
        }

        if(collision.gameObject.tag.Equals("Player"))
        {
            cUtil._player.ReduceHp((long)damage, Vector3.zero);
            this.gameObject.SetActive(false);
        }
        else if(collision.gameObject.tag.Equals("Tile_canHit")) 
        {
            dir = Vector3.zero;
            this.rt.enabled = false;
            explodeEffect.originObj.SetActive(true);
            explodeEffect.Init(damage);
        }
        else if(collision.gameObject.tag.Equals("Tile_cannotHit"))
        {
            dir = Vector3.zero;
            this.rt.enabled = false;
            explodeEffect.originObj.SetActive(true);
            explodeEffect.Init(damage);
        }
    }
}
