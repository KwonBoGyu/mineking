﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cBullet_boss_goblin_bomb : cBullet
{
    private long damage;
    private Animator _animator;
    private Vector2 targetPos;
    public cBulletManager bulletMng; // 다이너마이트 있는 bulletMng 오브젝트 캐싱

    public void SetTargetPos(Vector3 pTargetPos) { targetPos = pTargetPos; }
    public void SetDamage(long pDamage) { damage = pDamage; }

    private WaitForSeconds idleTime = new WaitForSeconds(0.5f);
    private WaitForSeconds wf = new WaitForSeconds(0.1f);

    public override void Init(BULLET_TYPE pType, float pPower, bool pUseGravity, Vector3 pDir, long pDamage, Vector3 pInitPos, float pGravity = 7)
    {
        base.Init(pType, pPower, pUseGravity, pDir, pDamage, pInitPos, pGravity);

        _animator = this.transform.GetChild(0).GetComponent<Animator>();
        _animator.SetTrigger("Init");
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        if(isInit)
        {
            if (Vector2.Distance(targetPos, this.originObj.transform.position) <= 20.0f)
            {
                LaunchDynamite();
                isInit = false;
            }
        }
    }
    
    private void LaunchDynamite()
    {
        StartCoroutine(LaunchDynamite_cor());
    }

    IEnumerator LaunchDynamite_cor()
    {
        yield return idleTime;

        _animator.SetTrigger("Explode");

        byte dynamiteNum = (byte)Random.Range(10, 15);
        float randomX, randomY;
        Vector2 randomTarget;

        for (byte i = 0; i < dynamiteNum; i++)
        {
            randomX = this.originObj.transform.position.x + Random.Range(-50f, 50f);
            randomY = this.originObj.transform.position.y + Random.Range(-25f, 50f);
            randomTarget = new Vector2(randomX, randomY);

            bulletMng.LaunchBullet(this.originObj.transform.position,
                true,
                randomTarget,
                damage,
                10.0f,
                9f);

            yield return new WaitForSeconds(0.1f);
        }

        this.originObj.SetActive(false);
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        if (isInit.Equals(false))
            return;

        if (collision.gameObject.tag.Equals("Tile_canHit"))
        {
            isCollide = true;
        }
        else if (collision.gameObject.tag.Equals("Tile_CannotHit"))
        {
            isCollide = true;
        }
    }
}
