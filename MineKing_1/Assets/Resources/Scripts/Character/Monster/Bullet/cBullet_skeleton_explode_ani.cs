﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cBullet_skeleton_explode_ani : MonoBehaviour
{
    private cBullet_skeleton_explode bullet;

    private void Start()
    {
        bullet = this.transform.parent.GetComponent<cBullet_skeleton_explode>();
    }

    public void SetInit()
    {
        bullet.isInit = true;
        bullet.GetComponent<BoxCollider2D>().enabled = true;

        bullet.gameObject.transform.up = bullet.dir;
    }

    public void BreakObject()
    {
        bullet.isInit = false;
        bullet.gameObject.SetActive(false);
    }
}
