﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cBullet_boss_slime : cBullet
{
    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        if (isInit.Equals(false))
            return;

        if (collision.gameObject.tag.Equals("Tile_canHit"))
        {
            isCollide = true;
        }
        else if (collision.gameObject.tag.Equals("Tile_CannotHit"))
        {
            isCollide = true;
        }

        else if (collision.gameObject.tag.Equals("Player"))
        {
            cUtil._player.SetPoison(1, 5f);
            cUtil._player.ReduceHp(1, dir);
            this.gameObject.SetActive(false);
        }
        else if (collision.gameObject.tag.Equals("Tile_canHit"))
        {
            this.gameObject.SetActive(false);
        }
        else if (collision.gameObject.tag.Equals("Tile_cannotHit"))
        {
            this.gameObject.SetActive(false);
        }
    }
}
