﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cBullet_skeleton : cBullet
{    
    private Animator _animator;

    public override void Init(BULLET_TYPE pType, float pPower, bool pUseGravity, Vector3 pDir, long pDamage,
        Vector3 pInitPos, float pGravity = 7.0f)
    {
        this.transform.position = pInitPos;

        type = pType;
        damage = pDamage;

        splitCount = 0;
        explodeOn = false;
        explodeTime = 3.0f;
        isCollide = false;
        upBlockedContinue = false;
        gravityAmount = pGravity;

        flyingTime = 0;
        defaultPower = pPower;
        changingPower = defaultPower;
        isReflectOn = true;
        isGravityOn = pUseGravity;
        SetDir(pDir);

        _animator = this.transform.GetChild(0).GetComponent<Animator>();
        _animator.SetTrigger("Init");

        this.gameObject.transform.up = new Vector3(0, 1, 0);
    }
    
    protected override void FixedUpdate()
    {
        if (isInit.Equals(false))
            return;

        if (dir.Equals(Vector3.zero))
            return;

        Move();
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        if (isInit.Equals(false))
            return;
        
        if (collision.gameObject.tag.Equals("Player"))
        {
            cUtil._player.ReduceHp((long)damage, dir);
            _animator.SetTrigger("Break");
            this.GetComponent<BoxCollider2D>().enabled = false;
            isInit = false;
        }
        else if (collision.gameObject.tag.Equals("Tile_canHit"))
        {
            _animator.SetTrigger("Break");
            this.GetComponent<BoxCollider2D>().enabled = false;
            isInit = false;
        }
        else if (collision.gameObject.tag.Equals("Tile_cannotHit"))
        {
            _animator.SetTrigger("Break");
            this.GetComponent<BoxCollider2D>().enabled = false;
            isInit = false;
        }
    }

}

