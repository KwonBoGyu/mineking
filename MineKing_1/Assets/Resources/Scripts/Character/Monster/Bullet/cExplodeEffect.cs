﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cExplodeEffect : MonoBehaviour
{
    public Animator _animator;
    public GameObject motherObj;
    public GameObject originObj;
    private float damage;

    public void Init(float pDamage)
    {
        _animator.SetTrigger("StartExplode");
        damage = pDamage;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag.Equals("Player"))
        {
            cUtil._player.ReduceHp((long)damage,Vector3.zero);
        }
    }

    public void ExplodeDone_ani()
    {
        motherObj.SetActive(false);
    }
}
