﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cBoss_theme1_stage1_JumpAttackBox : MonoBehaviour
{
    private cBoss_theme1_stage1 script;

    public void Init()
    {
        script = this.transform.parent.GetChild(0).GetComponent<cBoss_theme1_stage1>();
        script.SetIsInJumpAttackRange(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag.Equals("Player"))
        {
            script.SetIsInJumpAttackRange(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.gameObject.tag.Equals("Player"))
        {
            script.SetIsInJumpAttackRange(false);
        }
    }
}
