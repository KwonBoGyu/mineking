﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cBoss_theme1_stage1_sub : cEnemy_monster
{
    //Pattern1 변수
    private byte curNormalAttackCount; //n번 일반 공격 시 총알 난사
    private bool isSpoilBullet;
    private byte curBulletSpoilCount; //n번 총알 난사 시 궁극기
    private bool isSmashGround;
    public Transform smashGroundPos;
    private bool isMeltingAttack;
    private bool isMeltingAttackReady;
    private bool isMeltedOnce;

    private bool isNormalAttack;
    private const float normalAttackCoolTime = 3.0f;
    private float normalAttackCoolTimer;

    //Pattern2 변수
    public cBoss_theme1_stage1 prevBoss;

    private Vector3 NormalNearAttackBoxPos;//local

    float randomX;
    float randomY;
    float randomX2;
    float randomY2;

    public void InitBoss()
    {
        base.Init(cEnemyTable.SetMonsterInfo(51));


        bulletDamage = 2;
        NormalNearAttackBoxPos = new Vector3(-54, 39, -1);
    }

    protected override void FixedUpdate()
    {
        if (isDead.Equals(true))
            return;

        if (isInit.Equals(true))
        {
            base.FixedUpdate();
        }
    }

    protected override void Move()
    {
        if (_animator != null)
            _animator.SetFloat("MoveSpeed", curMoveSpeed);

        //막히면 방향 바꿔준다.
        if (isRightBlocked == true)
        {
            isRightBlocked = false;
            ChangeDir(Vector3.left);
        }
        else if (isLeftBlocked == true)
        {
            isLeftBlocked = false;
            ChangeDir(Vector3.right);
        }

        //패턴
        if (curPatternId.Equals(0))
            Pattern1();
    }

    private void Pattern1()
    {
        // 인식 범위 내 진입
        //isInAttackRange : 근접공격범위
        if (isInNoticeRange.Equals(true))
        {
            playerPos = cUtil._player.gameObject.transform.position;

            if (playerPos.x >= this.transform.position.x)
                ChangeDir(Vector3.right);

            else if (playerPos.x < this.transform.position.x)
                ChangeDir(Vector3.left);

            //총알 난사 공격을 2회 했을 시 스매쉬
            if (curBulletSpoilCount.Equals(2) && isSpoilBullet.Equals(false) && isMeltingAttack.Equals(false))
            {
                SmashGround();
                return;
            }
            //일반 공격을 5회 했을 시 총알 난사
            else if (curNormalAttackCount.Equals(3) && isNormalAttack.Equals(false) && isSmashGround.Equals(false) && isMeltingAttack.Equals(false))
            {
                SpoilBullet();
                return;
            }
            else if (isNormalAttack.Equals(false) && isSpoilBullet.Equals(false) && isSmashGround.Equals(false) &&
                isMeltingAttack.Equals(false) && isMeltingAttackReady.Equals(true) && isMeltedOnce.Equals(false))
            {
                MeltingAttack();
                return;
            }
            //일반 원거리 공격
            else if (isInAttackRange.Equals(false) && isSpoilBullet.Equals(false) && isSmashGround.Equals(false) && isMeltingAttack.Equals(false))
            {
                //공격 쿨타임 돌린다.
                normalAttackCoolTimer += Time.deltaTime;
                if (normalAttackCoolTimer >= normalAttackCoolTime)
                {
                    NormalAttack(false);
                    normalAttackCoolTimer = 0;
                }
            }
            //일반 근접 공격
            else if (isInAttackRange.Equals(true) && isSpoilBullet.Equals(false) && isSmashGround.Equals(false) && isMeltingAttack.Equals(false))
            {
                //공격 쿨타임 돌린다.
                normalAttackCoolTimer += Time.deltaTime;
                if (normalAttackCoolTimer >= normalAttackCoolTime)
                {
                    NormalAttack(true);
                    normalAttackCoolTimer = 0;
                }
            }
            //Debug.Log("meltingAttack : " + isMeltingAttack);
            //Debug.Log("isMeltedOnce : " + isMeltedOnce);
            //Debug.Log("isMeltingAttackReady : " + isMeltingAttackReady);
        }
    }

    private void Pattern2()
    {

    }

    private void SmashGround()
    {
        _animator.SetTrigger("SmashGround");
        curBulletSpoilCount = 0;
        isSmashGround = true;
    }

    public void SmashGround_ani()
    {
        for (byte i = 0; i < 8; i++)
        {
            randomX = Random.Range(-100, 100);
            randomY = Random.Range(50, 100);
            randomX2 = Random.Range(-200, 200);
            randomY2 = Random.Range(100, 150);

            bulletManager.LaunchBullet(
                new Vector3(smashGroundPos.position.x + randomX,
                smashGroundPos.position.y + 10,
                smashGroundPos.position.z),
                true,
                new Vector3(smashGroundPos.position.x + randomX2,
                smashGroundPos.position.y + randomY2,
                smashGroundPos.position.z),
                bulletDamage, 15.0f);
        }
    }

    public void SmashDone_ani()
    {
        curNormalAttackCount = 0;
        curBulletSpoilCount = 0;
        isNormalAttack = false;
        isSpoilBullet = false;
        isSmashGround = false;
        _animator.SetBool("NormalNearAttack", false);
        _animator.SetBool("NormalFarAttack", false);
        _animator.SetBool("SpoilBullet", false);
    }

    private void SpoilBullet()
    {
        _animator.SetTrigger("SpoilBullet");
        curNormalAttackCount = 0;

        isSpoilBullet = true;
    }

    public void SpoilBullet_ani()
    {
        randomY = Random.Range(-30, 30);
        bulletManager.LaunchBullet(originObj.transform.position, true,
            new Vector3(cUtil._player.originObj.transform.position.x,
            cUtil._player.originObj.transform.position.y + randomY,
            cUtil._player.originObj.transform.position.z),
            bulletDamage, 20.0f);
    }

    public void SpoilBulletDone_ani()
    {
        curBulletSpoilCount += 1;
        isSpoilBullet = false;
    }

    private void MeltingAttack()
    {
        isMeltingAttack = true;
        isMeltedOnce = true;
        _animator.SetTrigger("MeltingStart");
    }
    private void Melting_ani()
    {
        //Debug.Log("func_Melted_ani");
        _animator.SetTrigger("IsMelted");
        StartCoroutine(Melted_cor());
    }
    IEnumerator Melted_cor()
    {
        curMoveSpeed = maxMoveSpeed * 2;

        byte curAniIdx = 0;
        float meltedTime = Random.Range(5.0f, 7.0f);
        float timer = 0;

        while(true)
        {
            yield return new WaitForFixedUpdate();
            if(timer >= meltedTime)
            {
                break;
            }
            if ((timer >= meltedTime / 3) && curAniIdx.Equals(0))
            {
                _animator.SetTrigger("MeltingStep" + curAniIdx);
                curAniIdx += 1;
            }
            else if((timer >= (meltedTime * 2)/3) && curAniIdx.Equals(1))
            {
                _animator.SetTrigger("MeltingStep" + curAniIdx);
                curAniIdx += 1;
            }
            
            timer += Time.deltaTime;

            if(Mathf.Abs(cUtil._player.originObj.transform.position.x - this.originObj.transform.position.x) >= 25f)
            {
                this.originObj.transform.Translate(curMoveSpeed * dir * Time.deltaTime);
            }
        }
        
        curMoveSpeed = 0;
        //Debug.Log("cor end ");
        _animator.SetTrigger("MeltingEnd");
    }
    // NormalNearAttack_ani와 같은 기능
    private void MeltingAttack_ani()
    {
        if (isInAttackRange.Equals(true))
        {
            cUtil._player.ReduceHp(damage.value, dir);
        }
    }
    private void MeltingAttackDone_ani()
    {
        isMeltingAttackReady = false;
        isMeltingAttack = false;
    }

    private void NormalAttack(bool isNear)
    {
        if (isNear.Equals(true))
        {
            _animator.SetTrigger("NormalNearAttack");
        }
        else
        {
            _animator.SetTrigger("NormalFarAttack");
        }

        isNormalAttack = true;
    }

    public void NormalNearAttack_ani()
    {
        if (isInAttackRange.Equals(true))
        {
            cUtil._player.ReduceHp(damage.value, dir);
        }
    }

    public void NormalFarAttack_ani()
    {
        bulletManager.LaunchBullet(originObj.transform.position, true,
            cUtil._player.originObj.transform.position,
            bulletDamage, 15.0f);
    }

    public void NormalAttackDone_ani()
    {
        curNormalAttackCount += 1;
        isNormalAttack = false;
    }

    public void Dead()
    {
        originObj.transform.position = new Vector3(-10000, -10000, 0);
        prevBoss.Dead();
    }

    // 공격자가 있는 경우 공격자가 바라보는 방향을 pDir로 받음
    public override void ReduceHp(long pVal, Vector3 pDir, float pVelocity = 7.5f, float pDelay = 0)
    {

        if (pDelay != 0)
        {
            delayHit = DelayHit_cor(pVal, pDir, pVelocity, pDelay);
            StartCoroutine(delayHit);
            return;
        }

        cUtil._soundMng.playAxeEffect(2);

        curHp.value -= pVal;

        hitCor = HitEffect_cor();
        StartCoroutine(hitCor);

        if (curHp.value <= 0)
        {
            coolTimer = 0;
            curHp.value = 0;
            isDead = true;
            _animator.SetTrigger("Dead");
            originObj.GetComponent<BoxCollider2D>().enabled = false;
        }

        if (curHp.value <= maxHp.value * 0.3f)
        {
            isMeltingAttackReady = true;
        }

        prevBoss.SlimeKingSetHp();
    }
}