﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cBoss_theme1_stage5 : cEnemy_monster
{
    //Pattern1 변수
    private bool isNormalAttackOrTeleportAttack; // true : 일반 근접공격, false : 텔레포트 공격
    private byte inputTeleportAttackCount; // 텔레포트 할 횟수
    private Vector3 teleportPos_Right;
    private Vector3 teleportPos_Left;
    private byte curTeleportAttackCount; // 텔레포트 애니메이션 이벤트 스크립트에서 횟수 감지를 위해 사용
    private byte curNormalAttackCount; //n번 일반 공격 시 광역 공격
    public bool isNormalAttack;
    private byte curDoubleAttackCount; //n번 2발 일반공격 시 광역기
    public bool isDoubleAttack;
    private byte curBulletSpoilCount; //n번 광역기 시 쫄 소환
    public bool isSpoilDynamite;
    public bool isCharging;
    public bool isGlobalAttackDone;
    private const float dealTime = 5.0f;
    private float dealTimer;

    //Pattern2 변수
    private bool isPattern2Started;
    private bool isPattern2ChangingDone;
    private const float monsterOnCoolTime = 10.0f;
    private float monsterOnCoolTimer;

    private const float normalAttackCoolTime = 3.0f;
    private float normalAttackCoolTimer;

    private Vector3 NormalNearAttackBoxPos;//local
    public GameObject normalAttackBox;

    public Transform bossPos_DuringSummon;
    private bool isSummoning;
    private bool isSummonDone;
    private bool isCloneAllDead;
    private WaitForSeconds waitforSec_Summon = new WaitForSeconds(0.3f);

    public GameObject monstersParent_forUlti;
    public GameObject monstersParent;

    public Transform fxPos_Teleport;
    private bool isHpLowerThan20Percent;
    private bool isTeleport10TimesDone;
    private bool isTeleport10Times;
    private long damageForTeleport10Times;

    public GameObject lazerObjs;
    private bool isLazer;
    private float lazerTimer;
    private float lazerCoolTime;
    public Vector2[] lazerPos;
    private long lazerDamage;
    public long GetLazerDamage() { return lazerDamage; }
    private WaitForFixedUpdate wf = new WaitForFixedUpdate();
    private WaitForSeconds lazerTerm = new WaitForSeconds(1f);

    float randomX;
    float randomY;
    float randomX2;
    float randomY2;

    Vector2 fxDir;
    float fxAngle;

    public void InitBoss()
    {
        base.Init(cEnemyTable.SetMonsterInfo(54));

        bulletDamage = 25;
        NormalNearAttackBoxPos = new Vector3(-54, 39, -1);
        curMoveSpeed = 0;

        normalAttackBox.GetComponent<cEnemy_bossAttackBox>().Init(damage.value);
        normalAttackBox.SetActive(false);
        attackBox.GetComponent<cEnemy_bossAttackBox>().Init(damage.value * 2);
        attackBox.SetActive(false);
        damageForTeleport10Times = (long)(cUtil._player.GetMaxHp().value * 0.05);
        lazerDamage = (long)(damage.value * 3);
        lazerPos = new Vector2[lazerObjs.transform.childCount];
        lazerCoolTime = 10.0f;

        inputTeleportAttackCount = 0;
        curTeleportAttackCount = 0;
        talkText = new string[5];
        talkText[0] = "쿸,,,마계의 지.존 으로 여기까지 온 것을 치하한한,,,달까?(퍽)";
        talkText[1] = "내,,,안의 흑.염.룡(오.른.손)이여,,,,,,,,,,,,,,날뛰어라(섬찟)";
        talkText[2] = "네.놈.이. 마주할 것은-------- '심연'의 '연무',,,, \n휘몰아쳐라,  나의 오.른.손이여. . . . !!!";
        talkText[3] = "지금까지는 '연습'게임에 불과---------(욱신)";
        talkText[4] = "킄, , , 닝겐,  자.만하는가, , , 암.흑.지.존.마.왕(.본좌)은 \n언제든 너희가 테마 2를 원할때 부활할 터이니, , ,!!!";
    }

    protected override void FixedUpdate()
    {
        if (isDead.Equals(true))
            return;

        if (isInit.Equals(true))
        {
            base.FixedUpdate();
        }
    }

    protected override void Move()
    {
        if (_animator != null)
            _animator.SetFloat("MoveSpeed", curMoveSpeed);

        //막히면 방향 바꿔준다.
        if (isRightBlocked == true)
        {
            isRightBlocked = false;
            ChangeDir(Vector3.left);
        }
        else if (isLeftBlocked == true)
        {
            isLeftBlocked = false;
            ChangeDir(Vector3.right);
        }

        //패턴
        if (curPatternId.Equals(0))
            Pattern1();
        else if (curPatternId.Equals(1))
            Pattern2();
    }

    private void Pattern1()
    {
        if (isPattern2ChangingDone.Equals(true))
            return;

        curMoveSpeed = 0;

        if (isCharging.Equals(true))
            return;

        // 인식 범위 내 진입
        //isInAttackRange : 근접공격범위
        if (isInNoticeRange.Equals(true))
        {
            if (isFirstTalkingOn.Equals(false))
            {
                PresentTalking(0);
                isFirstTalkingOn = true;
            }

            playerPos = cUtil._player.gameObject.transform.position;

            if (playerPos.x >= this.transform.position.x)
                ChangeDir(Vector3.right);

            else if (playerPos.x < this.transform.position.x)
                ChangeDir(Vector3.left);


            if (curDoubleAttackCount.Equals(2) && isSpoilDynamite.Equals(false) && isDoubleAttack.Equals(false))
            {
                DynamiteChainAttack();
                return;
            }
            //일반 공격을 3회 했을 시
            else if (curNormalAttackCount.Equals(3) && isSpoilDynamite.Equals(false)
                && isNormalAttack.Equals(false) && isCharging.Equals(false))
            {
                DoubleDynamite();
                return;
            }
            //일반 근거리 공격
            else if (isDoubleAttack.Equals(false) && isSpoilDynamite.Equals(false) && isCharging.Equals(false))
            {
                //공격 쿨타임 돌린다.
                normalAttackCoolTimer += Time.deltaTime;
                if (normalAttackCoolTimer >= normalAttackCoolTime)
                {
                    isNormalAttackOrTeleportAttack = (Random.value > 0.5f); // 랜덤으로 일반공격인지 텔레포트인지 결정
                    if (isNormalAttackOrTeleportAttack)
                    {
                        NormalAttack(true);
                    }
                    else
                    {
                        TeleportAttack(1);
                    }
                    normalAttackCoolTimer = 0;
                    Debug.Log(curNormalAttackCount);
                }
            }
        }
    }

    private void Pattern2()
    {
        //Debug.Log("lazerTimer : " + lazerTimer);

        //궁극기
        if (isCharging.Equals(true))
        {
            // 건드리면 피곤해짐
            curNormalAttackCount = 0;
            curDoubleAttackCount = 0;
            curBulletSpoilCount = 0;

            dealTimer += Time.deltaTime;
            dp.chg_camera.CameraShake(10.0f, 0.02f);

            if (dealTimer >= dealTime)
            {
                dealTimer = 0;
                isCharging = false;
                _animator.SetTrigger("Off_3Done");
                return;
            }
            return;
        }

        if (isSummoning.Equals(true))
        {
            _animator.SetBool("NormalAttack", false);
            _animator.SetBool("NormalAttack2", false);
            _animator.SetBool("NormalAttack3", false);
            _animator.SetBool("Teleport", false);
            _animator.SetBool("Off_NormalAttack3", false);
            if (isCloneAllDead.Equals(true))
            {
                //밑으로 이동, 지정위치 되면 isSummoning false, rt active, 모든 변수 초기화
                if (isGrounded.Equals(false))
                {
                    curMoveSpeed = maxMoveSpeed * 0.5f;
                    dir = Vector3.down;
                    originObj.transform.Translate(dir * curMoveSpeed * Time.deltaTime);
                    return;
                }
                else
                {
                    curMoveSpeed = 0;
                    isGravity = true;
                    curNormalAttackCount = 0;
                    curDoubleAttackCount = 0;
                    curBulletSpoilCount = 0;
                    isNormalAttack = false;
                    isDoubleAttack = false;
                    isSpoilDynamite = false;
                    _animator.SetBool("NormalAttack", false);
                    _animator.SetBool("NormalAttack2", false);
                    _animator.SetBool("NormalAttack3", false);
                    rt.enabled = true;
                    isSummoning = false;

                    return;
                }
            }
            else if (isCloneAllDead.Equals(false) && isSummonDone.Equals(true))
            {
                //지정 위치로 이동, 지정위치 되면 멈춤
                if (Vector2.Distance(this.originObj.transform.position, bossPos_DuringSummon.position) <= 10.0f)
                {
                    curMoveSpeed = 0;
                    return;
                }
                else
                {
                    isGravity = false;
                    curMoveSpeed = maxMoveSpeed;
                    dir = (bossPos_DuringSummon.position - this.originObj.transform.position).normalized;
                    originObj.transform.Translate(dir * curMoveSpeed * Time.deltaTime);
                    return;
                }
            }
            return;
        }

        // 인식 범위 내 진입
        //isInAttackRange : 근접공격범위
        if (isInNoticeRange.Equals(true))
        {
            curMoveSpeed = 0;
            playerPos = cUtil._player.gameObject.transform.position;

            if (playerPos.x >= this.transform.position.x)
                ChangeDir(Vector3.right);

            else if (playerPos.x < this.transform.position.x)
                ChangeDir(Vector3.left);

            if (isHpLowerThan20Percent.Equals(true) && isTeleport10TimesDone.Equals(false) &&
                isTeleport10Times.Equals(false) && isLazer.Equals(false))
            {
                if (isSpoilDynamite.Equals(false) && isDoubleAttack.Equals(false) && isCharging.Equals(false))
                {
                    Debug.Log("10회 시작");
                    TeleportAttack(10);
                    return;
                }
            }

            //쫄 소환
            monsterOnCoolTimer += Time.deltaTime;
            if (monsterOnCoolTimer >= monsterOnCoolTime)
            {
                monsterOnCoolTimer = 0;
                InstantiateMonsters();
            }

            lazerTimer += Time.deltaTime;
            if ((lazerTimer >= lazerCoolTime) && isSpoilDynamite.Equals(false) && isDoubleAttack.Equals(false)
                && isTeleport10Times.Equals(false) && isNormalAttack.Equals(false) && isCharging.Equals(false)
                && isLazer.Equals(false))
            {
                Debug.Log("LAZERON");
                LazerOn();
                return;
            }

            if (curDoubleAttackCount.Equals(2) && isSpoilDynamite.Equals(false) && isDoubleAttack.Equals(false)
                && isTeleport10Times.Equals(false) && isLazer.Equals(false) && isCharging.Equals(false))
            {
                DynamiteChainAttack(true);
                return;
            }
            //일반 공격을 2회 했을 시
            else if (curNormalAttackCount.Equals(2) && isSpoilDynamite.Equals(false)
                && isNormalAttack.Equals(false) && isCharging.Equals(false)
                && isTeleport10Times.Equals(false) && isLazer.Equals(false))
            {
                DoubleDynamite(true);
                return;
            }
            //일반 근거리 공격
            else if (isDoubleAttack.Equals(false) && isSpoilDynamite.Equals(false) && isCharging.Equals(false)
                && isTeleport10Times.Equals(false) && isLazer.Equals(false))
            {
                //공격 쿨타임 돌린다.
                normalAttackCoolTimer += Time.deltaTime;
                if (normalAttackCoolTimer >= normalAttackCoolTime)
                {
                    isNormalAttackOrTeleportAttack = (Random.value > 0.5f); // 랜덤으로 일반공격인지 텔레포트인지 결정
                    if (isNormalAttackOrTeleportAttack)
                    {
                        NormalAttack(true);
                    }
                    else
                    {
                        TeleportAttack(1);
                    }
                    normalAttackCoolTimer = 0;
                }
            }
        }
    }

    private void LazerOn()
    {
        Debug.Log("LazerOn");
        isLazer = true;
        _animator.SetTrigger("LazerOn");
    }
    public void LazerOn_ani()
    {
        StartCoroutine(LazerOn_cor());
    }
    IEnumerator LazerOn_cor()
    {
        cCamera camera = dp.chg_camera;
        Vector3 leftDownPos = camera.cam.ViewportToWorldPoint(new Vector2(0, 0));
        Vector3 rightUpPos = camera.cam.ViewportToWorldPoint(new Vector2(1, 1));

        byte i = 0;
        while (true)
        {
            yield return wf;

            if (i >= lazerPos.Length)
                break;

            float randomX = Random.Range(leftDownPos.x, rightUpPos.x);
            float randomY = Random.Range(leftDownPos.y, rightUpPos.y);
            Vector2 randomPos = new Vector2(randomX, randomY);

            if (tileMng.isTileExist(randomPos).Equals(false))
            {
                lazerPos[i] = tileMng.WorldPosToTilePos(randomPos);
                i += 1;
            }
            else
                continue;
        }

        for (byte j = 0; j < lazerPos.Length; j++)
        {
            lazerObjs.transform.GetChild(j).gameObject.SetActive(true);
            lazerObjs.transform.GetChild(j).gameObject.GetComponent<cBoss_theme1_stage5_Lazer>().Init(lazerPos[j]);
            yield return lazerTerm;
        }

        lazerTimer = 0;
        isLazer = false;
    }

    public void JumpAttack_ani()
    {
        if (Vector3.Distance(cUtil._player.transform.position, originObj.transform.position) <= 600)
        {
            PlayDrainEffect();
            cUtil._player.ReduceHp(damage.value * 3, dir);
        }
    }

    public void JumpAttackDone_ani()
    {
        isCharging = false;
    }

    private void DynamiteChainAttack(bool pPattern2 = false)
    {
        if (pPattern2.Equals(false))
        {
            _animator.SetTrigger("Charge");

            isSpoilDynamite = true;
        }
        else
        {
            _animator.SetTrigger("Off_NormalAttack3");
            isCharging = true;
        }

        PresentTalking(2);
    }
    public void SummonChargeDone_ani()
    {
        _animator.SetTrigger("NormalAttack3");
    }

    public void ChainAttack_ani()
    {
        PlayDrainEffect();
        cUtil._player.ReduceHp(damage.value * 2, Vector3.zero);
    }

    public void InstantiateMonsters()
    {
        for (byte i = 0; i < monstersParent.transform.childCount; i++)
        {
            if (monstersParent.transform.GetChild(i).gameObject.activeSelf.Equals(false))
            {
                monstersParent.transform.GetChild(i).gameObject.SetActive(true);
                monstersParent.transform.GetChild(i).GetChild(0).GetComponent<cEnemy_monster>().RespawnInit();
            }
        }
    }
    public void ChargeDone_ani()
    {
        isCharging = false;
    }

    public void SummonClones_ani()
    {
        isSummoning = true;
        rt.enabled = false;
        StartCoroutine(SummonClones_cor());
    }
    IEnumerator SummonClones_cor()
    {
        for (byte i = 0; i < monstersParent_forUlti.transform.childCount; i++)
        {
            monstersParent_forUlti.transform.GetChild(i).gameObject.SetActive(true);
            monstersParent_forUlti.transform.GetChild(i).GetChild(0).GetComponent<cEnemy_monster>().RespawnInit();
            monstersParent_forUlti.transform.GetChild(i).gameObject.
                transform.GetChild(0).GetComponent<cMonster_devil_sub>().SetMother(this);
            yield return waitforSec_Summon;
        }
    }
    public void SummonClonesDone_ani()
    {
        isSummonDone = true;
    }

    public void DynamiteChainDone_ani()
    {
        curNormalAttackCount = 0;
        curDoubleAttackCount = 0;
        curBulletSpoilCount = 0;
        isNormalAttack = false;
        isDoubleAttack = false;
        isSpoilDynamite = false;
        isCharging = false;
        _animator.SetBool("NormalAttack", false);
        _animator.SetBool("NormalAttack2", false);
        _animator.SetBool("NormalAttack3", false);
    }

    private void DoubleDynamite(bool pPattern2 = false)
    {
        if (pPattern2.Equals(false))
            _animator.SetTrigger("NormalAttack2");
        else
            _animator.SetTrigger("Off_NormalAttack2");

        curNormalAttackCount = 0;
        isDoubleAttack = true;
        PresentTalking(1);
    }

    public void Off_NormalAttack2_ani()
    {
        attackBox.SetActive(true);
    }
    public void Off_NormalAttack2AttackBoxDone_ani()
    {
        attackBox.SetActive(false);
    }

    public void DoubleDynamiteDone_ani()
    {
        curDoubleAttackCount += 1;
        isDoubleAttack = false;
    }

    private void NormalAttack(bool isNear)
    {
        if (isNear.Equals(true))
        {
            _animator.SetTrigger("NormalAttack");
        }
        else
        {
            _animator.SetTrigger("Off_NormalAttack");
        }

        isNormalAttack = true;
    }

    public void NormalAttackDone_ani()
    {
        curNormalAttackCount += 1;
        isNormalAttack = false;
    }

    public void NormalNearAttack_ani()
    {
        normalAttackBox.SetActive(true);
    }
    public void NormalNearAttackBoxDone_ani()
    {
        normalAttackBox.SetActive(false);
    }

    // 인자는 텔레포트 횟수
    public void TeleportAttack(byte pNum)
    {
        // 일반공격일경우
        if (pNum.Equals(1))
        {
            isNormalAttack = true;
        }
        if (pNum.Equals(10))
        {
            // 데미지 고정값으로 변화
            normalAttackBox.GetComponent<cEnemy_bossAttackBox>().SetDamage(damageForTeleport10Times);
            isTeleport10Times = true;
        }
        curTeleportAttackCount = 0;
        inputTeleportAttackCount = pNum;
        _animator.SetTrigger("Teleport");
    }
    public void TeleportAttack_Teleport_ani()
    {
        // 플레이어 양 옆 좌표
        teleportPos_Right = new Vector3(playerPos.x + 250f, originObj.transform.position.y, originObj.transform.position.z);
        teleportPos_Left = new Vector3(playerPos.x - 250f, originObj.transform.position.y, originObj.transform.position.z);

        // 두 위치 중 타일이 없는 곳으로 텔레포트
        if (this.dir.Equals(Vector3.right))
        {
            if (tileMng.isTileExist(teleportPos_Right).Equals(false))
            {
                this.originObj.transform.position = teleportPos_Right;
            }
            else
            {
                this.originObj.transform.position = teleportPos_Left;
            }
        }
        else
        {
            if (tileMng.isTileExist(teleportPos_Left).Equals(false))
            {
                this.originObj.transform.position = teleportPos_Left;
            }
            else
            {
                this.originObj.transform.position = teleportPos_Right;
            }
        }

        PlayTeleportEffect();
    }
    public void TeleportAttack_Attack_ani()
    {
        curTeleportAttackCount += 1;
        normalAttackBox.SetActive(true);
    }
    public void TeleportAttack_AttackBoxDone_ani()
    {
        normalAttackBox.SetActive(false);
    }
    public void TeleportAttackDone_ani()
    {
        // 횟수만큼 텔레포트 시행했을때
        if (inputTeleportAttackCount >= curTeleportAttackCount)
        {
            // 10회 연속공격일경우
            if (inputTeleportAttackCount == 10)
            {
                // 본래 노말어택 데미지로
                normalAttackBox.GetComponent<cEnemy_bossAttackBox>().SetDamage(damage.value);
                _animator.SetTrigger("Groggy");
                inputTeleportAttackCount = 0;
                curTeleportAttackCount = 0;
                return;
            }
            // 이 텔레포트 공격이 일반 공격일 경우
            else
            {
                curNormalAttackCount += 1;
                isNormalAttack = false;
                _animator.SetTrigger("TeleportDone");
                inputTeleportAttackCount = 0;
                curTeleportAttackCount = 0;
                return;
            }
        }
        else
        {
            if (inputTeleportAttackCount == 10)
                Debug.Log("curTeleportAttackCount : " + curTeleportAttackCount);
            
            _animator.SetTrigger("TeleportAgain");
            return;
        }
    }
    public void GroggyDone_ani()
    {
        isTeleport10TimesDone = true;
        isTeleport10Times = false;
    }


    public void NormalFarAttack_ani()
    {
        bulletManager.LaunchBullet(originObj.transform.position, true, cUtil._player.originObj.transform.position, bulletDamage, 15.0f);
    }

    public void Dead()
    {
        originObj.transform.position = new Vector3(-10000, -10000, 0);
        PresentTalking(4, true);
        dp.StartBossVictory();
    }

    // 공격자가 있는 경우 공격자가 바라보는 방향을 pDir로 받음
    public override void ReduceHp(long pVal, Vector3 pDir, float pVelocity = 7.5f, float pDelay = 0)
    {
        if (isDead.Equals(true))
            return;

        curHp.value -= pVal;

        if (pDelay != 0)
        {
            delayHit = DelayHit_cor(pVal, pDir, pVelocity, pDelay);
            StartCoroutine(delayHit);
            return;
        }

        cUtil._soundMng.playAxeEffect(2);

        hitCor = HitEffect_cor();
        StartCoroutine(hitCor);

        if (curHp.value <= (long)(maxHp.value * 0.5f) && isPattern2Started.Equals(false))
        {
            _animator.SetTrigger("Charge");
            _animator.SetTrigger("Change");
            originObj.GetComponent<BoxCollider2D>().enabled = false;
            isPattern2Started = true;
        }

        if (curHp.value <= 0)
        {
            curHp.value = 0;
            isDead = true;
            _animator.SetTrigger("Dead");
            originObj.GetComponent<BoxCollider2D>().enabled = false;
        }
        else if (curHp.value <= maxHp.value * 0.5f && isHalfHpTalkingOn.Equals(false))
        {
            PresentTalking(3);
            normalAttackBox.GetComponent<cEnemy_bossAttackBox_stage5>().SetDrainOn(true);
            attackBox.GetComponent<cEnemy_bossAttackBox_stage5>().SetDrainOn(true);
            isHalfHpTalkingOn = true;
        }

        if (curHp.value <= maxHp.value * 0.2f && isTeleport10TimesDone.Equals(false))
        {
            isHpLowerThan20Percent = true;
        }

        //if (isDead.Equals(false))
        //    _animator.SetTrigger("GetHit");

        SetHp();
    }

    public void Pattern2ChangingDone_ani()
    {
        isPattern2ChangingDone = true;
        originObj.GetComponent<BoxCollider2D>().enabled = true;
        curMoveSpeed = maxMoveSpeed;
        curPatternId += 1;
        curNormalAttackCount = 0;
        curDoubleAttackCount = 0;
        curBulletSpoilCount = 0;
        isNormalAttack = false;
        isDoubleAttack = false;
        isSpoilDynamite = false;
        isCharging = false;
        _animator.SetBool("NormalAttack", false);
        _animator.SetBool("NormalAttack2", false);
        _animator.SetBool("NormalAttack3", false);
        normalAttackBox.GetComponent<cEnemy_bossAttackBox_stage5>().SetDrainOn(true);
        attackBox.GetComponent<cEnemy_bossAttackBox_stage5>().SetDrainOn(true);
    }

    // 클론 모두 다 죽었으면 isCloneAllDead = true
    public void CheckClonesAllDead()
    {
        byte deadCall = 0;

        for (byte i = 0; i < monstersParent_forUlti.transform.childCount; i++)
        {
            if (monstersParent_forUlti.transform.GetChild(i).gameObject.activeSelf.Equals(true))
            {
                deadCall += 1;
            }
            else
                continue;
        }

        // 마지막 클론일 경우
        if (deadCall == 1)
            isCloneAllDead = true;
        else
            isCloneAllDead = false;
        
    }

    public void PlayDrainEffect()
    {
        fxDir = (cUtil._player.transform.position - originObj.transform.position).normalized;
        fxAngle = Vector3.SignedAngle(transform.up, fxDir, -transform.forward);
        effects[0].transform.position = cUtil._player.transform.position;
        effects[0].transform.rotation = Quaternion.AngleAxis(fxAngle, Vector3.forward);
        effects[0].Play();
    }

    public void PlayTeleportEffect()
    {
        effects[1].transform.position = fxPos_Teleport.position;
        effects[1].Play();
    }
}
