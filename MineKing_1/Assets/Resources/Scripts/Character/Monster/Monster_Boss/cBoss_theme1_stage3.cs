﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cBoss_theme1_stage3 : cEnemy_monster
{
    //Pattern1 변수
    private byte curNormalAttackCount; //n번 일반 공격 시 다이너 2발 발사
    public bool isNormalAttack;
    private byte curDoubleAttackCount; //n번 2발 다이너 시 다이너 난사
    public bool isDoubleAttack;
    private byte curBulletSpoilCount; //n번 총알 난사 시 연쇄 다이너
    public bool isSpoilDynamite;
    public bool isCharging;
    public bool isGlobalAttackDone;
    private bool isDrillAttackStart;
    private const float dealTime = 5.0f;
    private float dealTimer;

    public cBoss_theme1_stage3_ChargeAttackBox chargeAttckBox;
    public bool isCollidePlayer; // 대쉬 공격 중 플레이어 충돌 여부
    private const float drillDealCoolTime = 0.7f;
    private float drillDealCoolTimer;
    private bool isChargeFailMotion;

    //Pattern2 변수
    private bool isPattern2Started;
    private bool isPattern2ChangingDone;

    private bool isJumpAttackOrBombing; // true : 점프어택, false : 소이탄
    public cBulletManager bulletMng_forBombing;
    private const float normalAttackCoolTime = 3.0f;
    private float normalAttackCoolTimer;

    private byte curBulletIdx;
    private List<cBullet_skeleton> bullets_AOE;

    private Vector3 NormalNearAttackBoxPos;//local
    
    float randomX;
    float randomY;
    float randomX2;
    float randomY2;

    // 코루틴용 캐싱
    WaitForFixedUpdate wf = new WaitForFixedUpdate();

    public void InitBoss()
    {
        base.Init(cEnemyTable.SetMonsterInfo(53));

        jumpHeight = 400.0f;
        defaultGravity = 1300.0f;

        chargeAttckBox.Init();
        bulletDamage = 18;
        NormalNearAttackBoxPos = new Vector3(-54, 39, -1);
        curMoveSpeed = 0;
        isJumpAttackOrBombing = false;

        curBulletIdx = 0;
        bullets_AOE = new List<cBullet_skeleton>();

        talkText = new string[5];
        talkText[0] = "본인.등장.안구.깔기.요망.";
        talkText[1] = "본인.폭약.대량.투척.예정.하수.빤쓰도주.필요";
        talkText[2] = "무자비.전진.공격.개시.";
        talkText[3] = "개발자.휴전.협정.제안.본인.스탯.상당수.불합리";
        talkText[4] = "적장.강력.반면.본인.허접.개발자.밸붕.알못.";
    }

    protected override void FixedUpdate()
    {
        if (isDead.Equals(true))
            return;

        if (isInit.Equals(true))
        {
            base.FixedUpdate();
        }

    }

    protected override void Move()
    {
        if (_animator != null)
            _animator.SetFloat("MoveSpeed", curMoveSpeed);

        // 차지공격중일때는 방향 전환 안함 (차지공격 코루틴에서 blocked 변수 참조하기 때문)
        if(isCharging.Equals(false))
        {
            //막히면 방향 바꿔준다.
            if (isRightBlocked == true)
            {
                isRightBlocked = false;
                ChangeDir(Vector3.left);
            }
            else if (isLeftBlocked == true)
            {
                isLeftBlocked = false;
                ChangeDir(Vector3.right);
            }
        }

        //패턴
        if (curPatternId.Equals(0))
            Pattern1();
        else if (curPatternId.Equals(1))
            Pattern2();
    }

    private void Pattern1()
    {
        if (isPattern2ChangingDone.Equals(true))
        {
            return;
        }

        if (isPattern2Started.Equals(true) && isPattern2ChangingDone.Equals(false))
        {
            return;
        }

        //장갑차 타있게
        if(isDrillAttackStart.Equals(false))
        {
            curMoveSpeed = 0;
        }

        // 인식 범위 내 진입
        //isInAttackRange : 근접공격범위
        if (isInNoticeRange.Equals(true))
        {
            if (isFirstTalkingOn.Equals(false))
            {
                PresentTalking(0);
                isFirstTalkingOn = true;
            }

            playerPos = cUtil._player.gameObject.transform.position;

            if (playerPos.x >= this.transform.position.x)
                ChangeDir(Vector3.right);

            else if (playerPos.x < this.transform.position.x)
                ChangeDir(Vector3.left);
            
            if (curBulletSpoilCount.Equals(2) && isSpoilDynamite.Equals(false))
            {
                DrillAttack();
                return;
            }
            //다이너 난사 공격을 2회 했을 시 다이너 연사
            else if (curDoubleAttackCount.Equals(2) && isCharging.Equals(false) && isDoubleAttack.Equals(false))
            {
                DynamiteChainAttack();
                return;
            }
            //일반 공격을 1회 했을 시 2발 발사
            else if (curNormalAttackCount.Equals(1) && isSpoilDynamite.Equals(false)
                && isNormalAttack.Equals(false) && isCharging.Equals(false))
            {
                DoubleDynamite();
                return;
            }
            //일반 원거리 공격
            else if (isDoubleAttack.Equals(false) && isSpoilDynamite.Equals(false) && isCharging.Equals(false))
            {
                //공격 쿨타임 돌린다.
                normalAttackCoolTimer += Time.deltaTime;
                if (normalAttackCoolTimer >= normalAttackCoolTime)
                {
                    NormalAttack(true);
                    normalAttackCoolTimer = 0;
                }
            }
        }
}

    private void Pattern2()
    {
        if (isPattern2Started.Equals(true) && isPattern2ChangingDone.Equals(false))
            return;

        //점프 공격중이면 리턴
        if (isCharging.Equals(true))
            return;

        // 인식 범위 내 진입
        //isInAttackRange : 근접공격범위
        if (isInNoticeRange.Equals(true))
        {
            playerPos = cUtil._player.gameObject.transform.position;

            if (playerPos.x >= this.transform.position.x)
                ChangeDir(Vector3.right);

            else if (playerPos.x < this.transform.position.x)
                ChangeDir(Vector3.left);


            if (curBulletSpoilCount.Equals(2) && isSpoilDynamite.Equals(false))
            {
                if(isJumpAttackOrBombing)
                {
                    DrillAttack(true);
                }
                else
                {
                    Bombing();
                }
                return;
            }
            //다이너 난사 공격을 2회 했을 시 다이너 연사
            else if (curDoubleAttackCount.Equals(2) && isCharging.Equals(false) && isDoubleAttack.Equals(false))
            {
                DynamiteChainAttack(true);
                return;
            }
            //일반 공격을 1회 했을 시 스탭
            else if (curNormalAttackCount.Equals(1) && isSpoilDynamite.Equals(false)
                && isNormalAttack.Equals(false) && isCharging.Equals(false))
            {
                if (isInAttackRange.Equals(false))
                {
                    curMoveSpeed = maxMoveSpeed;
                    originObj.transform.Translate(dir * curMoveSpeed * Time.deltaTime);
                    Debug.Log(dir);
                    return;
                }

                DoubleDynamite(true);
                curMoveSpeed = 0;

                return;
            }
            //일반 근거리 공격
            else if (isDoubleAttack.Equals(false) && isSpoilDynamite.Equals(false) && isCharging.Equals(false))
            {
                if (isInAttackRange.Equals(false))
                {
                    curMoveSpeed = maxMoveSpeed;
                    originObj.transform.Translate(dir * curMoveSpeed * Time.deltaTime);
                    Debug.Log(dir);
                    return;
                }
                else
                    curMoveSpeed = 0;

                //공격 쿨타임 돌린다.
                normalAttackCoolTimer += Time.deltaTime;
                if (normalAttackCoolTimer >= normalAttackCoolTime)
                {
                    NormalAttack(false);
                    normalAttackCoolTimer = 0;
                    curMoveSpeed = 0;
                }
            }
        }
        else
            curMoveSpeed = 0;
    }

    public void Charge_CamShake()
    {
        dp.chg_camera.CameraShake(5.0f, 1.5f);
    }

    private void DrillAttack(bool pPattern2 = false)
    {
        if (pPattern2.Equals(false))
            _animator.SetTrigger("ChargeOn");
        else
             _animator.SetTrigger("JumpAttack");

        curBulletSpoilCount = 0;
        isCharging = true;

        PresentTalking(2);
    }

    public void Bombing()
    {
        isJumpAttackOrBombing = true;
        _animator.SetTrigger("Bombing");
        curBulletSpoilCount = 0;
    }
    public void Bombing_ani()
    {
        Vector3 target = new Vector3(cUtil._player.originObj.transform.position.x,
            cUtil._player.transform.position.y + 300f, 0);

        bulletMng_forBombing.LaunchBullet_GoblinBomb(this.transform.position,false,target, bulletDamage,15.0f);
    }
    public void BombingDone_ani()
    {
        isCharging = false;
    }

    public void StartJump()
    {
        isJumpAttackOrBombing = false;
        StartCoroutine(Jump());
    }

    protected override IEnumerator Jump()
    {
        float jumpTimer = 0;
        float factor;

        float currentHeight = originObj.transform.position.y;

        while (true)
        {
            yield return wf;
            
            isGrounded = false;

            factor = Mathf.PI * (jumpTimer / jumpTime) * 0.5f;

            originObj.transform.position = new Vector3(originObj.transform.position.x,
                currentHeight + jumpHeight * Mathf.Sin(factor), originObj.transform.position.z);

            if (jumpTimer >= jumpTime)
            {
                changingGravity = defaultGravity;
                break;
            }
            if (isUpBlocked == true)
            {
                changingGravity = defaultGravity;
                break;
            }
            jumpTimer += Time.deltaTime;
        }
    }

    public void JumpAttack_ani()
    {
        if (Vector3.Distance(cUtil._player.transform.position, originObj.transform.position) <= 600)
        {
            cUtil._player.ReduceHp(damage.value * 3, dir);
        }
    }

    public void JumpAttackDone_ani()
    {
        isCharging = false;
    }

    public void DrillAttackStart_ani()
    {
        isDrillAttackStart = true;
        StartCoroutine(DrillAttack_cor());
    }
    
    private void DynamiteChainAttack(bool pPattern2 = false)
    {
        if (pPattern2.Equals(false))
            _animator.SetTrigger("NormalAttack3");
        else
            _animator.SetTrigger("Off_NormalAttack3");

        curDoubleAttackCount = 0;
        curBulletSpoilCount += 1;
        isSpoilDynamite = true;

        PresentTalking(1);
    }

    public void DynamiteChainDone_ani()
    {
        isSpoilDynamite = false;
    }
    
    private void DoubleDynamite(bool pPattern2 = false)
    {
        if (pPattern2.Equals(false))
            _animator.SetTrigger("NormalAttack2");
        else
            _animator.SetTrigger("Off_NormalAttack2");

        curDoubleAttackCount += 1;
        curNormalAttackCount = 0;
        isDoubleAttack = true;
    }

    public void DoubleDynamiteDone_ani()
    {
        isDoubleAttack = false;
    }

    private void NormalAttack(bool isNear)
    {
        if (isNear.Equals(true))
        {
            _animator.SetTrigger("NormalAttack");
        }
        else
        {
            _animator.SetTrigger("Off_NormalAttack");
        }

        curNormalAttackCount += 1;
        isNormalAttack = true;
    }

    public void NormalAttackDone_ani()
    {
        isNormalAttack = false;
    }

    public void NormalNearAttack_ani()
    {
        if (isInAttackRange.Equals(true))
        {
            cUtil._player.ReduceHp(damage.value, dir);
        }
    }

    public void NormalFarAttack_ani()
    {
        bulletManager.LaunchBullet(originObj.transform.position, true, cUtil._player.originObj.transform.position, bulletDamage, 15.0f);
    }

    public void Dead()
    {
        originObj.transform.position = new Vector3(-10000, -10000, 0);
        PresentTalking(4, true);
        dp.StartBossVictory();
    }

    // 공격자가 있는 경우 공격자가 바라보는 방향을 pDir로 받음
    public override void ReduceHp(long pVal, Vector3 pDir, float pVelocity = 7.5f, float pDelay = 0)
    {
        if (isDead.Equals(true))
            return;

        if (pDelay != 0)
        {
            delayHit = DelayHit_cor(pVal, pDir, pVelocity, pDelay);
            StartCoroutine(delayHit);
            return;
        }

        if (isChargeFailMotion)
        {
            _animator.SetTrigger("HitWhitinChargeFail");
        }

        cUtil._soundMng.playAxeEffect(2);

        curHp.value -= pVal;

        hitCor = HitEffect_cor();
        StartCoroutine(hitCor);

        if (curHp.value <= (long)(maxHp.value * 0.5f) && isPattern2Started.Equals(false))
        {
            _animator.SetTrigger("GetOff");
            originObj.GetComponent<BoxCollider2D>().enabled = false;
            isPattern2Started = true;
        }
        else if (curHp.value <= maxHp.value * 0.5f && isHalfHpTalkingOn.Equals(false))
        {
            PresentTalking(3);
            isHalfHpTalkingOn = true;
        }

        if (curHp.value <= 0)
        {
            curHp.value = 0;
            isDead = true;
            _animator.SetTrigger("Dead");
            originObj.GetComponent<BoxCollider2D>().enabled = false;
        }

        //if (isDead.Equals(false))
        //    _animator.SetTrigger("GetHit");

        SetHp();
    }

    public void Pattern2ChangingDone_ani()
    {
        isPattern2ChangingDone = true;
        originObj.GetComponent<BoxCollider2D>().enabled = true;
        curMoveSpeed = maxMoveSpeed;
        curPatternId += 1;
        curNormalAttackCount = 0;
        curDoubleAttackCount = 0;
        curBulletSpoilCount = 0;
        isNormalAttack = false;
        isDoubleAttack = false;
        isSpoilDynamite = false;
        isCharging = false;
        _animator.SetBool("NormalAttack", false);
        _animator.SetBool("NormalAttack2", false);
        _animator.SetBool("NormalAttack3", false);
    }

    IEnumerator DrillAttack_cor()
    {
        curMoveSpeed = maxMoveSpeed;

        while(true)
        {
            yield return wf;
            
            if (isLeftBlocked)
            {
                dp.chg_camera.CameraShake(25.0f, 0.05f);
                ChargeFail();
                break;
            }
            if (isRightBlocked)
            {
                dp.chg_camera.CameraShake(25.0f, 0.05f);
                _animator.SetTrigger("ChargeFail");
                break;
            }
            if (isCollidePlayer)
            {
                dp.chg_camera.CameraShake(25.0f, 0.05f);
                cUtil._player.ReduceHp((long)(cUtil._player.GetMaxHp().value * 0.25f), this.dir, 9f);
                _animator.SetTrigger("ChargeSuccess");
                break;
            }

            if (curMoveSpeed < maxMoveSpeed * 5f)
            {
                curMoveSpeed = curMoveSpeed * 1.2f;
            }
            else
            {
                curMoveSpeed = maxMoveSpeed * 5f;
            }
            
            originObj.transform.Translate(dir * curMoveSpeed * Time.deltaTime);
        }
        
        isDrillAttackStart = false;
        isGlobalAttackDone = true;
    }

    private void ChargeFail()
    {
        isChargeFailMotion = true;
        _animator.SetTrigger("ChargeFail");
    }
    private void ChargeFailDealTimeDone_ani()
    {
        isChargeFailMotion = false;
    }
    private void ChargeDone_ani()
    {
        isGlobalAttackDone = false;
        isCharging = false;
        _animator.SetTrigger("ChargeOff");
        _animator.SetBool("NormalAttack2", false);
        _animator.SetBool("NormalAttack3", false);
        curNormalAttackCount = 0;
        curDoubleAttackCount = 0;
        curBulletSpoilCount = 0;
    }
}
