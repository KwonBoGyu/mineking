﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cBoss_theme1_stage2 : cEnemy_monster
{
    //Pattern1 변수
    private byte curNormalAttackCount; //n번 일반 공격 시 총알 난사
    private bool isSpoilOrThreeBones; // true : spoilBullet, false : ThreeBones
    private bool isSpoilBullet;
    private byte curBulletSpoilCount; //n번 총알 난사 시 궁극기
    private bool isSmash;
    private byte isSmashed; //스매쉬 1번하면 쉴드
    public cShield scr_Shield;
    private const float shieldTime = 5.0f;
    private float shieldTimer;
    private long shieldHp;
    public long GetShieldHp() { return shieldHp; }
    public Transform smashGroundPos;
    // ThreeBones 애니메이션 변수
    private byte curAimIdx;
    public GameObject Aims;

    // 패턴2 변수
    public GameObject monstersParent;
    public cBulletManager bulletManager_explode; // 2페이즈 되면 bulletManager가 본 변수를 참조함
    private bool isPattern2Started;
    private bool isPattern2Changing;
    private bool isPattern2ChangingDone;
    private float timer_ForSummon;
    private bool isSummon;
    public GameObject bulletPos_AOE;
    private byte maxBulletCountAOE;
    private byte curBulletIdx;
    private cBullet_skeleton_explode[] bullets_AOE;

    private bool isNormalAttack;
    private const float normalAttackCoolTime = 3.0f;
    private float normalAttackCoolTimer;
    
    private Vector3 NormalNearAttackBoxPos;//local
    
    float randomX;
    float randomY;
    float randomX2;
    float randomY2;

    public void InitBoss()
    {
        base.Init(cEnemyTable.SetMonsterInfo(52));

        scr_Shield.Init();

        isSpoilOrThreeBones = true;
        shieldHp = 30;
        bulletDamage = 12;
        timer_ForSummon = 30.0f;
        NormalNearAttackBoxPos = new Vector3(-54, 39, -1);

        maxBulletCountAOE = 6;
        curBulletIdx = 0;
        bullets_AOE = new cBullet_skeleton_explode[maxBulletCountAOE];

        talkText = new string[5];
        talkText[0] = "이누무,,,,좌식덜,,,,^^ 으른. 왔는디,,, 인사덜을. 않혀구~~~";
        talkText[1] = "요사이,,, 절믄것덜은~~~ 공경심이~ 다. 죽어부러쓰~~~~";
        talkText[2] = "나. 05년생. 청년. 인디,,, 동년배덜~~ 다~~ 마인킹현다^^";
        talkText[3] = "아놔~ 등산이나,,, 댕겨. 올걸. 그랬읍니다,,~~~";
        talkText[4] = "노병은,,, 죽지. 안는다,,^^,,,,";
    }

    protected override void FixedUpdate()
    {
        //분열 전 보스 죽었으면..
        if (isDead.Equals(true))
            return;

        if (isInit.Equals(true))
        {
            base.FixedUpdate();
        }
    }

    protected override void Move()
    {
        if (_animator != null)
            _animator.SetFloat("MoveSpeed", curMoveSpeed);

        //막히면 방향 바꿔준다.
        if (isRightBlocked == true)
        {
            isRightBlocked = false;
            ChangeDir(Vector3.left);
        }
        else if (isLeftBlocked == true)
        {
            isLeftBlocked = false;
            ChangeDir(Vector3.right);
        }

        //패턴
        if (curPatternId.Equals(0))
            Pattern1();
        else if (curPatternId.Equals(1))
            Pattern2();
    }

    private void Pattern1()
    {
        if(isPattern2Changing.Equals(true))
        {
            return;
        }
        if (isPattern2Started.Equals(true) && isPattern2ChangingDone.Equals(false))
        {
            return;
        }
        // 인식 범위 내 진입
        //isInAttackRange : 근접공격범위
        if (isInNoticeRange.Equals(true))
        {
            if (isFirstTalkingOn.Equals(false))
            {
                PresentTalking(0);
                isFirstTalkingOn = true;
            }

            playerPos = cUtil._player.gameObject.transform.position;

            if (playerPos.x >= this.transform.position.x)
                ChangeDir(Vector3.right);

            else if (playerPos.x < this.transform.position.x)
                ChangeDir(Vector3.left);

            if (isSmashed.Equals(1))
            {
                ShieldOn();
                _animator.SetBool("MultipleAttack", false);
                _animator.SetBool("NormalFarAttack", false);
                _animator.SetBool("NormalNearAttack", false);
                return;
            }
            else if (isSmashed.Equals(2))
            {
                shieldTimer += Time.deltaTime;
                if(shieldTimer >= shieldTime)
                {
                    shieldTimer = 0;
                    ShieldOff();
                }
                if(scr_Shield.CheckShieldDestroyed().Equals(true))
                {
                    // 쉴드 끄고
                    ShieldOff();
                }
                return;
            }

            //총알 난사 공격을 2회 했을 시 스매쉬
            if (curBulletSpoilCount.Equals(2) && isSpoilBullet.Equals(false))
            {
                SmashGround();
                return;
            }
            //일반 공격을 2회 했을 시 총알 난사
            else if (curNormalAttackCount.Equals(2) && isNormalAttack.Equals(false) && isSmash.Equals(false))
            {
                SpoilBullet(isSpoilOrThreeBones);
                return;
            }
            //일반 원거리 공격
            else if (isInAttackRange.Equals(false) && isSpoilBullet.Equals(false) && isSmash.Equals(false))
            {
                //공격 쿨타임 돌린다.
                normalAttackCoolTimer += Time.deltaTime;
                if (normalAttackCoolTimer >= normalAttackCoolTime)
                {
                    NormalAttack(false);
                    normalAttackCoolTimer = 0;
                }
            }
            //일반 근접 공격
            else if (isInAttackRange.Equals(true) && isSpoilBullet.Equals(false) && isSmash.Equals(false))
            {
                //공격 쿨타임 돌린다.
                normalAttackCoolTimer += Time.deltaTime;
                if (normalAttackCoolTimer >= normalAttackCoolTime)
                {
                    NormalAttack(true);
                    normalAttackCoolTimer = 0;
                }
            }
        }
    }

    private void Pattern2()
    {
        if (isPattern2Started.Equals(true) && isPattern2ChangingDone.Equals(false))
            return;

        // 이펙트 넣어야 함

        // 인식 범위 내 진입
        //isInAttackRange : 근접공격범위
        if (isInNoticeRange.Equals(true))
        {
            if (isFirstTalkingOn.Equals(false))
            {
                PresentTalking(0);
                isFirstTalkingOn = true;
            }

            playerPos = cUtil._player.gameObject.transform.position;

            if (playerPos.x >= this.transform.position.x)
                ChangeDir(Vector3.right);

            else if (playerPos.x < this.transform.position.x)
                ChangeDir(Vector3.left);

            if (isSmashed.Equals(1))
            {
                ShieldOn();
                _animator.SetBool("MultipleAttack", false);
                _animator.SetBool("NormalFarAttack", false);
                _animator.SetBool("NormalNearAttack", false);
                return;
            }
            else if (isSmashed.Equals(2))
            {
                shieldTimer += Time.deltaTime;
                if (shieldTimer >= shieldTime)
                {
                    shieldTimer = 0;
                    ShieldOff();
                }
                if (scr_Shield.CheckShieldDestroyed().Equals(true))
                {
                    // 쉴드 끄고
                    ShieldOff();
                }
                return;
            }
            
            // 소환 타이머 돌림
            timer_ForSummon += Time.deltaTime;
            if (timer_ForSummon >= 30.0f)
            {
                timer_ForSummon = 0;
                SummonSkeleton();
            }

            //총알 난사 공격을 2회 했을 시 스매쉬
            if (curBulletSpoilCount.Equals(2) && isSpoilBullet.Equals(false))
            {
                SmashGround();
                return;
            }
            //일반 공격을 2회 했을 시 총알 난사
            else if (curNormalAttackCount.Equals(2) && isNormalAttack.Equals(false) && isSmash.Equals(false))
            {
                SpoilBullet(isSpoilOrThreeBones);
                return;
            }
            //일반 원거리 공격
            else if (isInAttackRange.Equals(false) && isSpoilBullet.Equals(false) && isSmash.Equals(false))
            {
                //공격 쿨타임 돌린다.
                normalAttackCoolTimer += Time.deltaTime;
                if (normalAttackCoolTimer >= normalAttackCoolTime)
                {
                    NormalAttack(false);
                    normalAttackCoolTimer = 0;
                }
            }
            //일반 근접 공격
            else if (isInAttackRange.Equals(true) && isSpoilBullet.Equals(false) && isSmash.Equals(false))
            {
                //공격 쿨타임 돌린다.
                normalAttackCoolTimer += Time.deltaTime;
                if (normalAttackCoolTimer >= normalAttackCoolTime)
                {
                    NormalAttack(true);
                    normalAttackCoolTimer = 0;
                }
            }
        }
    }

    private void SummonSkeleton()
    {
        for(byte i = 0; i < monstersParent.transform.childCount; i++)
        {
            if (monstersParent.transform.GetChild(i).gameObject.activeSelf.Equals(false))
            {
                monstersParent.transform.GetChild(i).gameObject.SetActive(true);
                monstersParent.transform.GetChild(i).GetChild(0).GetComponent<cEnemy_monster>().RespawnInit();
            }
        }
    }

    private void ShieldOn()
    {
        rt.enabled = false;
        scr_Shield.On();
        _animator.SetTrigger("ShieldOn");
        isSmashed = 2;
    }

    public void ShieldOff()
    {
        isSmashed = 0;
        scr_Shield.Off();
        rt.enabled = true;

        // 쉴드 제한시간내에 못 깼을 때 블라인드 후 공격
        if (scr_Shield.CheckShieldDestroyed().Equals(false))
        {
            _animator.SetTrigger("ShieldOff");
            dp.chg_camera.Blind(3.0f);
            //// 패턴 1
            if (curPatternId.Equals(0))
            {
                Invoke("SmashGround_afterShield", 3.0f);
            }
            //// 패턴 2
            else
            {
                Invoke("AOEAttack", 3.0f);
            }
        }
        // 쉴드 깼을 경우 패턴 초기화
        else
        {
            _animator.SetTrigger("ShieldDestroyed");
        }
    }

    private void SmashGround_afterShield()
    {
        _animator.SetTrigger("GlobalAttack_AfterShield");
        curBulletSpoilCount = 0;
        isSmash = true;

        PresentTalking(2);
    }

    private void AOEAttack()
    {
        _animator.SetTrigger("AOEAttack");
        curBulletSpoilCount = 0;
        isSmash = true;
        curBulletIdx = 0;

        PresentTalking(2);
    }

    public void AOEAttack_BulletAppear_ani()
    {
        for(byte i = 0; i < maxBulletCountAOE; i++)
        {
            Debug.Log("i : " + i);
            bullets_AOE[i] = bulletManager.LaunchBullet_SkeletonAOE(bulletPos_AOE.transform.GetChild(i).
                transform.position,
                false,
                originObj.transform.position, bulletDamage,
                20.0f);
        }
    }

    public void AOEAttack_BulletLaunch_ani()
    {
        bullets_AOE[curBulletIdx].SetDir((cUtil._player.transform.position - 
            bullets_AOE[curBulletIdx].originObj.transform.position).normalized);
        curBulletIdx += 1;
    }

    private void SmashGround()
    {
        _animator.SetTrigger("GlobalAttack");
        curBulletSpoilCount = 0;
        isSmash = true;

        PresentTalking(2);
    }

    public void SmashGround_ani()
    {
        dp.chg_camera.CameraShake(25.0f, 0.2f);

        if (Vector3.Distance(cUtil._player.transform.position, originObj.transform.position) <= 400)
        {
            cUtil._player.ReduceHp(damage.value * 3, dir);
        }
    }
    public void SmashGround_ani2()
    {
        isSmashed = 1;
    }
    public void SmashDone_ani()
    {
        isSmash = false;
    }

    private void SpoilBullet(bool pIsSpoilOrThreeBones)
    {
        if(pIsSpoilOrThreeBones)
        {
            isSpoilOrThreeBones = false;
            _animator.SetTrigger("MultipleAttack");
        }
        else
        {
            Aims.transform.position = new Vector3(cUtil._player.transform.position.x, Aims.transform.position.y, Aims.transform.position.z);
            curAimIdx = 0;
            isSpoilOrThreeBones = true;
            _animator.SetTrigger("ThreeBoneAttack");
        }
        _animator.SetBool("NormalFarAttack", false);
        _animator.SetBool("NormalNearAttack", false);

        curNormalAttackCount = 0;
        isSpoilBullet = true;

        PresentTalking(1);
    }

    public void SpoilBullet_ani()
    {
        randomX = Random.Range(-150, 150);

        bulletManager.LaunchBullet(
            new Vector3(originObj.transform.position.x + randomX,
            originObj.transform.position.y + randomX,
            originObj.transform.position.z), 
            false,
            new Vector3(cUtil._player.originObj.transform.position.x,
            cUtil._player.originObj.transform.position.y,
            cUtil._player.originObj.transform.position.z),
            bulletDamage, 20.0f            
            );
    }

    public void SpoilBulletDone_ani()
    {
        curBulletSpoilCount += 1;
        isSpoilBullet = false;
    }

    public void ThreeBoneAttack_ani()
    {
        bulletManager.LaunchBullet(Aims.transform.GetChild(curAimIdx).transform.position,
            false,
            new Vector3(cUtil._player.originObj.transform.position.x,
            cUtil._player.originObj.transform.position.y,
            cUtil._player.originObj.transform.position.z),
            bulletDamage, 20.0f);

        curAimIdx++;
    }

    public void ThreeBoneAttackDone_ani()
    {
        curBulletSpoilCount += 1;
        isSpoilBullet = false;
    }

    private void NormalAttack(bool isNear)
    {
        if (isNear.Equals(true))
        {
            _animator.SetTrigger("NormalNearAttack");
        }
        else
        {
            _animator.SetTrigger("NormalFarAttack");
        }

        isNormalAttack = true;
    }

    public void NormalNearAttack_ani()
    {
        if (isInAttackRange.Equals(true))
        {
            cUtil._player.ReduceHp(damage.value, dir);
        }
    }

    public void NormalFarAttack_ani()
    {
        randomX = Random.Range(-150, 150);

        bulletManager.LaunchBullet(
    new Vector3(originObj.transform.position.x + randomX,
    originObj.transform.position.y + randomX,
    originObj.transform.position.z),
    false,
    cUtil._player.originObj.transform.position,
    bulletDamage, 15.0f
    );
    }

    public void NormalAttackDone_ani()
    {
        curNormalAttackCount += 1;
        isNormalAttack = false;
    }
    
    public void Dead()
    {
        originObj.transform.position = new Vector3(-10000, -10000, 0);
        PresentTalking(4, true);
        dp.StartBossVictory();
    }

    // 공격자가 있는 경우 공격자가 바라보는 방향을 pDir로 받음
    public override void ReduceHp(long pVal, Vector3 pDir, float pVelocity = 7.5f, float pDelay = 0)
    {
        if (isDead.Equals(true))
            return;

        //쉴드 중이면 리턴
        if (isSmashed.Equals(2))
            return;

        if (pDelay != 0)
        {
            delayHit = DelayHit_cor(pVal, pDir, pVelocity, pDelay);
            StartCoroutine(delayHit);
            return;
        }

        cUtil._soundMng.playAxeEffect(2);

        curHp.value -= pVal;

        hitCor = HitEffect_cor();
        StartCoroutine(hitCor);

        if (curHp.value <= 0)
        {
            curHp.value = 0;
            isDead = true;
            _animator.SetTrigger("Dead");
            originObj.GetComponent<BoxCollider2D>().enabled = false;
        }

        if (curHp.value < maxHp.value * 0.5f && isPattern2Started.Equals(false))
        {
            _animator.SetTrigger("PatternChange");
            rt.enabled = false;
            isPattern2Started = true;
        }
        else if (curHp.value <= maxHp.value * 0.5f && isHalfHpTalkingOn.Equals(false))
        {
            PresentTalking(3);
            isHalfHpTalkingOn = true;
        }

        //if (isDead.Equals(false))
        //    _animator.SetTrigger("GetHit");

        SetHp();
    }

    public void Pattern2ChangingDone_ani()
    {
        _animator.speed = _animator.speed * 1.5f;
        isPattern2ChangingDone = true;
        originObj.GetComponent<BoxCollider2D>().enabled = true;
        curPatternId += 1;
        curNormalAttackCount = 0;
        curBulletSpoilCount = 0;
        bulletManager = bulletManager_explode;
        
        isNormalAttack = false;
    }
}
