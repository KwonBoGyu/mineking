﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cBoss_theme1_stage1 : cEnemy_monster
{
    //Pattern1 변수
    private byte curNormalAttackCount; //n번 일반 공격 시 총알 난사
    private bool isSpoilBullet;
    private byte curBulletSpoilCount; //n번 총알 난사 시 궁극기
    private bool isSmashGround;
    public Transform smashGroundPos;
    public Transform nearGroundpos;
    private bool isJumpAttack;
    private float jumpAttackCoolTime;
    private float jumpAttackTimer;
    private float playerHp;
    public cBoss_theme1_stage1_JumpAttackBox jumpAttackBox;
    private bool isInJumpAttackRange; // cBoss_theme1_stage1_JumpAttackBox 스크립트에서 관리
    public void SetIsInJumpAttackRange(bool pIsIn) { isInJumpAttackRange = pIsIn; }

    private bool isNormalAttack;
    private const float normalAttackCoolTime = 3.0f;
    private float normalAttackCoolTimer;

    //Pattern2 변수
    public cBoss_theme1_stage1_sub[] subBoss;

    private Vector3 NormalNearAttackBoxPos;//local

    float randomX;
    float randomY;
    float randomX2;
    float randomY2;

    public void InitBoss()
    {
        base.Init(cEnemyTable.SetMonsterInfo(50));

        for(byte i = 0; i < subBoss.Length; i++)
        {
            subBoss[i].InitBoss();
            subBoss[i].originObj.SetActive(false);
        }

        jumpHeight = 600.0f;
        maxGravity = 3000.0f;
        jumpAttackCoolTime = 30.0f;
        jumpAttackBox.Init();
        bulletDamage = 2;
        NormalNearAttackBoxPos = new Vector3(-54, 39, -1);

        talkText = new string[5];
        talkText[0] = "빼애애애애ㅐㅐㅐ앰~~~!!! 등장 애지는 부분 ㅇㅈ??!!?!";
        talkText[1] = "오지게 갈겨부리쥬??? 당황했쥬??? 절대못피하쥬??";
        talkText[2] = "빼애액!! 무지개반사반오반육반칠!!!!";
        talkText[3] = "상상도 못한 딜량;; ㄴㅇㄱ봐줄생각 없나 혹쉬??";
        talkText[4] = "와;;; 님개쌔내;;; 애임핵각인대;;;;; 반박시 마알못이쥬?";
    }

    protected override void FixedUpdate()
    {
        //분열 전 보스 죽었으면..
        if (isDead.Equals(true))
            return;

        if (isInit.Equals(true))
        {
            base.FixedUpdate();      
        }
    }

    protected override void Move()
    {
        if (_animator != null)
            _animator.SetFloat("MoveSpeed", curMoveSpeed);

        //막히면 방향 바꿔준다.
        if (isRightBlocked == true)
        {
            isRightBlocked = false;
            ChangeDir(Vector3.left);
        }
        else if (isLeftBlocked == true)
        {
            isLeftBlocked = false;
            ChangeDir(Vector3.right);
        }

        //패턴
        if (curPatternId.Equals(0))
            Pattern1();
        else if (curPatternId.Equals(1))
            Pattern2();
    }

    private void Pattern1()
    {
        // 인식 범위 내 진입
        //isInAttackRange : 근접공격범위
        if (isInNoticeRange.Equals(true))
        {
            if(isFirstTalkingOn.Equals(false))
            {
                PresentTalking(0);
                isFirstTalkingOn = true;
            }

            playerPos = cUtil._player.gameObject.transform.position;

            if (playerPos.x >= this.transform.position.x)
                ChangeDir(Vector3.right);

            else if (playerPos.x < this.transform.position.x)
                ChangeDir(Vector3.left);

            //총알 난사 공격을 2회 했을 시 스매쉬
            if (curBulletSpoilCount.Equals(2) && isSpoilBullet.Equals(false) && isJumpAttack.Equals(false))
            {
                SmashGround();
                return;
            }
            //일반 공격을 5회 했을 시 총알 난사
            else if (curNormalAttackCount.Equals(3) && isNormalAttack.Equals(false) && isSmashGround.Equals(false) && isJumpAttack.Equals(false))
            {
                SpoilBullet();
                return;
            }
            //30초 이상 플레이어 체력 변화 없을 시 점프 공격
            else if ((jumpAttackTimer >= jumpAttackCoolTime) && isSpoilBullet.Equals(false) && isSmashGround.Equals(false) && isNormalAttack.Equals(false) && isJumpAttack.Equals(false))
            {
                JumpAttack();
                return;
            }
            //일반 원거리 공격
            else if (isInAttackRange.Equals(false) && isSpoilBullet.Equals(false) && isSmashGround.Equals(false) && isJumpAttack.Equals(false))
            {
                //공격 쿨타임 돌린다.
                normalAttackCoolTimer += Time.deltaTime;
                if (normalAttackCoolTimer >= normalAttackCoolTime)
                {
                    NormalAttack(false);
                    normalAttackCoolTimer = 0;
                }
            }
            //일반 근접 공격
            else if (isInAttackRange.Equals(true) && isSpoilBullet.Equals(false) && isSmashGround.Equals(false) && isJumpAttack.Equals(false))
            {
                //공격 쿨타임 돌린다.
                normalAttackCoolTimer += Time.deltaTime;
                if (normalAttackCoolTimer >= normalAttackCoolTime)
                {
                    NormalAttack(true);
                    normalAttackCoolTimer = 0;
                }
            }
            //플레이어 체력 변화시 타이머 초기화
            jumpAttackTimer += Time.deltaTime;
            if(playerHp != cUtil._player.GetCurHP().value)
            {
                playerHp = cUtil._player.GetCurHP().value;
                jumpAttackTimer = 0;
            }
        }
    }

    private void Pattern2()
    {

    }

    private void JumpAttack()
    {
        isJumpAttack = true;
        _animator.SetTrigger("JumpAttack");
    }
    public void Jump_ani()
    {
        StartCoroutine(Jump());
    }
    private void JumpAttack_ani()
    {
        dp.chg_camera.CameraShake(25.0f, 0.2f);
        if (isInJumpAttackRange)
        {
            cUtil._player.ReduceHp(cUtil._player.GetMaxHp().value / 4, Vector3.zero); // 임시 : 최대 체력 25퍼센트 데미지
        }
    }
    private void JumpAttackDone_ani()
    {
        jumpAttackTimer = 0;
        isJumpAttack = false;
    }
    
    private void SmashGround()
    {
        _animator.SetTrigger("SmashGround");
        curBulletSpoilCount = 0;

        isSmashGround = true;
        PresentTalking(2);
    }

    public void SmashGround_ani()
    {
        dp.chg_camera.CameraShake(25.0f, 0.2f);

        for (byte i = 0; i < 20; i++)
        {
            randomX = Random.Range(-100, 100);
            randomY = Random.Range(50, 100);
            randomX2 = Random.Range(-200, 200);
            randomY2 = Random.Range(100, 150);

            bulletManager.LaunchBullet(
                new Vector3(smashGroundPos.position.x + randomX,
                smashGroundPos.position.y + 10,
                smashGroundPos.position.z),
                true,
                new Vector3(smashGroundPos.position.x + randomX2,
                smashGroundPos.position.y + randomY2,
                smashGroundPos.position.z),
                bulletDamage, 15.0f, 15.0f);
        }
    }

    public void SmashDone_ani()
    {
        curNormalAttackCount = 0;
        curBulletSpoilCount = 0;
        isNormalAttack = false;
        isSpoilBullet = false;
        isSmashGround = false;
        _animator.SetBool("NormalNearAttack", false);
        _animator.SetBool("NormalFarAttack", false);
        _animator.SetBool("SpoilBullet", false);
    }

    private void SpoilBullet()
    {
        _animator.SetTrigger("SpoilBullet");
        curNormalAttackCount = 0;
        isSpoilBullet = true;

        PresentTalking(1);
    }

    public void SpoilBullet_ani()
    {
        randomY = Random.Range(-30, 30);
        bulletManager.LaunchBullet(originObj.transform.position, true, 
            new Vector3(cUtil._player.originObj.transform.position.x,
            cUtil._player.originObj.transform.position.y + randomY, 
            cUtil._player.originObj.transform.position.z) , 
            bulletDamage, 20.0f
            );
    }

    public void SpoilBulletDone_ani()
    {
        curBulletSpoilCount += 1;
        isSpoilBullet = false;
    }

    private void NormalAttack(bool isNear)
    {
        if(isNear.Equals(true))
        {
            _animator.SetTrigger("NormalNearAttack");
        }
        else
        {
            _animator.SetTrigger("NormalFarAttack");
        }

        isNormalAttack = true;
    }

    public void NormalNearAttack_ani()
    {
        if(isInAttackRange.Equals(true))
        {
            cUtil._player.ReduceHp(damage.value, dir);
        }
    }

    public void NormalFarAttack_ani()
    {
        bulletManager.LaunchBullet(originObj.transform.position, true, 
            cUtil._player.originObj.transform.position, 
            bulletDamage, 15.0f);
    }

    public void NormalAttackDone_ani()
    {
        curNormalAttackCount += 1;
        isNormalAttack = false;
    }

    public void SplitInit()
    {
        for (byte i = 0; i < subBoss.Length; i++)
        {
            originObj.transform.position = new Vector3(-10000, -10000, 0);
            curHp.value = maxHp.value;
            SetHp();
            subBoss[i].originObj.SetActive(true);
        }
    }

    public void Dead()
    {
        if (subBoss[0].isDead.Equals(false) || subBoss[1].isDead.Equals(false))
            return;

        PresentTalking(4, true);

        dp.StartBossVictory();
    }

    public void SlimeKingSetHp()
    {
        curHp.value = subBoss[0].GetCurHP().value + subBoss[1].GetCurHP().value;

        if (curHp.value <= 0)
        {
            coolTimer = 0;
            curHp.value = 0;
            isDead = true;
        }

        SetHp();        
    }
    
    // 공격자가 있는 경우 공격자가 바라보는 방향을 pDir로 받음
    public override void ReduceHp(long pVal, Vector3 pDir, float pVelocity = 7.5f, float pDelay = 0)
    {
        if (isDead.Equals(true))
            return;

        if (pDelay != 0)
        {
            delayHit = DelayHit_cor(pVal, pDir, pVelocity, pDelay);
            StartCoroutine(delayHit);
            return;
        }

        cUtil._soundMng.playAxeEffect(2);

        curHp.value -= pVal;

        hitCor = HitEffect_cor();
        StartCoroutine(hitCor);

        if (curHp.value <= 0)
        {
            curHp.value = 0;
            isDead = true;
            _animator.SetTrigger("Dead");
            _animator.SetBool("SplitInit", true);
            originObj.GetComponent<BoxCollider2D>().enabled = false;

            PresentTalking(3);
        }
        
        SetHp();
    }

    protected override IEnumerator Jump()
    {   
        float jumpTimer = 0;
        float factor;

        float currentHeight = originObj.transform.position.y;

        while (true)
        {
            yield return new WaitForFixedUpdate();
            
            isGrounded = false;

            factor = Mathf.PI * (jumpTimer / jumpTime) * 0.5f;

            originObj.transform.position = new Vector3(originObj.transform.position.x,
                currentHeight + jumpHeight * Mathf.Sin(factor), originObj.transform.position.z);

            if (jumpTimer >= jumpTime)
            {
                changingGravity = defaultGravity;
                break;
            }

            jumpTimer += Time.deltaTime;
        }

        changingGravity = 0;

        yield return new WaitForSeconds(0.3f);

        changingGravity = defaultGravity * 5f;
        bool nearGround = false;

        while(true)
        {
            yield return new WaitForFixedUpdate();
            RaycastHit2D[] hit = Physics2D.RaycastAll(this.originObj.transform.position, Vector2.down, 500.0f);
            Debug.DrawRay(this.originObj.transform.position, Vector3.down * 500.0f, Color.red);

            for (byte i = 0; i < hit.Length; i++)
            {
                if(hit[i].collider.gameObject.tag.Equals("Tile_cannotHit"))
                {
                    nearGround = true;
                    break;
                }
                if(hit[i].collider.gameObject.tag.Equals("Tile_canHit"))
                {
                    nearGround = true;
                    break;
                }
            }

            if(nearGround)
            {
                break;
            }
        }
        _animator.SetTrigger("JumpDone");
    }
}