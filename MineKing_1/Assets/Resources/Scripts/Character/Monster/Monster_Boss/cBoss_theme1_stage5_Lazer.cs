﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cBoss_theme1_stage5_Lazer : MonoBehaviour
{
    public Animator _animator;
    public CompositeCollider2D col;
    private long damage;

    private void Start()
    {
        damage = GameObject.Find("boss_TheHood").GetComponent<cBoss_theme1_stage5>().GetLazerDamage();
        this.gameObject.SetActive(false);      
    }

    public void Init(Vector2 pPos)
    {
        this.gameObject.transform.position = pPos;
        _animator.SetTrigger("Init");
    }

    public void ColliderOn()
    {
        col.enabled = true;
    }

    public void ColliderOff()
    {
        col.enabled = false;
    }

    public void LazerOff()
    {
        this.gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag.Equals("Player"))
        {
            cUtil._player.ReduceHp(damage, Vector3.zero);
        }

    }
}
