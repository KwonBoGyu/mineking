﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cBoss_theme1_stage3_ChargeAttackBox : MonoBehaviour
{
    private cBoss_theme1_stage3 script;

    public void Init()
    {
        script = this.transform.parent.GetChild(0).GetComponent<cBoss_theme1_stage3>();
        script.isCollidePlayer = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))
        {
            script.isCollidePlayer = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))
        {
            script.isCollidePlayer = false;
        }
    }
}
