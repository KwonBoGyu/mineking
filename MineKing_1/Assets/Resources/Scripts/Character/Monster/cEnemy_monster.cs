﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface IPassiveMonster
{
    bool isAttacked { get; set; }

    void DoThisWhenNotAttacked();
    void ReduceHp(long pVal, Vector3 pDir, float pVelocity = 7.5f, float pDelay = 0);
}

public class cEnemy_monster : cCharacter
{
    protected bool isInit;
    protected bool isGravity = true;

    //몬스터 행동 양식
    protected delegate void Skill(float pTime);
    protected byte curPatternId;

    public cBulletManager bulletManager; // 투사체 관련 BulletManager에서 관리
    protected long bulletDamage; // 투사체 데미지
    protected float bulletCoolTime;
    protected float bulletCoolTimer;
    public float GetBulletDamage() { return bulletDamage; }

    //초기 위치
    protected Vector3 InitPos;
    //몬스터의 생사 상태
    public bool isDead;
    //리스폰 시간, 타이머
    public float respawnTime;
    //몬스터 id
    public int id;
    public int GetId() { return id; }
    //몬스터 광물 보유량
    protected cProperty rocks;
    public cProperty GetRocks() { return rocks; }
    //키스톤 랜덤 드랍 확률
    public bool isKeyMonster;
    protected const int keyDropPercent = 100;
    // 플레이어 인식 여부, 공격 범위 위치 여부
    protected cEnemy_AttckBox attackBoxMng; // 공격 범위는 attackBox에서 관리함
    public cRangeNotizer notizer;
    public bool isInAttackRange;
    public bool isInNoticeRange;
    // (공격 행동시) 플레이어 위치
    protected Vector3 playerPos;
    // 리스폰 코루틴
    protected IEnumerator respawnCor;
    // 쿨타임 관리 변수
    protected float attackCoolTime;
    public float GetAttackCoolTime() { return attackCoolTime; }
    protected float coolTimer;
    protected float dirChangeTimer;

    public cAttackScript attackScript;

    public cDungeonNormal_processor dp;

    public Animator obj_hitEffect;
        
    public virtual void Init(enemyInitStruct pEs)
    {
        base.Init(pEs.nickName, pEs.damage, pEs.maxMoveSpeed, pEs.maxHp, pEs.curHp);
        rt = originObj.GetComponent<BoxCollider2D>();
        defaultGravity = 300.0f;
        changingGravity = defaultGravity;
        jumpHeight = 200.0f;
        id = pEs.id;
        rocks = new cProperty(pEs.rocks);
        isDead = false;
        respawnTime = 5.0f;
        InitPos = originObj.transform.localPosition;
        isInNoticeRange = false;
        playerPos = Vector3.zero;
        attackBoxPos[0] = new Vector3(1.5f, 0f, 0f);
        attackBoxPos[2] = new Vector3(-1.5f, 0f, 0f);
        respawnCor = RespawnTimer();
        curMoveSpeed = maxMoveSpeed;
        attackCoolTime = 3.0f;
        attackBoxMng = attackBox.GetComponent<cEnemy_AttckBox>();
        isInAttackRange = false;
        SetIsGrounded(false);
        _animator = this.GetComponent<Animator>();
        if(attackScript != null)
            attackScript.em = this;
        curPatternId = 0;

        notizer.Init();
        attackBoxMng.Init();

        if (originObj.transform.localScale.x.Equals(-1))
            ChangeDir(Vector3.left);
        else if (originObj.transform.localScale.x.Equals(1))
            ChangeDir(Vector3.right);

        isInit = true;
    }

    protected override void FixedUpdate()
    {
        if(isInit.Equals(true))
        {
            base.FixedUpdate();
            if (isDead.Equals(false))
            {
                if(isGravity)
                    SetGravity();
                Move();
            }
        }
    }
    
    //Defalut Move
    protected virtual void Move()
    {
        if (isDead.Equals(true))
            return;

        if(_animator != null)
             _animator.SetFloat("MoveSpeed", curMoveSpeed);
        
        playerPos = cUtil._player.gameObject.transform.position;
    }

    // ???
    //protected void IdleMove()
    //{
    //    dirChangeTimer += Time.deltaTime;
    //    if (dirChangeTimer > 2.0f && isInNoticeRange.Equals(true))
    //    {
    //        playerPos = cUtil._player.gameObject.transform.position;
    //        originObj.transform.Translate(dir * curMoveSpeed * Time.deltaTime);

    //        if (playerPos.x >= this.transform.position.x)
    //            ChangeDir(Vector3.right);

    //        else if (playerPos.x < this.transform.position.x)
    //            ChangeDir(Vector3.left);

    //        dirChangeTimer = 0;
    //    }
    //}


    // 대기상태
    protected void IdleMove()
    {
        curMoveSpeed = maxMoveSpeed;

        originObj.transform.Translate(dir * curMoveSpeed * Time.deltaTime);
        //막히면 방향 바꿔준다.
        if (isRightBlocked == true)
        {
            isRightBlocked = false;
            ChangeDir(Vector3.left);
        }
        else if (isLeftBlocked == true)
        {
            isLeftBlocked = false;
            ChangeDir(Vector3.right);
        }
    }

    // 근접공격
    protected void AttackMove()
    {
        originObj.transform.Translate(dir * curMoveSpeed * Time.deltaTime);
        
        if (isInAttackRange.Equals(true))
        {
            if (coolTimer.Equals(attackCoolTime))
            {
                if (playerPos.x >= this.transform.position.x)
                    ChangeDir(Vector3.right);
                else if (playerPos.x < this.transform.position.x)
                    ChangeDir(Vector3.left);

                coolTimer = 0;
                curMoveSpeed = 0;
                attackScript.SetAttackParameter(damage.value, dir);
                _animator.SetTrigger("Attack");
            }
        }
        else
        {
            if (playerPos.x >= this.transform.position.x)
                ChangeDir(Vector3.right);
            else if (playerPos.x < this.transform.position.x)
                ChangeDir(Vector3.left);
        }
    }

    // 쿨타임 관리 함수
    protected void CoolTimeMng()
    {
        if (coolTimer < attackCoolTime)
        {
            coolTimer += Time.deltaTime;
            if (coolTimer >= attackCoolTime)
            {
                coolTimer = attackCoolTime;
                curMoveSpeed = maxMoveSpeed;
            }
        }
        else
        {
            return;
        }
    }

    public virtual void ChangeDir(Vector3 pDir)
    {
        dir = pDir;

        if(dir.Equals(Vector3.right))
            originObj.transform.localScale = new Vector3(-1, 1, 1);
        else if(dir.Equals(Vector3.left))
            originObj.transform.localScale = new Vector3(1, 1, 1);
    }
    
    // 공격자가 있는 경우 공격자가 바라보는 방향을 pDir로 받음
    public override void ReduceHp(long pVal, Vector3 pDir, float pVelocity = 7.5f, float pDelay = 0)
    {
        if (isDead.Equals(true))
            return;

        if (pDelay != 0)
        {
            if (delayHit != null)
                StopCoroutine(delayHit);
            delayHit = DelayHit_cor(pVal, pDir, pVelocity, pDelay);
            StartCoroutine(delayHit);
            return;
        }

        //막히면 방향 바꿔준다.
        if (isRightBlocked == true && pDir.Equals(Vector3.left))
        {
            isRightBlocked = false;
        }
        else if (isLeftBlocked == true && pDir.Equals(Vector3.right))
        {
            isLeftBlocked = false;
        }

        cUtil._soundMng.playAxeEffect(2);
        curHp.value -= pVal;
        // 체력이 0 이하로 떨어질 시 코루틴 중지, 리스폰 타이머 활성화
        if (curHp.value <= 0)
        {
            if (isDead.Equals(true))
                return;

            curHp.value = 0;
            isDead = true;
            obj_hitEffect.SetTrigger("GetHit");
            _animator.SetTrigger("Dead");
            originObj.GetComponent<BoxCollider2D>().enabled = false;
            //키 몬스터라면..
            if (isKeyMonster.Equals(true))
            {
                if (Random.Range((int)1, (int)101) <= keyDropPercent)
                    dp.GetKeystone();
            }
            // 리스폰 타이머 활성화
            Respawn();
        }

        if (isDead.Equals(false))
        {
            _animator.SetTrigger("GetHit");
            obj_hitEffect.SetTrigger("GetHit");
        }

        hitCor = HitEffect_cor();
        StartCoroutine(hitCor);

        SetHp();

        // 넉백
        if (curHp.value > 0)
            StartKnockBack(pDir, pVelocity);
    }

    protected void Respawn()
    {
        StartCoroutine(RespawnTimer());
    }

    // 리스폰 관리
    protected IEnumerator RespawnTimer()
    {
        float time = 0;

        while (true)
        {
            yield return new WaitForFixedUpdate();

            if (time >= respawnTime)
            {
                RespawnInit();
                break;
            }

            time += Time.deltaTime;
        }
    }

    public virtual void RespawnInit()
    {
        originObj.GetComponent<BoxCollider2D>().enabled = true;
        originObj.transform.localPosition = InitPos;
        isDead = false;
        curHp.value = maxHp.value;
        curMoveSpeed = maxMoveSpeed;
        SetHp();
        Debug.Log("respawn");
        ChangeDir(dir);
        _animator.SetTrigger("Init");
    }

    ////////////////////보스 전용
    /// 0 : 시작할 때, 1 : 스킬1, 2 : 스킬2, 3 : 반피 이하 시작, 4 : 죽었을 때
    protected string[] talkText;
    protected bool isFirstTalkingOn;
    protected bool isHalfHpTalkingOn;
    public Text textObj;
    protected IEnumerator talkingCor;
    private const float talkingTime = 4.0f;
    private float talkingTimer;    

    //대사 표현
    protected virtual void PresentTalking(byte pIdx, bool pIsEnd = false)
    {
        //시작 대사 시작하지 않았다면 리턴
        if (pIdx != 0 && isFirstTalkingOn.Equals(false))
            return;

        talkingTimer = 0;
        textObj.text = talkText[pIdx];
        textObj.transform.parent.gameObject.SetActive(true);

        talkingCor = Talking_cor(pIsEnd);
        StopCoroutine(talkingCor);
        StartCoroutine(talkingCor);
    }

    IEnumerator Talking_cor(bool isEnd = false)
    {
        while (true)
        {
            yield return new WaitForFixedUpdate();
            talkingTimer += Time.deltaTime;

            if(talkingTimer >= talkingTime)
            {
                if(isEnd.Equals(true))
                    img_curHp.transform.parent.gameObject.SetActive(false);

                textObj.transform.parent.gameObject.SetActive(false);
                talkingTimer = 0;
                break;
            }
        }
    }
}
