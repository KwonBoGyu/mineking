﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cMonster_stage3_goblin_ranged : cEnemy_monster
{
    private bool isInDanger;
    private bool isRestoring;
    private cProperty restoringHP;
    private float distance;
    private float time;

    private Transform _player;
    private Vector3 target;

    private void Awake()
    {
        Init(cEnemyTable.SetMonsterInfo(7));
    }

    public override void Init(enemyInitStruct pEs)
    {
        base.Init(pEs);
        respawnTime = 5.0f;
        isInDanger = false;
        isRestoring = false;
        distance = 0;
        restoringHP = new cProperty("restore", 1); // 디버깅용 수치이므로 나중에 값 수정해야 함
        time = 0;

        attackCoolTime = 3.0f;
        bulletCoolTime = 3.0f;
        bulletDamage = (long)(damage.value * 0.5f);
    }

    protected override void Move()
    {
        base.Move();

        Pattern1();
    }

    private void Pattern1()
    {
        // 인식 범위 내 진입
        //isInAttackRange : 근접공격범위
        if (isInNoticeRange.Equals(true))
        {
            originObj.transform.Translate(dir * curMoveSpeed * Time.deltaTime);

            if (isInAttackRange.Equals(true))
            {
                curMoveSpeed = 0;
                if (coolTimer.Equals(attackCoolTime))
                {
                    Attack();
                }
            }
        }
        // idle 상태
        else if (isInNoticeRange.Equals(false))
        {
            originObj.transform.Translate(dir * curMoveSpeed * Time.deltaTime);
            //막히면 방향 바꿔준다.
            if (isRightBlocked == true)
            {
                isRightBlocked = false;
                ChangeDir(Vector3.left);
            }
            else if (isLeftBlocked == true)
            {
                isLeftBlocked = false;
                ChangeDir(Vector3.right);
            }
        }

        //공격 쿨타임
        if (coolTimer < attackCoolTime)
        {
            coolTimer += Time.deltaTime;
            if (coolTimer >= attackCoolTime)
            {
                coolTimer = attackCoolTime;
                curMoveSpeed = maxMoveSpeed;
            }
        }
    }

    private void Attack()
    {
        playerPos = cUtil._player.gameObject.transform.position;

        if (playerPos.x >= originObj.transform.position.x)
            ChangeDir(Vector3.right);
        else if (playerPos.x < originObj.transform.position.x)
            ChangeDir(Vector3.left);

        coolTimer = 0;
        curMoveSpeed = 0;
        attackScript.SetAttackParameter(damage.value, dir);
        _animator.SetTrigger("Attack");
    }

    public void LaunchBullet()
    {
        playerPos = cUtil._player.gameObject.transform.position;
        bulletManager.LaunchBullet(originObj.transform.position, true, playerPos, damage.value, 10.0f);

        if (playerPos.x >= originObj.transform.position.x)
            ChangeDir(Vector3.right);
        else if (playerPos.x < originObj.transform.position.x)
            ChangeDir(Vector3.left);
    }
}
