﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cMonster_stage1_mouse : cEnemy_monster, IPassiveMonster
{
    public bool isAttacked { get; set; }

    private void Awake()
    {
        Init(cEnemyTable.SetMonsterInfo(0));
    }

    public override void Init(enemyInitStruct pEs)
    {
        base.Init(pEs);
        respawnTime = 5.0f;
        attackCoolTime = 2.0f;
        ChangeDir(dir);

        isAttacked = false;
    }
    
    protected override void Move()
    {
        base.Move();

        Pattern1();
    }

    private void Pattern1()
    {
        // 공격받았을 때
        if(isAttacked)
        {
            // 인식 범위 내 진입
            //isInAttackRange : 근접공격범위
            if (isInNoticeRange.Equals(true))
            {
                AttackMove();
            }
            // 인식 범위 밖으로 벗어나면 비선공 상태로 전환
            else if (isInNoticeRange.Equals(false))
            {
                isAttacked = false;
                return;
            }

            //공격 쿨타임
            CoolTimeMng();
        }

        // 비선공 상태
        else
        {
            DoThisWhenNotAttacked();
        }
    }

    // 비선공 상태 행동
    public void DoThisWhenNotAttacked()
    {
        IdleMove();
    }

    //public override void ReduceHp(long pVal)
    //{
    //    isAttacked = true;
    //    base.ReduceHp(pVal);
    //}

    public override void ReduceHp(long pVal, Vector3 pDir, float pVelocity = 7.5f, float pDelay = 0)
    {
        isAttacked = true;
        base.ReduceHp(pVal, pDir, pVelocity, pDelay);
    }
}
