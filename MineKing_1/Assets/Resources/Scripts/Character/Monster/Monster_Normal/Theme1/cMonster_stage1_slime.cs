﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cMonster_stage1_slime : cEnemy_monster
{
    // 분열된 횟수
    public bool isSplited;

    public GameObject motherObj;
    private GameObject clone1;
    private GameObject clone2;

    public bool isClone1Dead;
    public bool isClone2Dead;

    public cMonster_stage1_slime()
    {
        isSplited = false;
    }

    private void Awake()
    {
        Init(cEnemyTable.SetMonsterInfo(1));
    }

    public override void Init(enemyInitStruct pEs)
    {
        base.Init(pEs);

        respawnTime = 5.0f;
        attackCoolTime = 3.0f;
        bulletCoolTime = 2.0f;
        coolTimer = 0;
        bulletDamage = (long)(damage.value);
        isClone1Dead = false;
        isClone2Dead = false;
    }

    protected override void Move()
    {
        base.Move();

        Pattern1();
    }

    private void Pattern1()
    {
        // 인식 범위 내 진입
        //isInAttackRange : 근접공격범위
        if (isInNoticeRange.Equals(true))
        {
            originObj.transform.Translate(dir * curMoveSpeed * Time.deltaTime);
            
            if (isInAttackRange.Equals(true))
            {
                curMoveSpeed = 0;
                if (coolTimer.Equals(attackCoolTime))
                {
                    Attack();
                }
            }
        }
        // idle 상태
        else if (isInNoticeRange.Equals(false))
        {
            originObj.transform.Translate(dir * curMoveSpeed * Time.deltaTime);
            //막히면 방향 바꿔준다.
            if (isRightBlocked == true)
            {
                isRightBlocked = false;
                ChangeDir(Vector3.left);
            }
            else if (isLeftBlocked == true)
            {
                isLeftBlocked = false;
                ChangeDir(Vector3.right);
            }
        }

        //공격 쿨타임
        if (coolTimer < attackCoolTime)
        {
            coolTimer += Time.deltaTime;
            if (coolTimer >= attackCoolTime)
            {
                coolTimer = attackCoolTime;
                curMoveSpeed = maxMoveSpeed;
            }
        }
    }

    private void Attack()
    {
        playerPos = cUtil._player.gameObject.transform.position;

        if (playerPos.x >= this.transform.position.x)
            ChangeDir(Vector3.right);
        else if (playerPos.x < this.transform.position.x)
            ChangeDir(Vector3.left);

        coolTimer = 0;
        curMoveSpeed = 0;
        attackScript.SetAttackParameter(damage.value, dir);
        _animator.SetTrigger("Attack");
    }

    public void LaunchBullet()
    {
        playerPos = cUtil._player.gameObject.transform.position;

        bulletManager.LaunchBullet(originObj.transform.position, true, playerPos, damage.value, 10.0f);
    }

    public void Split()
    {
        isSplited = true;
    }
    public void SetMotherObj(GameObject pObj)
    {
        motherObj = pObj;
    }

    public void StartRespawn()
    {
        if (isClone1Dead && isClone2Dead)
        {
            StartCoroutine(respawnCor);
        }
    }

    public override void ReduceHp(long pVal, Vector3 pDir, float pVelocity = 7.5f, float pDelay = 0)
    {
        if (pDelay != 0)
        {
            delayHit = DelayHit_cor(pVal, pDir, pVelocity, pDelay);
            StartCoroutine(delayHit);
        }

        curHp.value -= pVal;

        hitCor = HitEffect_cor();
        StartCoroutine(hitCor);

        // 넉백
        if (curHp.value > 0)
            StartKnockBack(pDir, pVelocity);

        SetHp();

        if (curHp.value <= 0)
        {
            curHp.value = 0;
            isDead = true;
            obj_hitEffect.SetTrigger("GetHit");
            _animator.SetTrigger("Dead");
            //img_curHp.transform.parent.gameObject.SetActive(false);
            originObj.GetComponent<BoxCollider2D>().enabled = false;

            // 이미 분열한 슬라임이라면
            if (isSplited)
            {
                if (!motherObj.GetComponent<cMonster_stage1_slime>().isClone1Dead)
                {
                    motherObj.GetComponent<cMonster_stage1_slime>().isClone1Dead = true;
                    this.gameObject.SetActive(false);
                    Destroy(this.transform.parent.gameObject);
                }
                else
                {
                    motherObj.GetComponent<cMonster_stage1_slime>().isClone2Dead = true;
                    this.gameObject.SetActive(false);

                    motherObj.GetComponent<cMonster_stage1_slime>().Respawn();
                    Destroy(this.transform.parent.gameObject);
                }
            }
            else
            {
                // 분열        
                clone1 = Instantiate(Resources.Load<GameObject>(cPath.PrefabPath() + "Monster/Monster_Slime"), new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z),
                    Quaternion.identity, this.transform.parent.parent.parent);
                clone1.transform.position = new Vector3(this.transform.position.x - 30, this.transform.position.y+ 10, this.transform.position.z);
                clone1.transform.localScale = new Vector3(1, 1, 1);
                clone1.transform.GetChild(0).GetComponent<cMonster_stage1_slime>().Split();
                clone1.transform.GetChild(0).GetComponent<cMonster_stage1_slime>().SetMotherObj(this.gameObject);
                clone1.transform.GetChild(0).GetComponent<cMonster_stage1_slime>().bulletManager = this.bulletManager;

                clone2 = Instantiate(Resources.Load<GameObject>(cPath.PrefabPath() + "Monster/Monster_Slime"), new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z),
                    Quaternion.identity, this.transform.parent.parent.parent);
                clone2.transform.position = new Vector3(this.transform.position.x + 30, this.transform.position.y+10, this.transform.position.z);
                clone2.transform.localScale = new Vector3(1, 1, 1);
                clone2.transform.GetChild(0).GetComponent<cMonster_stage1_slime>().Split();
                clone2.transform.GetChild(0).GetComponent<cMonster_stage1_slime>().SetMotherObj(this.gameObject);
                clone2.transform.GetChild(0).GetComponent<cMonster_stage1_slime>().bulletManager = this.bulletManager;
            }
        }
        else
        {
            _animator.SetTrigger("GetHit");
            obj_hitEffect.SetTrigger("GetHit");
        }
    }
}