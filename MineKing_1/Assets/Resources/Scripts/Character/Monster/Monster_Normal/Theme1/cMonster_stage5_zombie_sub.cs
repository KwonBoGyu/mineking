﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cMonster_stage5_zombie_sub : cEnemy_monster
{
    private void Awake()
    {
        Init(cEnemyTable.SetMonsterInfo(13));
        originObj.SetActive(false);
    }

    public override void Init(enemyInitStruct pEs)
    {
        base.Init(pEs);
        curMoveSpeed = curMoveSpeed * 0.5f;
        maxMoveSpeed = maxMoveSpeed * 0.5f;

        ChangeDir(dir);
    }
    
    protected override void Move()
    {
        base.Move();

        Pattern1();
    }

    private void Pattern1()
    {
        // 인식 범위 내 진입
        //isInAttackRange : 근접공격범위
        if (isInNoticeRange.Equals(true))
        {
            originObj.transform.Translate(dir * curMoveSpeed * Time.deltaTime);

            if (isInAttackRange.Equals(true))
            {
                if (coolTimer.Equals(attackCoolTime))
                {
                    playerPos = cUtil._player.gameObject.transform.position;

                    if (playerPos.x >= this.transform.position.x)
                        ChangeDir(Vector3.right);
                    else if (playerPos.x < this.transform.position.x)
                        ChangeDir(Vector3.left);

                    coolTimer = 0;
                    curMoveSpeed = 0;
                    attackScript.SetAttackParameter(damage.value, dir);
                    _animator.SetTrigger("Attack");
                }
            }
        }
        // idle 상태
        else if (isInNoticeRange.Equals(false))
        {
            originObj.transform.Translate(dir * curMoveSpeed * Time.deltaTime);
            //막히면 방향 바꿔준다.
            if (isRightBlocked == true)
            {
                isRightBlocked = false;
                ChangeDir(Vector3.left);
            }
            else if (isLeftBlocked == true)
            {
                isLeftBlocked = false;
                ChangeDir(Vector3.right);
            }
        }

        //공격 쿨타임
        if (coolTimer < attackCoolTime)
        {
            coolTimer += Time.deltaTime;
            if (coolTimer >= attackCoolTime)
            {
                coolTimer = attackCoolTime;
                curMoveSpeed = maxMoveSpeed;
            }
        }
    }

    //// 공격자가 있는 경우 공격자가 바라보는 방향을 pDir로 받음
    //public override void ReduceHp(long pVal)
    //{
    //    curHp.value -= pVal;

    //    // 체력이 0 이하로 떨어질 시 코루틴 중지, 리스폰 타이머 활성화
    //    if (curHp.value <= 0)
    //    {
    //        curHp.value = 0;
    //        isDead = true;
    //        obj_hitEffect.SetTrigger("GetHit");
    //        _animator.SetTrigger("Dead");
    //        originObj.GetComponent<BoxCollider2D>().enabled = false;
    //        //키 몬스터라면..
    //        if (isKeyMonster.Equals(true))
    //        {
    //            if (Random.Range((int)1, (int)101) <= keyDropPercent)
    //                dp.GetKeystone();
    //        }
    //        originObj.SetActive(false);
    //    }

    //    if (isDead.Equals(false))
    //    {
    //        _animator.SetTrigger("GetHit");
    //        obj_hitEffect.SetTrigger("GetHit");
    //    }
    //    SetHp();
    //}

    // 공격자가 있는 경우 공격자가 바라보는 방향을 pDir로 받음
    public override void ReduceHp(long pVal, Vector3 pDir, float pVelocity = 7.5f, float pDelay = 0)
    {
        curHp.value -= pVal;

        // 체력이 0 이하로 떨어질 시 코루틴 중지, 리스폰 타이머 활성화
        if (curHp.value <= 0)
        {
            curHp.value = 0;
            isDead = true;
            obj_hitEffect.SetTrigger("GetHit");
            _animator.SetTrigger("Dead");
            originObj.GetComponent<BoxCollider2D>().enabled = false;
            //키 몬스터라면..
            if (isKeyMonster.Equals(true))
            {
                if (Random.Range((int)1, (int)101) <= keyDropPercent)
                    dp.GetKeystone();
            }
            originObj.SetActive(false);
        }

        if (isDead.Equals(false))
        {
            _animator.SetTrigger("GetHit");
            obj_hitEffect.SetTrigger("GetHit");
        }
        SetHp();

        // 넉백
        if (curHp.value > 0)
            StartKnockBack(pDir, pVelocity);
    }
}
