﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cMonster_devil_sub : cEnemy_monster
{
    private cBoss_theme1_stage5 boss;
    public void SetMother(cBoss_theme1_stage5 pMother) { boss = pMother; }

    private void Start()
    {
        Init(cEnemyTable.SetMonsterInfo(12));
        attackBox.GetComponent<cEnemy_AttckBox>().Init();
        originObj.SetActive(false);
    }
    protected override void Move()
    {
        base.Move();

        Pattern1();
    }

    private void Pattern1()
    {
        playerPos = cUtil._player.transform.position;

        if (playerPos.x >= this.transform.position.x)
            ChangeDir(Vector3.right);
        else if (playerPos.x < this.transform.position.x)
            ChangeDir(Vector3.left);

        // 인식 범위 내 진입
        //isInAttackRange : 근접공격범위
        if (isInNoticeRange.Equals(true))
        {
            if (isInAttackRange.Equals(true))
            {
                if (coolTimer.Equals(attackCoolTime))
                {
                    coolTimer = 0;
                    attackScript.SetAttackParameter(damage.value, dir);
                    _animator.SetTrigger("Attack");
                }
                curMoveSpeed = 0;
            }
            else
            {
                originObj.transform.Translate(dir * curMoveSpeed * Time.deltaTime);
            }
        }

        //공격 쿨타임
        if (coolTimer < attackCoolTime)
        {
            coolTimer += Time.deltaTime;
            if (coolTimer >= attackCoolTime)
            {
                coolTimer = attackCoolTime;
                curMoveSpeed = maxMoveSpeed;
            }
        }
    }
    public void Dead_forDevil()
    {
        // 보스에게 알림
        boss.CheckClonesAllDead();
        this.originObj.SetActive(false);
    }

    // 공격자가 있는 경우 공격자가 바라보는 방향을 pDir로 받음
    public override void ReduceHp(long pVal, Vector3 pDir, float pVelocity = 7.5f, float pDelay = 0)
    {
        curHp.value -= pVal;
        
        if (curHp.value <= 0)
        {
            curHp.value = 0;
            isDead = true;
            rt.enabled = false;
            _animator.SetTrigger("Dead");
        }
        SetHp();

        // 넉백
        if (curHp.value > 0)
            StartKnockBack(pDir, pVelocity);
    }

    public override void RespawnInit()
    {
        base.RespawnInit();


    }
}
