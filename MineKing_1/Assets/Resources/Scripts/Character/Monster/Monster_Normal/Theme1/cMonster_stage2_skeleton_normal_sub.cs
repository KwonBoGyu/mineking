﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cMonster_stage2_skeleton_normal_sub : cEnemy_monster
{
    private void Awake()
    {
        Init(cEnemyTable.SetMonsterInfo(3));
        originObj.SetActive(false);
    }

    public override void Init(enemyInitStruct pEs)
    {
        base.Init(pEs);
        respawnTime = 5.0f;
        attackCoolTime = 2.0f;
        ChangeDir(dir);
    }

    protected override void Move()
    {
        base.Move();

        Pattern1();
    }

    private void Pattern1()
    {
        // 인식 범위 내 진입
        //isInAttackRange : 근접공격범위
        if (isInNoticeRange.Equals(true))
        {
            originObj.transform.Translate(dir * curMoveSpeed * Time.deltaTime);

            if (isInAttackRange.Equals(true))
            {
                if (coolTimer.Equals(attackCoolTime))
                {
                    playerPos = cUtil._player.gameObject.transform.position;

                    if (playerPos.x >= this.transform.position.x)
                        ChangeDir(Vector3.right);
                    else if (playerPos.x < this.transform.position.x)
                        ChangeDir(Vector3.left);

                    coolTimer = 0;
                    curMoveSpeed = 0;
                    attackScript.SetAttackParameter(damage.value, dir);
                    _animator.SetTrigger("Attack");
                }
            }
        }
        // idle 상태
        else if (isInNoticeRange.Equals(false))
        {
            originObj.transform.Translate(dir * curMoveSpeed * Time.deltaTime);
            //막히면 방향 바꿔준다.
            if (isRightBlocked == true)
            {
                isRightBlocked = false;
                ChangeDir(Vector3.left);
            }
            else if (isLeftBlocked == true)
            {
                isLeftBlocked = false;
                ChangeDir(Vector3.right);
            }
        }

        //공격 쿨타임
        if (coolTimer < attackCoolTime)
        {
            coolTimer += Time.deltaTime;
            if (coolTimer >= attackCoolTime)
            {
                coolTimer = attackCoolTime;
                curMoveSpeed = maxMoveSpeed;
            }
        }
    }
}
