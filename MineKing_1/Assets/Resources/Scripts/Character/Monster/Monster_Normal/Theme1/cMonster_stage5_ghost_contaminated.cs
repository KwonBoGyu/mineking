﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cMonster_stage5_ghost_contaminated : cEnemy_monster
{
    private float hideTime;
    private float time;
    private bool isHideOn;

    private void Awake()
    {
        Init(cEnemyTable.SetMonsterInfo(12));
    }

    public override void Init(enemyInitStruct pEs)
    {
        base.Init(pEs);
        respawnTime = 5.0f;
        defaultGravity = 300.0f;
        changingGravity = defaultGravity;
        //flyingRangeY = 0.5f;
        hideTime = 2.0f;
        time = 0;
        isHideOn = false;
    }

    protected override void Move()
    {
        base.Move();
        Pattern1();
    }

    private void Pattern1()
    {
        // 인식 범위 내 진입
        //isInAttackRange : 근접공격범위
        if (isInNoticeRange.Equals(true))
        {
            originObj.transform.Translate(dir * curMoveSpeed * Time.deltaTime);

            if (isInAttackRange.Equals(true))
            {
                if (coolTimer.Equals(attackCoolTime) && isHideOn.Equals(false))
                {
                    playerPos = cUtil._player.gameObject.transform.position;

                    if (playerPos.x >= this.transform.position.x)
                        ChangeDir(Vector3.right);

                    else if (playerPos.x < this.transform.position.x)
                        ChangeDir(Vector3.left);

                    coolTimer = 0;
                    curMoveSpeed = 0;
                    time = 0;
                    attackScript.SetAttackParameter(damage.value, dir);
                    _animator.SetTrigger("Attack");
                }
            }
        }
        // idle 상태
        else if (isInNoticeRange.Equals(false))
        {
            originObj.transform.Translate(dir * curMoveSpeed * Time.deltaTime);
            //막히면 방향 바꿔준다.
            if (isRightBlocked == true)
            {
                isRightBlocked = false;
                ChangeDir(Vector3.left);
            }
            else if (isLeftBlocked == true)
            {
                isLeftBlocked = false;
                ChangeDir(Vector3.right);
            }
        }

        //공격 쿨타임
        if (coolTimer < attackCoolTime)
        {
            coolTimer += Time.deltaTime;
            if (coolTimer >= attackCoolTime)
            {
                coolTimer = attackCoolTime;
                curMoveSpeed = maxMoveSpeed;
            }
        }

        if (isHideOn)
        {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, -1000f);
            this.transform.parent.tag = "Untagged";
        }
        else
        {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 0);
            this.transform.parent.tag = "Enemy";
        }

        time += Time.deltaTime;

        if (time >= hideTime && time < hideTime * 2)
        {
            isHideOn = true;
        }
        else if (time >= hideTime * 2)
        {
            isHideOn = false;
            time = 0;
        }
    }
}
