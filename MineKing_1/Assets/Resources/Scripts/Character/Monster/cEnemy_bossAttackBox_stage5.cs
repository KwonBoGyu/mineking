﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cEnemy_bossAttackBox_stage5 : cEnemy_bossAttackBox
{
    protected cBoss_theme1_stage5 script;
    private bool isDrainOn;
    public void SetDrainOn(bool pBool) { isDrainOn = pBool; }

    public override void Init(long pDamage)
    {
        script = this.transform.parent.GetChild(0).GetComponent<cBoss_theme1_stage5>();
        damage = pDamage;
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))
        {
            cUtil._player.ReduceHp(damage, script.GetDirection());

            if(isDrainOn)
            {

                script.PlayDrainEffect();
                script.RestoreHp((long)Mathf.Ceil((float)(damage * 0.03)));
            }

            this.gameObject.SetActive(false);
        }
    }
}
