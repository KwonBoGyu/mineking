﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cShield : MonoBehaviour
{
    private cBoss_theme1_stage2 boss;
    public BoxCollider2D collider;
    private bool isDestroyed;
    private long shieldMaxHp;
    private long shieldHp;

    protected IEnumerator hpCor;
    public GameObject ShieldHpObj;
    public Image shieldHpImg;
    public Text shieldHpText;

    public void Init()
    {
        boss = this.transform.parent.GetChild(0).GetComponent<cBoss_theme1_stage2>();
        shieldHp = boss.GetShieldHp();
        collider.enabled = false;
        ShieldHpObj.SetActive(false);
    }

    public void On()
    {
        ShieldHpObj.SetActive(true);
        shieldHpImg.fillAmount = 1.0f;
        collider.enabled = true;
        isDestroyed = false;
        shieldMaxHp = boss.GetShieldHp();
        shieldHp = shieldMaxHp;
        shieldHpText.text = shieldHp.ToString() + " / " + shieldMaxHp.ToString();
    }

    public void Off()
    {
        ShieldHpObj.SetActive(false);
        collider.enabled = false;
    }
    
    // 콜라이더 활성화 되어있을 때만 cWeapon에 의해 호출되어야 함
    public void ReduceShieldHp(long pVal)
    {
        cUtil._soundMng.playAxeEffect(1);
        shieldHp -= pVal;
        SetHp();

        if (shieldHp <= 0)
        {
            isDestroyed = true;
        }
    }

    public bool CheckShieldDestroyed()
    {
        return isDestroyed;
    }

    public virtual void SetHp()
    {
        if (shieldHpImg == null)
            return;

        if (hpCor != null)
            StopCoroutine(hpCor);

        hpCor = HpInterpolation();
        StartCoroutine(hpCor);
        shieldHpText.text = shieldHp.ToString() + " / " + shieldMaxHp.ToString();
    }

    protected IEnumerator HpInterpolation()
    {
        float prevAmount = shieldHpImg.fillAmount;
        float curAmount = (float)shieldHp / (float)shieldMaxHp;
        float tick = (curAmount - prevAmount) / 10f;

        while (true)
        {
            yield return new WaitForFixedUpdate();
            shieldHpImg.fillAmount = shieldHpImg.fillAmount + tick;
            if (Mathf.Abs(shieldHpImg.fillAmount - curAmount) <= Mathf.Abs(tick))
            {
                shieldHpImg.fillAmount = curAmount;
                break;
            }
        }
    }
}
