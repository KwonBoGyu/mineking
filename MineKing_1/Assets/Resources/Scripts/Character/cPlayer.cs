﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class cPlayer : cCharacter
{
    public SpriteRenderer img_weapon;
    public Transform lightPos;
    private bool isLightInit;

    public GameObject indicator;
    public Transform flagPos;
    public bool isFlagDone;
    public Transform nextDoorPos;

    public cWeapon weapon;
    public cFloatingText ft;
    public cInventory inven;
    public cUseManager useMng; // 아이템 사용 관리 클래스
    private bool isJumpAniDone;
    public cBtn_item quickSlot;


    private bool isPoision;
    private WaitForSeconds poisonTick = new WaitForSeconds(1.0f);

    private bool isSpeedUp;
    private float speedUpTime;
    private float speedUpAmount;

    private bool isMoveAttack;
    public bool GetIsMoveAttack() { return isMoveAttack; }
    private IEnumerator MoveOnAttackCor;

    public byte doubleAttackPercentage;
    private bool isFalling;

    //더블어택 확률 판별 변수
    byte doubleAt;

    // 크리티컬 공격 관련 변수
    private bool isCritAttack;
    public bool GetIsCritAttack() { return isCritAttack; }
    private float critAtk_TimeTick = 0.1f;
    private float atkTime = 0.5f; // 공격 지속 시간

    //이펙트
    private bool landEffectPlayed;

    public cDungeonNormal_processor dp;

    float lightDist;

    //스킨 아이디
    public byte skinId;

    //다중 공격 어택박스 위치
    private Vector3[] variableAttackPos;
    //임시 공격력
    private cProperty[] tempDamage;
    //콤보
    public byte comboCount;
    //콤보 없어지는 시간
    private float comboTime = 4.0f;
    private float comboTimer;
    private IEnumerator comboTimer_cor;

    //눈사람 충돌체크
    private cCheckPlayerColi cpc;
    
    public override void Init(string pNickName, cProperty pDamage, float pMoveSpeed, cProperty pMaxHp, cProperty pCurHp)
    {
        base.Init(pNickName, pDamage, pMoveSpeed, pMaxHp, pCurHp);

        if (cUtil._sm._scene != SCENE.SKIN)
            this.gameObject.tag = "Untagged";

        _animator = this.GetComponent<Animator>();
        originObj = this.transform.parent.gameObject;
        if (cUtil._user != null)
        {
            inven = cUtil._user.GetInventory();
        }
        rt = originObj.GetComponent<BoxCollider2D>();

        changingGravity = defaultGravity;
        SetIsGrounded(false);
        isClimbing = false;
        isSpeedUp = false;
        speedUpTime = 0.0f;
        speedUpAmount = 0.0f;
        jumpHeight = 200.0f;
        attackBoxPos[0] = new Vector3(-29, 280, -1.1f);
        attackBoxPos[1] = new Vector3(-180, 0, -1.1f);
        attackBoxPos[2] = new Vector3(-29, -280, -1.1f);
        attackBoxPos[3] = new Vector3(-150, 70, -1.1f);
        attackBox.transform.position = attackBoxPos[0];
        weapon.damage = damage;
        status = CHARACTERSTATUS.NONE;
        isCritAttack = false;
        isMoveAttack = false;
        isFalling = false;

        if (this.tag.Equals("player_skin"))
        {
            //스킬 레벨
            //대쉬
            byte dashFactor = 50;
            float dashCooltimeFactor = 0.5f;
            dashMoveSpeed = maxMoveSpeed + 500 + dashFactor * 0;
            maxDashCoolDown = 4.0f - dashCooltimeFactor * 0;
            dashCoolDown = maxDashCoolDown;
            //시야
            //byte lightFactor = 200;

            //GameObject.Find("SkinScene").transform.Find("HeadLight").GetComponent<cHeadLight>().
            //SetLightRange(lightFactor * 0);
            isLightInit = true;

            //차지 계수
            //cBtn_attack에서 초기화
            //연속공격
            doubleAttackPercentage = (byte)(0 + 10 * 0);
        }
        else
        {
            //스킬 레벨
            //대쉬
            byte dashFactor = 50;
            float dashCooltimeFactor = 0.5f;
            dashMoveSpeed = maxMoveSpeed + 500 + dashFactor * cUtil._user.GetSkillLevel(0);
            maxDashCoolDown = 4.0f - dashCooltimeFactor * cUtil._user.GetSkillLevel(0);
            dashCoolDown = maxDashCoolDown;
            
            //시야
            int lightFactor = 200;
            this.transform.parent.parent.parent.transform.Find("HeadLight").GetComponent<cHeadLight>().
            SetLightRange(lightFactor + cUtil._user.GetSkillLevel(1) * 50);
            Debug.Log(lightFactor + cUtil._user.GetSkillLevel(1) * 50);
            isLightInit = true;

            //차지 계수
            //cBtn_attack에서 초기화
            //연속공격
            doubleAttackPercentage = (byte)(0 + 10 * cUtil._user.GetSkillLevel(3));

            //내구도
            maxDp = new cProperty("MaxDp", cUtil._user.GetWeaponInfo().indurance.value);
            curDp = new cProperty("MaxDp", cUtil._user.GetWeaponInfo().indurance.value);

            SetDp();
        }

        //캐릭터 스프라이트 오더 조절
        Transform[] allT = this.transform.GetChild(0).GetComponentsInChildren<Transform>();

        for(byte i = 0; i < allT.Length; i++)
        {
            if (allT[i].GetComponent<Anima2D.SpriteMeshInstance>() == null && allT[i].GetComponent<SpriteRenderer>() == null)
                continue;
            else
            {
                if (allT[i].GetComponent<Anima2D.SpriteMeshInstance>() == null)
                {
                    allT[i].GetComponent<SpriteRenderer>().sortingOrder += 5000;
                }
                else
                    allT[i].GetComponent<Anima2D.SpriteMeshInstance>().sortingOrder += 5000;
            }
        }

        img_weapon.sprite = cUtil._user.WeaponEquipImages[cUtil._user.GetCurWeaponId()];
        img_weapon.transform.localScale = new Vector3(3.3884f, 3.3884f, 3.3884f);

        lightDist = 50;

        originObj.transform.localPosition = new Vector3(originObj.transform.localPosition.x, originObj.transform.localPosition.y, 0.5f);

        skinId = cUtil._user.GetCurClothId();

        if (skinId.Equals(6))
            cpc = originObj.GetComponent<cCheckPlayerColi>();

        variableAttackPos = new Vector3[15];

        tempDamage = new cProperty[2];
        for (byte i = 0; i < 2; i++)
            tempDamage[i] = new cProperty("adsf", 0);

        attackBox.SetActive(true);
        attackBox.transform.localPosition = attackBoxPos[1];

        if (skinId.Equals(2))
        {
            tempDamage[0].value = (long)(damage_crit.value * 0.8f);
            tempDamage[1].value = (long)(damage_crit.value * 0.5f);
        }
    }

    public void DoubleJump()
    {
        _animator.SetTrigger("DoubleJump");
    }

    public override void SetCurMoveSpeed(float pCurMoveSpeed)
    {
        if (isSpeedUp)
        {
            curMoveSpeed = pCurMoveSpeed * speedUpAmount;
            StartCoroutine("SpeedUpTime");
        }
        else
            curMoveSpeed = pCurMoveSpeed;
    }

    IEnumerator SpeedUpTime()
    {
        float time = 0;
        while (time <= speedUpTime)
        {
            yield return new WaitForFixedUpdate();
            time += Time.deltaTime;
        }
        isSpeedUp = false;
    }

    public void ChangeLightDist(float pDist, bool isPlus = false)
    {
        if (isPlus.Equals(true))
            lightDist += pDist;
        else
            lightDist -= pDist;
        
        originObj.transform.parent.parent.transform.Find("HeadLight").GetComponent<cHeadLight>().
            ChangeLightDist(150 + lightDist);
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        originObj.transform.Translate(dir * curMoveSpeed * Time.deltaTime);
        _animator.SetFloat("MoveSpeed", curMoveSpeed);
        _animator.SetBool("isGrounded", GetIsGrounded());

        //시야
        if (isLightInit.Equals(false))
        {
            float lightFactor = 100;
            originObj.transform.parent.parent.transform.Find("HeadLight").GetComponent<cHeadLight>().
            SetLightRange(lightFactor * cUtil._user.GetSkillLevel(1));
            isLightInit = true;
        }

        //인디케이터
        if (this.tag != "player_skin" && (int)cUtil._sm._scene < 15 && flagPos != null)
        {
            //점령지 점령 전
            if(isFlagDone.Equals(false))
            {
                Vector3 flagDir = (flagPos.position - originObj.transform.position).normalized;
                indicator.transform.position = new Vector3(originObj.transform.position.x + flagDir.x * 120,
                    originObj.transform.position.y + flagDir.y * 120, originObj.transform.position.z);
                indicator.transform.up = flagDir;
            }
            //점령 후
            else
            {
                if (nextDoorPos != null)
                {
                    Vector3 doorDir = (nextDoorPos.position - originObj.transform.position).normalized;
                    indicator.transform.position = new Vector3(originObj.transform.position.x + doorDir.x * 120,
                        originObj.transform.position.y + doorDir.y * 120, originObj.transform.position.z);
                    indicator.transform.up = doorDir;
                }
            }
        }

        //점프 모션
        if (GetIsGrounded().Equals(false) && isJumpAniDone.Equals(false)
            && isClimbing.Equals(false) && status != CHARACTERSTATUS.ATTACK
            && isDash != true )
        {
            _animator.SetTrigger("jumping");
            if (status.Equals(CHARACTERSTATUS.ATTACK))
                status = CHARACTERSTATUS.NONE;
            isJumpAniDone = true;
            landEffectPlayed = false;
            sm.StopRunningEffect();

        }
        else if (GetIsGrounded().Equals(true))
        {
            if (landEffectPlayed.Equals(false))
            {
                effects[0].Play();
                landEffectPlayed = true;
            }
            isJumpAniDone = false;
        }


        if (dir.Equals(Vector3.up))
            _animator.SetInteger("Direction", 0);
        else if (dir.Equals(Vector3.right))
            _animator.SetInteger("Direction", 1);
        else if (dir.Equals(Vector3.down))
            _animator.SetInteger("Direction", 2);
        else if (dir.Equals(Vector3.left))
            _animator.SetInteger("Direction", 3);

        if (charDir.Equals(CHARDIRECTION.NONE))
            _animator.SetInteger("Direction", 1);

        if (!isClimbing && GetIsGrounded().Equals(false) && isDash.Equals(false))
        {
            SetGravity();
        }
        else
        {
            changingGravity = defaultGravity;
        }

        if (!isRightBlocked && !isLeftBlocked)
        {
            SetIsClimbing(false);
        }
    }

    public override void SetGravity()
    {
        base.SetGravity();

        if (changingGravity >= 1000f && !isFalling)
        {
            StartCoroutine(SetFallDamage());
        }
    }

    IEnumerator SetFallDamage()
    {
        byte curJumpCount = jumpCount;
        float timer = 0;
        bool isFallDamageOn = false;
        isFalling = true;

        //Debug.Log("Fall Start");
        while (true)
        {
            if (isGrounded)
            {
                //Debug.Log("isGrounded");
                break;
            }
            
            if (isClimbing)
            {
                //Debug.Log("Reset Fall Timer : isClimbing");
                isFallDamageOn = false;
                timer = 0;
            }

            if (curJumpCount != jumpCount)
            {
                //Debug.Log("Reset Fall Timer : isJumpCount");
                isFallDamageOn = false;
                timer = 0;
            }

            if (isDash)
            {
               //Debug.Log("Reset Fall Timer : isDash");
                isFallDamageOn = false;
                timer = 0;
            }

            timer += Time.deltaTime;
            if (timer >= 0.5f)
            {
                //Debug.Log("fall damage on");
                isFallDamageOn = true;
            }

            yield return new WaitForFixedUpdate();
        }

        if (isFallDamageOn)
        {
            //Debug.Log("Reduced Hp for Falling : " + Mathf.Ceil(maxHp.value * 0.1f * timer));
            ReduceHp((long)(Mathf.Ceil(maxHp.value * 0.1f * timer)),Vector3.zero);
        }

        isFalling = false;
    }

    public void ChargeAttack_front()
    {
        _animator.SetTrigger("ChargeAttack_front");
        status = CHARACTERSTATUS.ATTACK;
    }
    public void ChargeAttack_up()
    {
        _animator.SetTrigger("ChargeAttack_up");
        status = CHARACTERSTATUS.ATTACK;
    }
    public void ChargeAttack_down()
    {
        _animator.SetTrigger("ChargeAttack_down");
        status = CHARACTERSTATUS.ATTACK;
    }

    public void Attack_front(bool pIsDounbleAttack = false)
    {
        if(pIsDounbleAttack.Equals(true))        
            _animator.speed = 3;
        else
            _animator.speed = 1;

        _animator.SetTrigger("AttackFront");
        status = CHARACTERSTATUS.ATTACK;
    }
    public void Attack_up(bool pIsDounbleAttack = false)
    {
        if (pIsDounbleAttack.Equals(true))
            _animator.speed = 3;
        else
            _animator.speed = 1;

        _animator.SetTrigger("AttackUp");
        status = CHARACTERSTATUS.ATTACK;
    }
    public void Attack_down(bool pIsDounbleAttack = false)
    {
        if (pIsDounbleAttack.Equals(true))
            _animator.speed = 3;
        else
            _animator.speed = 1;

        if (isClimbing.Equals(true))
            return;
        _animator.SetTrigger("AttackDown");
        status = CHARACTERSTATUS.ATTACK;
    }

    public void SetMoveOnAttack()
    {
        if (isCritAttack.Equals(true))
            return;

        if (MoveOnAttackCor != null)
            StopCoroutine(MoveOnAttackCor);

        MoveOnAttackCor = MoveOnAttack_cor();
        StartCoroutine(MoveOnAttackCor);
    }
    IEnumerator MoveOnAttack_cor()
    {
        curMoveSpeed = maxMoveSpeed * 3.5f;
        isMoveAttack = true;
        yield return new WaitForSeconds(0.1f);

        isMoveAttack = false;

        if (isCritAttack.Equals(false))            
            curMoveSpeed = 0f;
    }

    public void ActiveAttackBox(int pDir)
    {
        if (pDir > 2)
        {
            pDir -= 3;
            isCritical = true;
            damage_crit.value = (long)(damage.value * 2);
        }
        else
            isCritical = false;

        //위
        if (pDir == 0)
            attackBox.transform.localPosition = attackBoxPos[0];
        //양옆
        else if (pDir == 1)
        {
            if (isClimbing.Equals(true))
                attackBox.transform.localPosition = attackBoxPos[3];
            else
            {
                attackBox.transform.localPosition = attackBoxPos[1];
                if ((joystick.GetStickDir() == JOYSTICKDIR.LEFT))
                {
                    SetMoveOnAttack();
                }
                else if (joystick.GetStickDir() == JOYSTICKDIR.RIGHT)
                {
                    SetMoveOnAttack();
                }
            }
        }
        //아래
        else if (pDir == 2)
            attackBox.transform.localPosition = attackBoxPos[2];

        attackBox.SetActive(true);

        float t_tileHp = 0;
        long curDmg = isCritical ? damage_crit.value : damage.value;

        // 크리티컬 공격시 코루틴 진입
        if (isCritical)
        {
            //네모네모 마민킹은 크리공격 안한다.
            if (skinId.Equals(4))
                return;

            isCritAttack = true;
            
            StartCoroutine(CriticalAttackCor(curDmg, pDir));
        }
        // 일반 공격
        else
        {
            //더블어택 검사
            doubleAt = (byte)Random.Range(1, 101);

            //타일 공격
            {
                if (tileMng.CheckAttackedTile(attackBox.transform.position, curDmg, out t_tileHp, true).Equals(true))
                {
                    byte i = 1;

                    if (t_tileHp.Equals(0) && this.tag != "player_skin")
                    {
                        //네모네모 마민킹이라면 콤보중첩
                        if (skinId.Equals(4))
                        {
                            if (comboCount.Equals(0))
                            {
                                comboTimer_cor = ComboCount_cor();
                                StartCoroutine(comboTimer_cor);
                            }
                            //콤보 중첩시 콤보 타이머 초기화
                            else
                                comboTimer = 0;

                            comboCount += 1;
                            Debug.Log(comboCount);
                        }

                        quickSlot.UpdateQuickSlot();
                    }
                    else if (t_tileHp < 0.3f)
                        i = 3;
                    else if (t_tileHp < 0.7f)
                        i = 2;
                    else
                        i = 1;

                    effects[i].transform.position = attackBox.transform.position;
                    effects[i].transform.localScale = new Vector3(originObj.transform.localScale.x,
                        effects[i].transform.localScale.y, effects[i].transform.localScale.z);
                    effects[i].Play();

                    //더블어택 검사
                    doubleAt = (byte)Random.Range(1, 101);

                    if (isCritical.Equals(true))
                    {
                        effects[5].transform.position = attackBox.transform.position;
                        effects[5].Play();
                        sm.playAxeEffect(1);
                        ft.DamageTextOn(cUtil.GetValueToString(damage_crit.value), attackBox.transform.position, true);

                        if (doubleAt < doubleAttackPercentage)
                        {
                            ft.DamageTextOn(cUtil.GetValueToString(damage_crit.value) , attackBox.transform.position, true, false, 0.2f);
                            tileMng.CheckAttackedTile_delayed(attackBox.transform.position, curDmg, 0.2f);
                            sm.playAxeEffect(1, 0.2f);
                        }
                    }
                    else
                    {
                        effects[4].transform.position = attackBox.transform.position;
                        effects[4].Play();
                        sm.playAxeEffect(0);
                        ft.DamageTextOn(cUtil.GetValueToString(damage.value), attackBox.transform.position);

                        if (doubleAt < doubleAttackPercentage)
                        {
                            ft.DamageTextOn(cUtil.GetValueToString(damage.value), attackBox.transform.position, false, false, 0.2f);
                            tileMng.CheckAttackedTile_delayed(attackBox.transform.position, curDmg, 0.2f);
                            sm.playAxeEffect(0, 0.2f);
                        }
                    }

                    //네모네모 마민킹 콤보공격
                    if (skinId.Equals(4))
                    {
                        if (comboCount >= 6)
                        {
                            variableAttackPos[0] = new Vector3(attackBox.transform.position.x - 150,
                                 attackBox.transform.position.y + 150,
                                 attackBox.transform.position.z);
                            variableAttackPos[1] = new Vector3(attackBox.transform.position.x,
            attackBox.transform.position.y + 150,
            attackBox.transform.position.z);
                            variableAttackPos[2] = new Vector3(attackBox.transform.position.x + 150,
            attackBox.transform.position.y + 150,
            attackBox.transform.position.z);
                            variableAttackPos[3] = new Vector3(attackBox.transform.position.x - 150,
            attackBox.transform.position.y,
            attackBox.transform.position.z);
                            variableAttackPos[4] = new Vector3(attackBox.transform.position.x + 150,
                                attackBox.transform.position.y,
                                attackBox.transform.position.z);
                            variableAttackPos[5] = new Vector3(attackBox.transform.position.x - 150,
                                attackBox.transform.position.y - 150,
                                attackBox.transform.position.z);
                            variableAttackPos[6] = new Vector3(attackBox.transform.position.x,
                                attackBox.transform.position.y - 150,
                                attackBox.transform.position.z);
                            variableAttackPos[7] = new Vector3(attackBox.transform.position.x + 150,
                                attackBox.transform.position.y - 150,
                                attackBox.transform.position.z);

                            for (byte m = 0; m < 8; m++)
                            {
                                if (tileMng.CheckAttackedTile(variableAttackPos[m], curDmg, out t_tileHp, true).Equals(true))
                                {
                                    byte b = 1;

                                    if (t_tileHp.Equals(0) && this.tag != "player_skin")
                                        quickSlot.UpdateQuickSlot();
                                    else if (t_tileHp < 0.3f)
                                        b = 3;
                                    else if (t_tileHp < 0.7f)
                                        b = 2;
                                    else
                                        b = 1;

                                    effects[b].transform.position = variableAttackPos[m];
                                    effects[i].transform.localScale = new Vector3(originObj.transform.localScale.x,
                                        effects[b].transform.localScale.y, effects[b].transform.localScale.z);
                                    effects[b].Play();

                                    if (isCritical.Equals(true))
                                    {
                                        effects[5].transform.position = variableAttackPos[m];
                                        effects[5].Play();
                                        sm.playAxeEffect(1);
                                        ft.DamageTextOn(cUtil.GetValueToString(damage_crit.value), variableAttackPos[m], true);
                                    }
                                    else
                                    {
                                        effects[4].transform.position = variableAttackPos[m];
                                        effects[4].Play();
                                        sm.playAxeEffect(0);
                                        ft.DamageTextOn(cUtil.GetValueToString(damage.value), variableAttackPos[m]);
                                    }
                                }
                            }
                        }
                        else if (comboCount >= 3)
                        {
                            variableAttackPos[0] = new Vector3(attackBox.transform.position.x,
                                attackBox.transform.position.y + 150,
                                attackBox.transform.position.z);
                            variableAttackPos[1] = new Vector3(attackBox.transform.position.x + 150,
                                attackBox.transform.position.y,
                                attackBox.transform.position.z);
                            variableAttackPos[2] = new Vector3(attackBox.transform.position.x,
                                attackBox.transform.position.y - 150,
                                attackBox.transform.position.z);
                            variableAttackPos[3] = new Vector3(attackBox.transform.position.x - 150,
                                attackBox.transform.position.y,
                                attackBox.transform.position.z);

                            for (byte m = 0; m < 4; m++)
                            {
                                if (tileMng.CheckAttackedTile(variableAttackPos[m], curDmg, out t_tileHp, true).Equals(true))
                                {
                                    byte b = 1;

                                    if (t_tileHp.Equals(0) && this.tag != "player_skin")
                                        quickSlot.UpdateQuickSlot();
                                    else if (t_tileHp < 0.3f)
                                        b = 3;
                                    else if (t_tileHp < 0.7f)
                                        b = 2;
                                    else
                                        b = 1;

                                    effects[b].transform.position = variableAttackPos[m];
                                    effects[b].transform.localScale = new Vector3(originObj.transform.localScale.x,
                                        effects[b].transform.localScale.y, effects[b].transform.localScale.z);
                                    effects[b].Play();

                                    if (isCritical.Equals(true))
                                    {
                                        effects[5].transform.position = variableAttackPos[m];
                                        effects[5].Play();
                                        sm.playAxeEffect(1);
                                        ft.DamageTextOn(cUtil.GetValueToString(damage_crit.value), variableAttackPos[m], true);
                                    }
                                    else
                                    {
                                        effects[4].transform.position = variableAttackPos[m];
                                        effects[4].Play();
                                        sm.playAxeEffect(0);
                                        ft.DamageTextOn(cUtil.GetValueToString(damage.value), variableAttackPos[m]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //몬스터 공격
            {
                //공격 범위에 있으면 공격
                for(byte i = 0; i < weapon.l_targetMonsters.Count; i++)
                {
                    ft.DamageTextOn(cUtil.GetValueToString(damage.value), weapon.l_targetMonsters[i].transform.position);
                    weapon.l_targetMonsters[i].transform.GetChild(0).GetComponent<cEnemy_monster>().ReduceHp
                        (damage.value, cUtil._player.GetDirection(), 10.0f);
                    //cUtil._soundMng.playAxeEffect(2);

                    if (doubleAt < doubleAttackPercentage)
                    {
                        ft.DamageTextOn(cUtil.GetValueToString(damage.value), weapon.l_targetMonsters[i].transform.position, false, false, 0.2f);
                        weapon.l_targetMonsters[i].transform.GetChild(0).GetComponent<cEnemy_monster>().ReduceHp
                            (damage.value, cUtil._player.GetDirection(), 10.0f, 0.2f);
                        //cUtil._soundMng.playAxeEffect(2, 0.2f);
                    }
                }
            }


        }
    }
    public void InactiveAttackBox()
    {
        joystick.SetDirOnAttack();
        status = CHARACTERSTATUS.NONE;
        _animator.SetBool("AttackFront", false);
    }
    
    // 스매시 공격 코루틴
    IEnumerator CriticalAttackCor(long pCurDamage, int pDir)
    {
        long finalDmg = pCurDamage;

        byte hitCount = 0; // 타일 공격 횟수
        float t_tileHp = 0;

        dp.DisableController(); // 조작 불가 상태로 변경

        float time = 0; // 시간 재기 위한 임시 변수

        // 공격시 이동하는 속도 설정
        //족장
        if(skinId.Equals(3))
        {
            critAtk_TimeTick = 0.3f;
            curMoveSpeed = 180 / critAtk_TimeTick;
        }
        //잘못 자란 완두콩
        else if(skinId.Equals(5))
        {
            critAtk_TimeTick = 0.04f;
        }
        //눈사람
        else if(skinId.Equals(6))
        {
            curMoveSpeed = 180 / critAtk_TimeTick;
        }
        //마녀 곽정팔
        else if(skinId.Equals(7))
        {

        }
        //나무늘보
        else if (skinId.Equals(8))
        {
            critAtk_TimeTick = 0.4f;
        }
        else
        {
            critAtk_TimeTick = 0.1f;

            if (pDir.Equals(0))
            {
                curMoveSpeed = (180 / critAtk_TimeTick) + changingGravity;
            }
            else if (pDir.Equals(1))
            {
                curMoveSpeed = 180 / critAtk_TimeTick;
            }
            else if (pDir.Equals(2))
            {
                curMoveSpeed = (180 / critAtk_TimeTick) - changingGravity;
            }
        }

        
        while (true)
        {
            time += critAtk_TimeTick;

            //족장, 눈사람
            if(skinId.Equals(3) || skinId.Equals(6))
            {
                
            }
            //완두콩
            else if(skinId.Equals(5))
            {
                if (time >= atkTime * 0.4f)
                {
                    dp.EnableController();
                    status = CHARACTERSTATUS.NONE;
                    break;
                }
            }
            else if(skinId.Equals(8))
            {
                useMng.SetBomb(new Vector3(Random.Range(-0.5f, 0.5f),
                    Random.Range(0.4f, 0.9f), originObj.transform.position.z));
            }
            //나머지
            else
            {
                if (time >= atkTime)
                {
                    dp.EnableController();
                    status = CHARACTERSTATUS.NONE;
                    break;
                }
            }
            
            // 도중에 공격받을경우 취소
            if (status.Equals(CHARACTERSTATUS.KNOCKBACK))
            {
                curMoveSpeed = 0;

                //족장일 경우 2초 그로기
                if (skinId.Equals(3))
                {
                    yield return new WaitForSeconds(2.0f);
                }
                
                dp.EnableController();
                status = CHARACTERSTATUS.NONE;
                break;
            }

            if (isCritAttack.Equals(false))
            {
                status = CHARACTERSTATUS.NONE;
                break;
            }

            //몬스터 공격
            {
                for(byte i = 0; i < weapon.l_targetMonsters.Count; i++)
                {
                    //눈사람
                    if (skinId.Equals(6))
                    {
                        useMng.SetBomb(Vector3.zero, 1);
                        EndCriticalAttack();
                    }

                    ft.DamageTextOn(cUtil.GetValueToString(damage_crit.value), weapon.l_targetMonsters[i].transform.position, true);

                    //길뚫는 바이킹 : 넉백 거리 증가, 크리공격력 * 0.8배 
                    if (skinId.Equals(2))
                    {
                        weapon.l_targetMonsters[i].transform.GetChild(0).GetComponent<cEnemy_monster>().ReduceHp
                            ((long)(damage_crit.value * 0.8f), cUtil._player.GetDirection(), 20);
                    }
                    //나머지
                    else
                    {
                        weapon.l_targetMonsters[i].transform.GetChild(0).GetComponent<cEnemy_monster>().ReduceHp
                            (damage_crit.value, cUtil._player.GetDirection(), 14);
                    }

                    //cUtil._soundMng.playAxeEffect(2);
                }

                
            }

            //길뚫는 바이킹 : 타일크리공격 *0.8, 0.5
            if (skinId.Equals(2))
            {
                // 타일이 있으면 공격처리
                if (tileMng.CheckAttackedTile(attackBox.transform.position, tempDamage[0].value, out t_tileHp, true).Equals(true))
                {
                    hitCount++; // 타일 공격 횟수 증가

                    byte i = 1;

                    if (t_tileHp.Equals(0) && this.tag != "player_skin")
                        quickSlot.UpdateQuickSlot();
                    else if (t_tileHp < 0.3f)
                        i = 3;
                    else if (t_tileHp < 0.7f)
                        i = 2;
                    else
                        i = 1;

                    effects[i].transform.position = attackBox.transform.position;
                    effects[i].transform.localScale = new Vector3(originObj.transform.localScale.x,
                        effects[i].transform.localScale.y, effects[i].transform.localScale.z);
                    effects[i].Play();

                    effects[5].transform.position = attackBox.transform.position;
                    effects[5].Play();
                    sm.playAxeEffect(1);
                    ft.DamageTextOn(cUtil.GetValueToString(tempDamage[0].value), attackBox.transform.position, true);
                }

                //가로방향
                if (pDir.Equals(1))
                {
                    //위
                    variableAttackPos[0] = new Vector3(attackBox.transform.position.x, attackBox.transform.position.y + 150, attackBox.transform.position.z);
                    //아래
                    variableAttackPos[1] = new Vector3(attackBox.transform.position.x, attackBox.transform.position.y - 150, attackBox.transform.position.z);

                    //위, 일반공격 데미지로
                    if (tileMng.CheckAttackedTile(variableAttackPos[0], tempDamage[1].value, out t_tileHp, true).Equals(true))
                    {
                        hitCount++; // 타일 공격 횟수 증가

                        byte i = 1;

                        if (t_tileHp.Equals(0) && this.tag != "player_skin")
                            quickSlot.UpdateQuickSlot();
                        else if (t_tileHp < 0.3f)
                            i = 3;
                        else if (t_tileHp < 0.7f)
                            i = 2;
                        else
                            i = 1;

                        effects[i].transform.position = attackBox.transform.position;
                        effects[i].transform.localScale = new Vector3(originObj.transform.localScale.x,
                            effects[i].transform.localScale.y, effects[i].transform.localScale.z);
                        effects[i].Play();

                        effects[5].transform.position = attackBox.transform.position;
                        effects[5].Play();
                        sm.playAxeEffect(1);
                        ft.DamageTextOn(cUtil.GetValueToString(tempDamage[1].value), variableAttackPos[0], true);
                    }
                    //아래
                    if (tileMng.CheckAttackedTile(variableAttackPos[1], tempDamage[1].value, out t_tileHp, true).Equals(true))
                    {
                        hitCount++; // 타일 공격 횟수 증가

                        byte i = 1;

                        if (t_tileHp.Equals(0) && this.tag != "player_skin")
                            quickSlot.UpdateQuickSlot();
                        else if (t_tileHp < 0.3f)
                            i = 3;
                        else if (t_tileHp < 0.7f)
                            i = 2;
                        else
                            i = 1;

                        effects[i].transform.position = attackBox.transform.position;
                        effects[i].transform.localScale = new Vector3(originObj.transform.localScale.x,
                            effects[i].transform.localScale.y, effects[i].transform.localScale.z);
                        effects[i].Play();

                        effects[5].transform.position = attackBox.transform.position;
                        effects[5].Play();
                        sm.playAxeEffect(1);
                        ft.DamageTextOn(cUtil.GetValueToString(tempDamage[1].value), variableAttackPos[1], true);
                    }
                }
                //세로방향
                else
                {
                    //오른쪽
                    variableAttackPos[0] = new Vector3(attackBox.transform.position.x + 150, attackBox.transform.position.y, attackBox.transform.position.z);
                    //왼쪽
                    variableAttackPos[1] = new Vector3(attackBox.transform.position.x - 150, attackBox.transform.position.y, attackBox.transform.position.z);

                    //위, 일반공격 데미지로
                    if (tileMng.CheckAttackedTile(variableAttackPos[0], tempDamage[1].value, out t_tileHp, true).Equals(true))
                    {
                        hitCount++; // 타일 공격 횟수 증가

                        byte i = 1;

                        if (t_tileHp.Equals(0) && this.tag != "player_skin")
                            quickSlot.UpdateQuickSlot();
                        else if (t_tileHp < 0.3f)
                            i = 3;
                        else if (t_tileHp < 0.7f)
                            i = 2;
                        else
                            i = 1;

                        effects[i].transform.position = attackBox.transform.position;
                        effects[i].transform.localScale = new Vector3(originObj.transform.localScale.x,
                            effects[i].transform.localScale.y, effects[i].transform.localScale.z);
                        effects[i].Play();

                        effects[5].transform.position = attackBox.transform.position;
                        effects[5].Play();
                        sm.playAxeEffect(1);
                        ft.DamageTextOn(cUtil.GetValueToString(tempDamage[1].value), variableAttackPos[0], true);
                    }
                    //아래
                    if (tileMng.CheckAttackedTile(variableAttackPos[1], tempDamage[1].value, out t_tileHp, true).Equals(true))
                    {
                        hitCount++; // 타일 공격 횟수 증가

                        byte i = 1;

                        if (t_tileHp.Equals(0) && this.tag != "player_skin")
                            quickSlot.UpdateQuickSlot();
                        else if (t_tileHp < 0.3f)
                            i = 3;
                        else if (t_tileHp < 0.7f)
                            i = 2;
                        else
                            i = 1;

                        effects[i].transform.position = attackBox.transform.position;
                        effects[i].transform.localScale = new Vector3(originObj.transform.localScale.x,
                            effects[i].transform.localScale.y, effects[i].transform.localScale.z);
                        effects[i].Play();

                        effects[5].transform.position = attackBox.transform.position;
                        effects[5].Play();
                        sm.playAxeEffect(1);
                        ft.DamageTextOn(cUtil.GetValueToString(tempDamage[1].value), variableAttackPos[1], true);
                    }
                }

            }
            //완두콩 어택박스 방향으로
            else if (skinId.Equals(5))
            {
                //위 방향으로
                if (pDir.Equals(0))
                {
                    attackBox.transform.position = new Vector3(attackBox.transform.position.x,
                        attackBox.transform.position.y + 150, attackBox.transform.position.z);
                }
                else if (pDir.Equals(1) && dir.Equals(Vector3.right))
                {
                    attackBox.transform.position = new Vector3(attackBox.transform.position.x + 150,
                        attackBox.transform.position.y, attackBox.transform.position.z);
                }
                else if (pDir.Equals(2))
                {
                    attackBox.transform.position = new Vector3(attackBox.transform.position.x,
    attackBox.transform.position.y - 150, attackBox.transform.position.z);
                }
                else if (pDir.Equals(1) && dir.Equals(Vector3.left))
                {
                    attackBox.transform.position = new Vector3(attackBox.transform.position.x - 150,
           attackBox.transform.position.y, attackBox.transform.position.z);
                }

                //위, 일반공격 데미지로
                if (tileMng.CheckAttackedTile(attackBox.transform.position, damage_crit.value, out t_tileHp, true).Equals(true))
                {
                    hitCount++; // 타일 공격 횟수 증가

                    byte i = 1;

                    if (t_tileHp.Equals(0) && this.tag != "player_skin")
                        quickSlot.UpdateQuickSlot();
                    else if (t_tileHp < 0.3f)
                        i = 3;
                    else if (t_tileHp < 0.7f)
                        i = 2;
                    else
                        i = 1;

                    effects[i].transform.position = attackBox.transform.position;
                    effects[i].transform.localScale = new Vector3(originObj.transform.localScale.x,
                        effects[i].transform.localScale.y, effects[i].transform.localScale.z);
                    effects[i].Play();

                    effects[5].transform.position = attackBox.transform.position;
                    effects[5].Play();
                    sm.playAxeEffect(1);
                    ft.DamageTextOn(cUtil.GetValueToString(damage_crit.value), attackBox.transform.position, true);
                }
            }
            //눈사람 -> checkPlayerColi 스크립트에서 제어
            if (skinId.Equals(6))
            {
                // 타일이 있으면 공격처리
                if (tileMng.CheckAttackedTile(attackBox.transform.position, 0, out t_tileHp, true).Equals(true))
                {
                    curMoveSpeed = 0;

                    useMng.SetBomb(Vector3.zero, 1);
                    EndCriticalAttack();
                }
                else
                {
                    //눈사람 오른쪽 충돌
                    if (pDir.Equals(1) && dir.Equals(Vector3.right) && isRightBlocked.Equals(true))
                    {
                        curMoveSpeed = 0;

                        useMng.SetBomb(Vector3.zero, 1);
                        EndCriticalAttack();
                        break;
                    }
                    //눈사람 왼쪽 충돌
                    else if (pDir.Equals(1) && dir.Equals(Vector3.left) && isLeftBlocked.Equals(true))
                    {
                        curMoveSpeed = 0;

                        useMng.SetBomb(Vector3.zero, 1);
                        EndCriticalAttack();
                        break;
                    }
                }
            }
            //마녀 곽정팔
            else if (skinId.Equals(7))
            {
                // 타일이 있으면 공격처리
                if (tileMng.CheckAttackedTile(attackBox.transform.position, pCurDamage, out t_tileHp, true).Equals(true))
                {
                    hitCount++; // 타일 공격 횟수 증가

                    byte i = 1;

                    if (t_tileHp.Equals(0) && this.tag != "player_skin")
                        quickSlot.UpdateQuickSlot();
                    else if (t_tileHp < 0.3f)
                        i = 3;
                    else if (t_tileHp < 0.7f)
                        i = 2;
                    else
                        i = 1;

                    effects[i].transform.position = attackBox.transform.position;
                    effects[i].transform.localScale = new Vector3(originObj.transform.localScale.x,
                        effects[i].transform.localScale.y, effects[i].transform.localScale.z);
                    effects[i].Play();

                    effects[5].transform.position = attackBox.transform.position;
                    effects[5].Play();
                    sm.playAxeEffect(1);
                    ft.DamageTextOn(cUtil.GetValueToString(damage_crit.value), attackBox.transform.position, true);

                    //오른쪽 방향
                    if (pDir.Equals(1) && dir.Equals(Vector3.right))
                    {
                        for (byte k = 0; k < 5; k++)
                        {
                            for (byte m = 0; m < 3; m++)
                            {
                                if (k.Equals(0) && m.Equals(1))
                                {
                                    continue;
                                }

                                variableAttackPos[(k * 3) + m] = new Vector3(attackBox.transform.position.x + 150 * k,
                                    attackBox.transform.position.y - 150 + (150 * m), attackBox.transform.position.z);
                            }
                        }
                    }
                    //위 방향
                    else if (pDir.Equals(0) && dir.Equals(Vector3.up))
                    {
                        for (byte k = 0; k < 5; k++)
                        {
                            for (byte m = 0; m < 3; m++)
                            {
                                if (k.Equals(0) && m.Equals(1))
                                {
                                    continue;
                                }

                                variableAttackPos[(k * 3) + m] = new Vector3(attackBox.transform.position.x - 150 + (150 * m),
                                    attackBox.transform.position.y + (150 * k), attackBox.transform.position.z);
                            }
                        }
                    }
                    //아래 방향
                    else if (pDir.Equals(2) && dir.Equals(Vector3.down))
                    {
                        for (byte k = 0; k < 5; k++)
                        {
                            for (byte m = 0; m < 3; m++)
                            {
                                if (k.Equals(0) && m.Equals(1))
                                {
                                    continue;
                                }

                                variableAttackPos[(k * 3) + m] = new Vector3(attackBox.transform.position.x - 150 + (150 * m),
                                    attackBox.transform.position.y - (150 * k), attackBox.transform.position.z);
                            }
                        }
                    }
                    //왼쪽 방향
                    if (pDir.Equals(1) && dir.Equals(Vector3.left))
                    {
                        for (byte k = 0; k < 5; k++)
                        {
                            for (byte m = 0; m < 3; m++)
                            {
                                if (k.Equals(0) && m.Equals(1))
                                {
                                    continue;
                                }

                                variableAttackPos[(k * 3) + m] = new Vector3(attackBox.transform.position.x - 150 * k,
                                    attackBox.transform.position.y - 150 + (150 * m), attackBox.transform.position.z);
                            }
                        }
                    }

                    for (byte k = 0; k < 15; k++)
                    {
                        if (k.Equals(1))
                            continue;

                        // 타일이 있으면 공격처리
                        if (tileMng.CheckAttackedTile(variableAttackPos[k], pCurDamage, out t_tileHp, true).Equals(true))
                        {
                            hitCount++; // 타일 공격 횟수 증가

                            byte b = 1;

                            if (t_tileHp.Equals(0) && this.tag != "player_skin")
                                quickSlot.UpdateQuickSlot();
                            else if (t_tileHp < 0.3f)
                                b = 3;
                            else if (t_tileHp < 0.7f)
                                b = 2;
                            else
                                b = 1;

                            effects[b].transform.position = variableAttackPos[k];
                            effects[b].transform.localScale = new Vector3(originObj.transform.localScale.x,
                                effects[b].transform.localScale.y, effects[b].transform.localScale.z);
                            effects[b].Play();

                            effects[5].transform.position = variableAttackPos[k];
                            effects[5].Play();
                            sm.playAxeEffect(1);
                            ft.DamageTextOn(cUtil.GetValueToString(damage_crit.value), variableAttackPos[k], true);
                        }
                    }
                }
            }
            //단련, 회춘한 광부, 족장
            else
            {
                // 타일이 있으면 공격처리
                if (tileMng.CheckAttackedTile(attackBox.transform.position, pCurDamage, out t_tileHp, true).Equals(true))
                {
                    hitCount++; // 타일 공격 횟수 증가

                    byte i = 1;

                    if (t_tileHp.Equals(0) && this.tag != "player_skin")
                        quickSlot.UpdateQuickSlot();
                    else if (t_tileHp < 0.3f)
                        i = 3;
                    else if (t_tileHp < 0.7f)
                        i = 2;
                    else
                        i = 1;

                    effects[i].transform.position = attackBox.transform.position;
                    effects[i].transform.localScale = new Vector3(originObj.transform.localScale.x,
                        effects[i].transform.localScale.y, effects[i].transform.localScale.z);
                    effects[i].Play();

                    effects[5].transform.position = attackBox.transform.position;
                    effects[5].Play();
                    sm.playAxeEffect(1);
                    ft.DamageTextOn(cUtil.GetValueToString(damage_crit.value), attackBox.transform.position, true);
                }
                else
                {
                    if(skinId.Equals(3))
                    {
                        //족장 오른쪽 충돌
                        if (pDir.Equals(1) && dir.Equals(Vector3.right) && isRightBlocked.Equals(true))
                        {
                            curMoveSpeed = 0;

                            yield return new WaitForSeconds(2.0f);

                            dp.EnableController();
                            status = CHARACTERSTATUS.NONE;
                            break;
                        }
                        //족장 왼쪽 충돌
                        else if (pDir.Equals(1) && dir.Equals(Vector3.left) && isLeftBlocked.Equals(true))
                        {
                            curMoveSpeed = 0;

                            yield return new WaitForSeconds(2.0f);

                            dp.EnableController();
                            status = CHARACTERSTATUS.NONE;
                            break;
                        }
                    }
                }
            }

            //마녀 곽정팔
            if (skinId.Equals(7))
            {            
                status = CHARACTERSTATUS.NONE;
                break;
            }                     
            
            // 공격 틱만큼 기다렸다가 반복
            yield return new WaitForSeconds(critAtk_TimeTick);
        }

        dp.EnableController(); // 조작 가능상태로 변경
        isCritAttack = false;
        curMoveSpeed = 0;
    }

    public void EndCriticalAttack()
    {
        isCritAttack = false;
        dp.EnableController(); // 조작 가능상태로 변경
        curMoveSpeed = 0;
        status = CHARACTERSTATUS.NONE;
        InactiveAttackBox();
    }
    
    //public override void ReduceHp(long pVal)
    //{
    //    // 플레이어 대쉬 행동시 회피 !!!!! 임시로 Miss 대신 0 출력
    //    if (status == CHARACTERSTATUS.DASH)
    //    {
    //        ft.DamageTextOn("0", originObj.transform.position, false, true);
    //        return;
    //    }

    //    base.ReduceHp(pVal);

    //    //30 이하일 때 적색 프레임 on
    //    if (curHp.value <= maxHp.value * 0.3f && dp.img_lowHp.activeSelf.Equals(false))
    //        dp.img_lowHp.SetActive(true);

    //    ft.DamageTextOn(pVal.ToString(), originObj.transform.position, false, true);

    //    ////죽으면 메인메뉴로
    //    //if (curHp.value.Equals(0))
    //    //    StartCoroutine(Dead_cor());
    //}

    public override void ReduceHp(long pVal, Vector3 pDir, float pVelocity = 7.5f, float pDelay = 0)
    {
        // 플레이어 대쉬 행동시 회피 !!!!! 임시로 Miss 대신 0 출력
        if (status == CHARACTERSTATUS.DASH)
        {
            ft.DamageTextOn("0", originObj.transform.position, false, true);
            return;
        }

        if (isCritAttack.Equals(true) && skinId != 8)
        {
            ft.DamageTextOn("0", originObj.transform.position, false, true);
            return;
        }

        if (isCritAttack.Equals(true) && skinId.Equals(8))
        {
            ft.DamageTextOn("0", originObj.transform.position, false, true);

            base.ReduceHp((long)(maxHp.value*0.5f), pDir, pVelocity, pDelay);
        }
        else
            base.ReduceHp(pVal, pDir, pVelocity, pDelay);

        //30 이하일 때 적색 프레임 on
        if (curHp.value <= maxHp.value * 0.3f && dp.img_lowHp.activeSelf.Equals(false))        
            dp.img_lowHp.SetActive(true);

        ft.DamageTextOn(pVal.ToString(), originObj.transform.position,false, true);

        //죽으면 메인메뉴로
        if (curHp.value.Equals(0))
            StartCoroutine(Dead_cor());
    }

    // 반사 타일에서 데미지 줄 때 호출
    public void ReduceHp(long pVal, Vector3 pDir, bool pIsReflectTile = false)
    {
        // 플레이어 대쉬 행동시 회피 !!!!! 임시로 Miss 대신 0 출력
        if (status == CHARACTERSTATUS.DASH)
        {
            ft.DamageTextOn("0", originObj.transform.position, false, true);
            return;
        }

        // 크리티컬 공격 중, 
        if (isCritAttack.Equals(true) && pIsReflectTile.Equals(false))
        {
            ft.DamageTextOn("0", originObj.transform.position, false, true);
            return;
        }

        base.ReduceHp(pVal, pDir);

        //30 이하일 때 적색 프레임 on
        if (curHp.value <= maxHp.value * 0.3f && dp.img_lowHp.activeSelf.Equals(false))
            dp.img_lowHp.SetActive(true);

        ft.DamageTextOn(pVal.ToString(), originObj.transform.position, false, true);

        //죽으면 메인메뉴로
        if (curHp.value.Equals(0))
            StartCoroutine(Dead_cor());
    }

    public void GetHit()
    {
        SetStatus(CHARACTERSTATUS.NONE);
    }

    IEnumerator Dead_cor()
    {
        yield return new WaitForSeconds(1.0f);

        cUtil._sm.ChangeScene("Main");
    }

    public override void RestoreHp(long pVal, bool toFool = false)
    {
        base.RestoreHp(pVal, toFool);

        //현재 체력이 30% 이상일 때 적색 프레임 off
        if (curHp.value > maxHp.value * 0.3f && dp.img_lowHp.activeSelf.Equals(true))
            dp.img_lowHp.SetActive(false);        
    }

    public void ReduceDp(long pVal)
    {
        if (cUtil._sm._scene.Equals(SCENE.SKIN))
            return;

        curDp.value -= pVal;

        if (curDp.value < 0)
            curDp.value = 0;

        SetDp();
    }
    public void RestoreDp(long pVal)
    {
        curDp.value += pVal;
        if (curDp.value > maxDp.value)
            curDp.value = maxDp.value;

        SetDp();
    }

    public byte UseItem(byte pItemKind)
    {
        byte curAmount = 100;
        for (byte i = 0; i < inven.GetItemUse().Count; i++)
        {
            if (pItemKind.Equals((byte)inven.GetItemUse()[i].id))
            {
                curAmount = inven.GetItemUse()[i].UseItem();
                break;
            }
        }
        return curAmount;
    }

    public void StartSpeedUp(float pAmount, float pTime)
    {
        StartCoroutine(SpeedUp(pAmount, pTime));
    }

    IEnumerator SpeedUp(float pAmount, float pTime)
    {
        Debug.Log("스피드업 시작");
        maxMoveSpeed += pAmount;

        yield return new WaitForSeconds(pTime);

        Debug.Log("스피드업 끝");
        maxMoveSpeed -= pAmount;
    }

    public void RootRocks(long pVal)
    {
        if(this.tag != "player_skin")
        {
            inven.GetRock().value += pVal;
            dp.UpdateValue();
        }
    }

    public void SetPoison(long pDamagePerTime, float pPoisonTime)
    {
        if (isPoision)
            return;

        StartCoroutine(Poison_cor(pDamagePerTime, pPoisonTime));
    }

    IEnumerator Poison_cor(long pDamagePerTime, float pPoisonTime)
    {
        Debug.Log("중독됨");
        isPoision = true;
        float timer = 0;

        while(true)
        {
            timer += 1.0f; // poisionTick == 1.0f;
            if (timer >= pPoisonTime)
                break;

            ReduceHp(pDamagePerTime,Vector3.zero);

            yield return poisonTick;
        }

        isPoision = false;
    }
    
    IEnumerator ComboCount_cor()
    {
        yield return null;
        
        while(true)
        {
            yield return new WaitForFixedUpdate();

            comboTimer += Time.deltaTime;

            if(comboTimer > comboTime)
            {
                comboTimer = 0;
                comboCount = 0;

                break;
            }

        }
    }
}
