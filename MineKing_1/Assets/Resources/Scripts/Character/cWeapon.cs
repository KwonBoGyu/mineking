﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cWeapon : MonoBehaviour
{
    public List<GameObject> l_targetMonsters;

    public cProperty damage;
    public cPlayer scr_player;
    public bool isDoubleAttackBox;


    private void Start()
    {
        l_targetMonsters = new List<GameObject>();
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Enemy"))
        {
            Debug.Log(collision.name + "insert");

            //리스트에 집어 넣는다.
            if (l_targetMonsters.Contains(collision.gameObject).Equals(false))
                l_targetMonsters.Add(collision.gameObject);
        }

        //if (collision.gameObject.tag.Equals("Shield"))
        //{
        //    collision.gameObject.GetComponent<cShield>().ReduceShieldHp(damage.value);
        //    Debug.Log("쉴드 공격함");
        //}
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag.Equals("Enemy"))
        {
            //리스트에서 뺀다
            if (l_targetMonsters.Contains(collision.gameObject).Equals(true))
            {
                l_targetMonsters.Remove(collision.gameObject);
                Debug.Log(collision.name + "removed");
            }
        }
    }
}
