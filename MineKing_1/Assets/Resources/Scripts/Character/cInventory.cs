﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cInventory : MonoBehaviour
{
    //장비 아이템
    private List<cItem_equip> l_itemEquip;
    public List<cItem_equip> GetItemEquip() { return l_itemEquip; }
    //소비 아이템
    private List<cItem_use> l_itemUse;
    public List<cItem_use> GetItemUse() { return l_itemUse; }
    //기타 아이템
    private List<cItem_etc> l_itemEtc;
    public List<cItem_etc> GetItemEtc() { return l_itemEtc; }
    //캐릭터 아이템
    private List<cItem_character> l_itemChar;
    public List<cItem_character> GetItemChar() { return l_itemChar; }
    // 재화
    private cGold money;
    public cGold GetMoney() { return money; }
    private cRock rock;
    public cRock GetRock() { return rock; }
    private cDia dia;
    public cDia GetDia() { return dia; }
    private List<cJewerly> jewerly;
    public List<cJewerly> GetJewerly() { return jewerly; }
    private List<cSoul> soul;
    public List<cSoul> GetSoul() { return soul; }

    //초기화
    public void Init()
    {
        l_itemEquip = new List<cItem_equip>();
        l_itemUse = new List<cItem_use>();
        l_itemEtc = new List<cItem_etc>();
        l_itemChar = new List<cItem_character>();

        money = new cGold();
        rock = new cRock();
        dia = new cDia();
        jewerly = new List<cJewerly>();
        for (byte i = 0; i < cUtil.AMOUNT_JEWERLY; i++)
            jewerly.Add(new cJewerly());
        soul = new List<cSoul>();
        for (byte i = 0; i < cUtil.AMOUNT_SOUL; i++)
            soul.Add(new cSoul());
    }

    //아이템 추가 (종류, ID, 수량). 순서 : 장비, 소비, 기타, 캐릭터
    public void AddItem(byte pKind, byte pId, byte pAmount = 1)
    {
        switch(pKind)
        {
            //////////////////////장비
            case 0:
                for (byte i = 0; i < l_itemEquip.Count; i++)
                {
                    //아이템이 존재한다면 리턴
                    if (l_itemEquip[i].id.Equals(pId))                    
                        return;                                        
                }

                //아이템이 존재하지 않을 때는 추가
                cItem pEquip;
                pEquip = new cItem_equip("", "", new cProperty("asdf"), pId);
                l_itemEquip.Add((cItem_equip)pEquip);

                break;

            ////////////////////////소비
            case 1:
                //아이템이 존재 할 때
                for (byte i = 0; i < l_itemUse.Count; i++)
                {
                    if (l_itemUse[i].id.Equals(pId))
                    {
                        l_itemUse[i].amount += pAmount;                        
                        return;
                    }
                }

                //아이템이 존재하지 않을 때는 추가
                cItem pUse;
                citemTable.GetItemInfo(out pUse, pId);
                l_itemUse.Add((cItem_use)pUse);
                if(pAmount > 1)
                    AddItem(1, pId, (byte)(pAmount - 1));

                break;

            ///////////////////////기타
            case 2:
                //아이템이 존재 할 때
                for (byte i = 0; i < l_itemEtc.Count; i++)
                {
                    if (l_itemEtc[i].id.Equals(pId))
                    {
                        //수량이 추가되지 않는 아이템
                        if (l_itemEtc[i]._name.Contains("BossKey"))
                            return;

                        //수량이 추가될 수 있는 아이템
                        l_itemEtc[i].amount += pAmount;
                        return;
                    }
                }

                //아이템이 존재하지 않을 때는 추가
                cItem pEtc;
                citemTable.GetItemInfo(out pEtc, pId);
                l_itemEtc.Add((cItem_etc)pEtc);
                if (pAmount > 1)
                    AddItem(2, pId, (byte)(pAmount - 1));

                break;

            //////////////////////캐릭터
            case 3:
                for (byte i = 0; i < l_itemChar.Count; i++)
                {
                    //아이템이 존재한다면 리턴
                    if (l_itemChar[i].id.Equals(pId))
                        return;
                }

                //아이템이 존재하지 않을 때는 추가
                cItem pChar;
                pChar = new cItem_character("", "", new cProperty("asdf"), pId);
                l_itemChar.Add((cItem_character)pChar);

                break;
        }
    }

    //아이템 삭제 (종류, ID, 수량). 순서 : 장비, 소비, 기타, 캐릭터
    public void RemoveItem(byte pKind, byte pId, byte pAmount = 1)
    {
        switch (pKind)
        {
            //////////////////////장비
            case 0:
                //아이템이 존재 할 때
                for (byte i = 0; i < l_itemEquip.Count; i++)
                {
                    if (l_itemEquip[i].id.Equals(pId))
                    {
                        l_itemEquip.RemoveAt(i);
                        return;
                    }
                }

                break;

            ////////////////////////소비
            case 1:
                //아이템이 존재 할 때
                for (byte i = 0; i < l_itemUse.Count; i++)
                {
                    if (l_itemUse[i].id.Equals(pId))
                    {
                        if (l_itemUse[i].amount <= pAmount)
                        {
                            l_itemUse.RemoveAt(i);
                        }
                        else
                            l_itemUse[i].amount -= pAmount;
                        return;
                    }
                }

                break;

            ///////////////////////기타
            case 2:
                //아이템이 존재 할 때
                for (byte i = 0; i < l_itemEtc.Count; i++)
                {
                    if (l_itemEtc[i].id.Equals(pId))
                    {
                        if (l_itemEtc[i].amount <= pAmount)
                            l_itemEtc.RemoveAt(i);
                        else
                            l_itemEtc[i].amount -= pAmount;
                        return;
                    }
                }

                break;

            //////////////////////캐릭터
            case 3:
                //아이템이 존재 할 때
                for (byte i = 0; i < l_itemChar.Count; i++)
                {
                    if (l_itemChar[i].id.Equals(pId))
                    {
                        l_itemChar.RemoveAt(i);
                        return;
                    }
                }

                break;
        }
    }

    //아이템 보유 여부
    public bool IsItemExist(byte pKind, byte pId)
    {
        bool r = false;

        switch(pKind)
        {
            //장비
            case 0:
                for(byte i = 0; i < l_itemEquip.Count; i++)
                {
                    if (l_itemEquip[i].id.Equals(pId))
                    {
                        r = true;
                        break;
                    }
                }

                break;

            //소비
            case 1:
                for (byte i = 0; i < l_itemUse.Count; i++)
                {
                    if (l_itemUse[i].id.Equals(pId))
                    {
                        r = true;
                        break;
                    }
                }

                break;

            //기타
            case 2:
                for (byte i = 0; i < l_itemEtc.Count; i++)
                {
                    if (l_itemEtc[i].id.Equals(pId))
                    {
                        r = true;
                        break;
                    }
                }

                break;

            //캐릭터
            case 3:
                for (byte i = 0; i < l_itemChar.Count; i++)
                {
                    if (l_itemChar[i].id.Equals(pId))
                    {
                        r = true;
                        break;
                    }
                }

                break;
        }

        return r;
    }
}
