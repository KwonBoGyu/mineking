﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cHeadLight : cLight
{
    public Transform HeadLight;

    private void Update()
    {
        if (HeadLight == null)
            return;

        if (isInit.Equals(true))
        {
            this.transform.position = new Vector3(HeadLight.position.x, HeadLight.position.y, HeadLight.position.z);
        }
    }
}
