﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class cPlayerInfo
{
    public cInventory inventory; //인벤토리
    public string nickName; // 닉네임
    public bool[] flaged;   //스테이지별 점령지
    public bool[] bossDone; //보스 스테이지 클리어 여부    
    public byte[] skillLevel; //0 : 대쉬, 1 : 시야증가, 2 : 차지강화, 3 : 점프강화
    public short[] quickSlotItemNum; //퀵슬롯 아이템 종류
    public byte curWeaponId; //현재 무기 아이디
    public byte curClothId; //현재 캐릭터 아이디
    public byte maxStageNum; //최대 클리어 스테이지
    public cProperty[] maxJewerlySellPrice; //보석 최고 판매가
    public cProperty[] prevStorePrice;
    public cProperty[] avgStorePrice;
    public cProperty[] curStorePrice;
    public cAxe weapon; //무기 정보

    //저장용 변수
    public cGold money;
    public cRock rock;
    public cDia dia;
    public cJewerly[] jewerly;
    public cSoul[] soul;
    public byte[] myWeaponsId; //무기 보유 여부
    public byte[] myClothesId; //캐릭터 보유 여부
    public cItem_use[] myUseId; //소비 보유 여부
    public cItem_etc[] myEtcId; //기타 보유 여부
    //

    #region 생성자
    public cPlayerInfo(string pNickName, cAxe pAxe, byte[] pSkillLevel, bool[] pFlaged, bool[] pBossDone,
        short[] pQuickSlotItemNum,cInventory pInventory, 
        cGold pMoney, cRock pRock, cDia pDia, cJewerly[] pJewerly, cSoul[] pSoul,
        cProperty[] pPrevStorePrice, cProperty[] pAvStorePrice, cProperty[] pCurStorePrice, cProperty[] pMaxJewerlySellPrice,
        byte[] pMyWeaponsId, byte pCurWeaponId, byte[] pMyClothesId, byte pCurClothId, byte pMaxStageNum,
        cItem_use[] pMyUseId = null, cItem_etc[] pMyEtcId = null)
    {
        //인벤토리
        inventory = pInventory;
        inventory.Init();

        //닉네임
        nickName = pNickName;
        
        //점령 여부
        flaged = new bool[cUtil._sm.maxSceneNum.dungeonNormal];
        for (byte i = 0; i < flaged.Length; i++)
            flaged[i] = pFlaged[i];

        //보스 클리어 여부
        bossDone = new bool[cUtil._sm.maxSceneNum.dungeonBoss];
        for (byte i = 0; i < bossDone.Length; i++)
            bossDone[i] = pBossDone[i];

        //스킬 레벨
        skillLevel = new byte[cUtil.AMOUNT_SKILL];
        for (byte i = 0; i < skillLevel.Length; i++)
            skillLevel[i] = pSkillLevel[i];

        //퀵슬롯
        quickSlotItemNum = new short[cUtil.AMOUNT_QUICKSLOT];
        for (byte i = 0; i < quickSlotItemNum.Length; i++)
            quickSlotItemNum[i] = pQuickSlotItemNum[i];

        //현재 무기 
        curWeaponId = pCurWeaponId;

        //현재 캐릭터
        curClothId = pCurClothId;

        //최대 클리어 스테이지
        maxStageNum = pMaxStageNum;

        //보석상점 values
        prevStorePrice = new cJewerly[cUtil.AMOUNT_JEWERLY];
        avgStorePrice = new cJewerly[cUtil.AMOUNT_JEWERLY];
        curStorePrice = new cJewerly[cUtil.AMOUNT_JEWERLY];
        maxJewerlySellPrice = new cProperty[cUtil.AMOUNT_JEWERLY];
        for (byte i = 0; i < cUtil.AMOUNT_JEWERLY; i++)
        {
            prevStorePrice[i] = new cJewerly(pPrevStorePrice[i]._name, pPrevStorePrice[i].value);
            avgStorePrice[i] = new cJewerly(pAvStorePrice[i]._name, pAvStorePrice[i].value);
            curStorePrice[i] = new cJewerly(pCurStorePrice[i]._name, pCurStorePrice[i].value);
            maxJewerlySellPrice[i] = new cProperty(pMaxJewerlySellPrice[i]._name, pMaxJewerlySellPrice[i].value);
        }
        
        //무기 정보
        weapon = new cAxe(pAxe);

        //재화
        money = new cGold();
        rock = new cRock();
        dia = new cDia();
        jewerly = new cJewerly[pJewerly.Length];            
        soul = new cSoul[pSoul.Length];
            
        inventory.GetMoney().value = pMoney.value;
        inventory.GetRock().value = pRock.value;
        inventory.GetDia().value = pDia.value;
        for (byte i = 0; i < pJewerly.Length; i++)
        {
            inventory.GetJewerly()[i].value = pJewerly[i].value;
            jewerly[i] = new cJewerly();
        }
        for (byte i = 0; i < pSoul.Length; i++)
        {
            inventory.GetSoul()[i].value = pSoul[i].value;
            soul[i] = new cSoul();
        }
                
        //무기 보유 여부
        for (byte i = 0; i < pMyWeaponsId.Length; i++)
        {
            inventory.AddItem(0, i);
        }

        //캐릭터 보유 여부
        for (byte i = 0; i < pMyClothesId.Length; i++)
        {
            inventory.AddItem(3, i);
        }

        //소비 보유 여뷰
        if(pMyUseId != null)
        {
            myUseId = new cItem_use[pMyUseId.Length];
            for (short i = 0; i < pMyUseId.Length; i++)
            {
                myUseId[i] = new cItem_use(pMyUseId[i]);
                inventory.AddItem(1, pMyUseId[i].id, pMyUseId[i].amount);
            }
        }

        //기타 보유 여부
        if(pMyEtcId != null)
        {
            myEtcId = new cItem_etc[pMyEtcId.Length];
            for (short i = 0; i < pMyEtcId.Length; i++)
            {
                myEtcId[i] = new cItem_etc(pMyEtcId[i]);
                inventory.AddItem(2, pMyEtcId[i].id, pMyEtcId[i].amount);
            }
        }
    }

    public cPlayerInfo(cPlayerInfo pPi, cInventory pInventory)
    {
        //인벤토리
        inventory = pInventory;
        inventory.Init();

        //닉네임
        nickName = pPi.nickName;

        //점령 여부
        flaged = new bool[cUtil._sm.maxSceneNum.dungeonNormal];
        for (byte i = 0; i < flaged.Length; i++)
            flaged[i] = pPi.flaged[i];

        //보스 클리어 여부
        bossDone = new bool[cUtil._sm.maxSceneNum.dungeonBoss];
        for (byte i = 0; i < bossDone.Length; i++)
            bossDone[i] = pPi.bossDone[i];

        //스킬 레벨
        skillLevel = new byte[cUtil.AMOUNT_SKILL];
        for (byte i = 0; i < skillLevel.Length; i++)
            skillLevel[i] = pPi.skillLevel[i];

        //퀵슬롯
        quickSlotItemNum = new short[cUtil.AMOUNT_QUICKSLOT];
        for (byte i = 0; i < quickSlotItemNum.Length; i++)
            quickSlotItemNum[i] = pPi.quickSlotItemNum[i];

        //현재 무기
        curWeaponId = pPi.curWeaponId;

        //현재 캐릭터
        curClothId = pPi.curClothId;

        //최대 클리어 스테이지
        maxStageNum = pPi.maxStageNum;

        //보석상점 values
        prevStorePrice = new cProperty[cUtil.AMOUNT_JEWERLY];
        avgStorePrice = new cProperty[cUtil.AMOUNT_JEWERLY];
        curStorePrice = new cProperty[cUtil.AMOUNT_JEWERLY];
        maxJewerlySellPrice = new cProperty[cUtil.AMOUNT_JEWERLY];
        for (byte i = 0; i < cUtil.AMOUNT_JEWERLY; i++)
        {
            prevStorePrice[i] = new cProperty(pPi.prevStorePrice[i]._name, pPi.prevStorePrice[i].value);
            avgStorePrice[i] = new cProperty(pPi.avgStorePrice[i]._name, pPi.avgStorePrice[i].value);
            curStorePrice[i] = new cProperty(pPi.curStorePrice[i]._name, pPi.curStorePrice[i].value);
            maxJewerlySellPrice[i] = new cProperty(pPi.maxJewerlySellPrice[i]._name, pPi.maxJewerlySellPrice[i].value);
        }

        //무기 정보
        weapon = new cAxe(pPi.weapon);

        //재화
        money = new cGold();
        rock = new cRock();
        dia = new cDia();
        jewerly = new cJewerly[pPi.jewerly.Length];
        soul = new cSoul[pPi.soul.Length];

        inventory.GetMoney().value = pPi.money.value;
        inventory.GetRock().value = pPi.rock.value;
        inventory.GetDia().value = pPi.dia.value;
        for (byte i = 0; i < pPi.jewerly.Length; i++)
        {
            inventory.GetJewerly()[i].value = pPi.jewerly[i].value;
            jewerly[i] = new cJewerly();
        }
        for (byte i = 0; i < pPi.soul.Length; i++)
        {
            inventory.GetSoul()[i].value = pPi.soul[i].value;
            soul[i] = new cSoul();
        }

        //무기 보유 여부
        for (byte i = 0; i < pPi.myWeaponsId.Length; i++)
        {
            if (pPi.myWeaponsId[i].Equals(true))
                inventory.AddItem(0, i, 1);
            else
                inventory.RemoveItem(0, i, 1);
        }

        //캐릭터 보유 여부
        for (byte i = 0; i < pPi.myClothesId.Length; i++)
        {
            if (pPi.myClothesId[i].Equals(true))
                inventory.AddItem(3, i);
            else
                inventory.RemoveItem(3, i);
        }

        //소비 보유 여뷰
        if(pPi.myUseId != null)
        {
            myUseId = new cItem_use[pPi.myUseId.Length];
            for (short i = 0; i < pPi.myUseId.Length; i++)
            {
                myUseId[i] = new cItem_use(pPi.myUseId[i]);
                inventory.AddItem(1, pPi.myUseId[i].id, pPi.myUseId[i].amount);
            }
        }

        //기타 보유 여부
        if(pPi.myEtcId != null)
        {
            myEtcId = new cItem_etc[pPi.myEtcId.Length];
            for (short i = 0; i < pPi.myEtcId.Length; i++)
            {
                myEtcId[i] = new cItem_etc(pPi.myEtcId[i]);
                inventory.AddItem(2, pPi.myEtcId[i].id, pPi.myEtcId[i].amount);
            }
        } 
    }
    #endregion

    //데이터 동기화
    public void SyncData()
    {
        //재화 동기화
        money.value = inventory.GetMoney().value;
        rock.value = inventory.GetRock().value;
        dia.value = inventory.GetDia().value;
        for (byte i = 0; i < jewerly.Length; i++)
            jewerly[i].value = inventory.GetJewerly()[i].value;
        for (byte i = 0; i < soul.Length; i++)
            soul[i].value = inventory.GetSoul()[i].value;

        //무기 보유 여부 동기화
        myWeaponsId = new byte[inventory.GetItemEquip().Count];
        for (byte i = 0; i < inventory.GetItemEquip().Count; i++)        
            myWeaponsId[i] = inventory.GetItemEquip()[i].id;        

        //소비 보유 여부 동기화
        myUseId = new cItem_use[inventory.GetItemUse().Count];
        for (byte i = 0; i < inventory.GetItemUse().Count; i++)        
            myUseId[i] = new cItem_use(inventory.GetItemUse()[i]);

        //기타 보유 여부 동기화
        myEtcId = new cItem_etc[inventory.GetItemEtc().Count];
        for (byte i = 0; i < inventory.GetItemEtc().Count; i++)
            myEtcId[i] = new cItem_etc(inventory.GetItemEtc()[i]);

        //캐릭터 보유 여부 동기화
        myClothesId = new byte[inventory.GetItemChar().Count];
        for (byte i = 0; i < inventory.GetItemChar().Count; i++)
            myClothesId[i] = inventory.GetItemChar()[i].id;
    }
}
