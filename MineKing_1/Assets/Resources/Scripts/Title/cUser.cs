﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cUser : MonoBehaviour
{
    #region 유저 정보 접근자
    //유저 정보
    private cPlayerInfo _playerInfo;
        ////인벤토리
    public cInventory GetInventory() { return _playerInfo.inventory; }
        ////닉네임
    public string GetNickName() { return _playerInfo.nickName; }
    public void SetNickName(string pName) { _playerInfo.nickName = pName; }    
        ////점령지
    public bool GetFlaged(byte pStage) { return _playerInfo.flaged[pStage]; }
    public void SetFlaged(byte pStage, bool pDone) { _playerInfo.flaged[pStage] = pDone; }
        ////보스 클리어
    public bool GetBossDone(byte pStage) { return _playerInfo.bossDone[pStage]; }
    public void SetBossDone(byte pStage, bool pDone) { _playerInfo.bossDone[pStage] = pDone; }
        ////스킬 레벨
    public byte GetSkillLevel(byte pSkillNum) { return _playerInfo.skillLevel[pSkillNum]; }//0 : 대쉬, 1 : 시야증가, 2 : 차지강화, 3 : 점프강화
    public void PlusSkillLevel(byte pSkillNum, byte pAmount)//0 : 대쉬, 1 : 시야증가, 2 : 차지강화, 3 : 점프강화
    {        
        _playerInfo.skillLevel[pSkillNum] += pAmount;

        if (_playerInfo.skillLevel[pSkillNum] > cUtil.AMOUNT_SKILLLEVEL)
            _playerInfo.skillLevel[pSkillNum] = cUtil.AMOUNT_SKILLLEVEL;
    }
    public void MinusSkillLevel(byte pSkillNum, byte pAmount)//0 : 대쉬, 1 : 시야증가, 2 : 차지강화, 3 : 점프강화
    {
        //감소량이 현재값보다 크다면 스킬레벨을 0으로
        if (pAmount > _playerInfo.skillLevel[pSkillNum])
            _playerInfo.skillLevel[pSkillNum] = 0;
        //아니라면 바로 감소시킨다
        else
            _playerInfo.skillLevel[pSkillNum] -= pAmount;
    }
        ////퀵슬롯 아이템
    public short GetQuickslotItem(byte pNum) { return _playerInfo.quickSlotItemNum[pNum]; }
    public void RemoveQuickslotItem(byte pNum) { _playerInfo.quickSlotItemNum[pNum] = -1; }
    public void SetQuickslotItem(byte pNum, short pId)
    {
        //이미 같은 아이템이라면 리턴
        if (_playerInfo.quickSlotItemNum[pNum].Equals(pId))
            return;

        //해당 자리에 이미 등록되어 있는 아이템이 있다면..
        if(_playerInfo.quickSlotItemNum[pNum] != -1)
        {
            //넣으려는 아이템이 이미 퀵슬롯에 등록된 상태라면... 둘이 위치 바꾸고 리턴
            for (byte i = 0; i < cUtil.AMOUNT_QUICKSLOT; i++)
            {
                if (_playerInfo.quickSlotItemNum[i].Equals(pId))
                {
                    _playerInfo.quickSlotItemNum[i] = _playerInfo.quickSlotItemNum[pNum];
                    _playerInfo.quickSlotItemNum[pNum] = pId;

                    return;
                }
            }
        }
        //해당 자리에 이미 등록되어 있는 아이템이 없다면..
        else
        {
            //넣으려는 아이템이 이미 퀵슬롯에 등록된 상태라면... 이전 퀵슬롯 삭제
            for (byte i = 0; i < cUtil.AMOUNT_QUICKSLOT; i++)
            {
                if (_playerInfo.quickSlotItemNum[i].Equals(pId))
                {
                    RemoveQuickslotItem(i);
                    _playerInfo.quickSlotItemNum[pNum] = pId;

                    return;
                }
            }
        }
                
        _playerInfo.quickSlotItemNum[pNum] = pId;
    }

        ////현재 무기 id
    public byte GetCurWeaponId() { return _playerInfo.curWeaponId; }
    public void SetCurWeaponId(byte pId)
    {
        for(byte i = 0; i < GetInventory().GetItemEquip().Count; i++)
        {
            //인벤토리에 바꾸려는 무기가 있으면 교체
            if(GetInventory().GetItemEquip()[i].id.Equals(pId))
            {
                _playerInfo.curWeaponId = pId;
                return;
            }
        }
    }
        ////현재 캐릭터 id
    public byte GetCurClothId() { return _playerInfo.curClothId; }
    public void SetCurClothId(byte pId)
    {
        for (byte i = 0; i < GetInventory().GetItemChar().Count; i++)
        {
            //인벤토리에 바꾸려는 캐릭터가 있으면 교체
            if (GetInventory().GetItemChar()[i].id.Equals(pId))
            {
                _playerInfo.curClothId = pId;
                return;
            }
        }
    }
        ////최대 클리어 스테이지
    public byte GetMaxClearedStage() { return _playerInfo.maxStageNum; }
    public void PlusMaxClearedStage(bool pForce = false)
    {
        //강제로 늘리기
        if (pForce.Equals(true))
            _playerInfo.maxStageNum += 1;
        else
        {
            if (GetMaxClearedStage() < (byte)cUtil._sm._scene - 2)
                _playerInfo.maxStageNum += 1;
        }
    }
        ////보석 판매가
    public long GetMaxJewerlySellPrice(byte pChar) { return _playerInfo.maxJewerlySellPrice[pChar].value; }
    public void SetMaxJewerlySellPrice(byte pChar, long pValue)
    {
        if (pValue > _playerInfo.maxJewerlySellPrice[pChar].value)
            _playerInfo.maxJewerlySellPrice[pChar].value = pValue;            
    }
        ////보석상점 이전 가격
    public long GetPrevStorePrice(byte pChar) { return _playerInfo.prevStorePrice[pChar].value; }
    public void SetPrevStorePrice(byte pChar, long pValue) { _playerInfo.prevStorePrice[pChar].value = pValue; }
        ////보석상점 평균 가격
    public long GetAvgStorePrice(byte pChar) { return _playerInfo.avgStorePrice[pChar].value; }
    public void SetAvgStorePrice(byte pChar, long pValue) { _playerInfo.avgStorePrice[pChar].value = pValue; }
        ////보석상점 현재 가격
    public long GetCurStorePrice(byte pChar) { return _playerInfo.curStorePrice[pChar].value; }
    public void SetCurStorePrice(byte pChar, long pValue) { _playerInfo.curStorePrice[pChar].value = pValue; }
        ////무기 정보
    public cAxe GetWeaponInfo() { return _playerInfo.weapon; }
    #endregion

    //데이터 로딩이 되었느냐?
    private bool isInited;
    public bool GetIsInited() { return isInited; }

    //저장 문자열
    private string saveString;

    //스킨 이미지(수동 캐싱)
    public Sprite[] WeaponImages;
    public Sprite[] WeaponImages_dungeonEnter;
    public Sprite[] WeaponEquipImages;
    public Sprite[] ClothImages;
    
    //초기화
    public void Init()
    {       
        if (isInited.Equals(true))
            return;

        cUtil.AMOUNT_WEAPON = (byte)WeaponImages.Length;
        cUtil.AMOUNT_CLOTH = (byte)ClothImages.Length;

        saveString = "save2";
        LoadUserData();
        isInited = true;
    }

    #region 데이터 저장&불러오기
    public void SaveUserData()
    {
        _playerInfo.SyncData();
        cSaveLoad.SaveData(saveString, _playerInfo);
        Debug.Log("SAVED");
    }

    public void LoadUserData()
    {
        bool _fileExist = true;

        string fileDir = Application.persistentDataPath + "/Saves";
        System.IO.DirectoryInfo _dr = new System.IO.DirectoryInfo(fileDir);

        if (_dr.Exists == false)
            _dr.Create();

        cSaveLoad.LoadData<cPlayerInfo>(saveString, ref _fileExist);

        /////////////////////////////////////////////////////////////////////////////////////////생성된 데이터가 없을 때
        if (_fileExist == false)
        {
            //재화 초기화
            cGold money = new cGold();
            cRock rock = new cRock();
            cDia dia = new cDia();
            cJewerly[] jewerly = new cJewerly[cUtil.AMOUNT_JEWERLY];
            for (byte i = 0; i < jewerly.Length; i++)
                jewerly[i] = new cJewerly();
            cSoul[] soul = new cSoul[cUtil.AMOUNT_SOUL];
            for (byte i = 0; i < soul.Length; i++)
                soul[i] = new cSoul();

            //무기 정보 초기화
            cProperty axeLevel = new cProperty("AxeLevel", 1);
            cAxe tAxe = new cAxe(cWeaponTable.GetAxeInfo(axeLevel));

            //스킬레벨 초기화
            byte[] skillLevel = new byte[cUtil.AMOUNT_SKILL];
            for (byte i = 0; i < skillLevel.Length; i++)
                skillLevel[i] = 1;

            //점령지 점령 여부 초기화
            bool[] flaged = new bool[cUtil._sm.maxSceneNum.dungeonNormal];
            for (byte i = 0; i < flaged.Length; i++)
                flaged[i] = false;

            //보스 점령 여부 초기화
            bool[] bossDone = new bool[cUtil._sm.maxSceneNum.dungeonBoss];
            for (byte i = 0; i < bossDone.Length; i++)
                bossDone[i] = false;

            //퀵슬롯 아이템 넘버 초기화
            short[] quickSlotItemNum = new short[cUtil.AMOUNT_QUICKSLOT];
            for (byte i = 0; i < quickSlotItemNum.Length; i++)
                quickSlotItemNum[i] = -1;

            //보석상점 가격 초기화
            cProperty[] pPrevStorePrice = new cProperty[cUtil.AMOUNT_JEWERLY];
            cProperty[] pAvStorePrice = new cProperty[cUtil.AMOUNT_JEWERLY];
            cProperty[] pCurStorePrice = new cProperty[cUtil.AMOUNT_JEWERLY];
            cProperty[] pMaxJewerlySellPrice = new cProperty[cUtil.AMOUNT_JEWERLY];
            pPrevStorePrice[0] = new cProperty("보석", 5000);
            pAvStorePrice[0] = new cProperty("보석", 5000);
            pCurStorePrice[0] = new cProperty("보석", 5000);
            pPrevStorePrice[1] = new cProperty("보석", 50000);
            pAvStorePrice[1] = new cProperty("보석", 50000);
            pCurStorePrice[1] = new cProperty("보석", 50000);
            pPrevStorePrice[2] = new cProperty("보석", 500000);
            pAvStorePrice[2] = new cProperty("보석", 500000);
            pCurStorePrice[2] = new cProperty("보석", 500000);
            pPrevStorePrice[3] = new cProperty("보석", 20000000);
            pAvStorePrice[3] = new cProperty("보석", 20000000);
            pCurStorePrice[3] = new cProperty("보석", 20000000);
            pPrevStorePrice[4] = new cProperty("보석", 100000000);
            pAvStorePrice[4] = new cProperty("보석", 100000000);
            pCurStorePrice[4] = new cProperty("보석", 100000000);

            for (byte i = 0; i < cUtil.AMOUNT_JEWERLY; i++)
                pMaxJewerlySellPrice[i] = new cProperty("adsf", 0);

            //무기 보유 여부 초기화
                //테스트 버전
            byte[] pMyWeaponsId = new byte[WeaponImages.Length];
            for (byte i = 0; i < pMyWeaponsId.Length; i++)
                pMyWeaponsId[i] = i;
                //출시 버전
            //byte[] pMyWeaponsId = new byte[1];
            //for (byte i = 0; i < pMyWeaponsId.Length; i++)
            //    pMyWeaponsId[i] = 0;

            //캐릭터 초기화
                //테스트 버전
            byte[] pMyClothesId = new byte[ClothImages.Length];
            for (byte i = 0; i < pMyClothesId.Length; i++)
                pMyClothesId[i] = i;    
                //출시 버전
            //byte[] pMyClothesId = new byte[1];
            //for (byte i = 0; i < pMyClothesId.Length; i++)
            //    pMyClothesId[i] = 0;

            //현재 무기
            byte pCurWeaponId = 0;

            //현재 캐릭터
            byte pCurClothId = 0;
            
            //Max 스테이지 클리어
            byte pMaxStageNum = 0;

            //유저 정보 초기화
            _playerInfo = new cPlayerInfo(
                "이름입니다.", 
                tAxe, 
                skillLevel, 
                flaged, 
                bossDone, 
                quickSlotItemNum, 
                this.GetComponent<cInventory>(), 
                money, 
                rock, 
                dia, 
                jewerly, 
                soul, 
                pPrevStorePrice,
                pAvStorePrice, 
                pCurStorePrice, 
                pMaxJewerlySellPrice,
                pMyWeaponsId, 
                pCurWeaponId, 
                pMyClothesId,
                pCurClothId, 
                pMaxStageNum);

            SaveUserData();
            Debug.Log("Initialized Done - CreatedInitData");
        }
        //기존 데이터가 있으면..
        else
        {
            _playerInfo = new cPlayerInfo(cSaveLoad.LoadData<cPlayerInfo>(saveString), this.GetComponent<cInventory>());
            Debug.Log("Initialized Done - Data Exists");
        }        
    }
    #endregion
}
