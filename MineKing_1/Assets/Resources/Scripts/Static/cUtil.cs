﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class cUtil
{
    //매니져
    public static cSceneManager _sm;
    public static cTileMng _tileMng;
    public static cSoundMng _soundMng;
    public static cTimeMng _timeMng;
    public static SpriteMaskScript _lightMng;    

    //유저 정보
    public static cUser _user;
    public static cPlayer _player; //조작용 플레이어    

    //전역변수
    public const float pi = 3.141592f;
    public const byte AMOUNT_JEWERLY = 5;
    public const byte AMOUNT_SOUL = 4;
    public const byte AMOUNT_SKILL = 4;
    public const byte AMOUNT_SKILLLEVEL = 5;
    public const byte AMOUNT_QUICKSLOT = 4;
    public const byte AMOUNT_ITEM = 3;
    public static byte AMOUNT_WEAPON;
    public static byte AMOUNT_CLOTH;
    

    public static void SetInstanceInit(GameObject pObj, string pName = null, GameObject pParent = null, 
        bool pExitButton = false, bool pOkayButton = false)
    {
        if (pParent != null)
            pObj.transform.SetParent(pParent.transform);

        if (pName != null)
            pObj.name = pName;

        pObj.transform.localPosition = new Vector3(0, 0, 0);
        pObj.transform.localScale = new Vector3(1, 1, 1);
    }

    public static string GetValueToString(long pValue)
    {
        string s_return = " ";

        if (pValue >= 1000000000000000000)
            s_return = string.Format("{0:F1}f", pValue * 0.000000000000000001f);
        else if (pValue >= 1000000000000000)
            s_return = string.Format("{0:F1}e", pValue * 0.000000000000001f);
        else if (pValue >= 1000000000000)
            s_return = string.Format("{0:F1}d", pValue * 0.000000000001f);
        else if (pValue >= 1000000000)
            s_return = string.Format("{0:F1}c", pValue * 0.000000001f);
        else if (pValue >= 1000000)
            s_return = string.Format("{0:F1}b", pValue * 0.000001f);
        else if (pValue >= 1000)
            s_return = string.Format("{0:F1}a", pValue * 0.001f);
        else
            s_return = pValue.ToString();

        return s_return;
    }
}
