﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct struct_SkinInfo
{
    public byte skinId;
    public string skinName;
    public string skinDesc;    //25자 내외
    public string skinSkillDesc;
}

public static class cSkinTable
{
    private static struct_SkinInfo skinInfo;

    public static struct_SkinInfo GetSkinInfo(byte pSkinId)
    {
        switch(pSkinId)
        {
            case 0:
                skinInfo.skinId = 0;
                skinInfo.skinName = "단련된 광부";
                skinInfo.skinDesc = "어릴적부터 광산에서 단련된 강인한 광부입니다.";
                skinInfo.skinSkillDesc = "없음";
                break;

            case 1:
                skinInfo.skinId = 1;
                skinInfo.skinName = "회춘한 광부";
                skinInfo.skinDesc = "무엇이 광부를 회춘하게 만들었을까요~?";
                skinInfo.skinSkillDesc = "없음";
                break;

            case 2:
                skinInfo.skinId = 2;
                skinInfo.skinName = "길뚫는 바이킹";
                skinInfo.skinDesc = "바닷길을 뚫다가 광산까지 도착해버린 바이킹입니다.";
                skinInfo.skinSkillDesc = "광폭화\n일정시간 쉬지않고 공격시 5초동안 모든 데미지가 증가합니다.";
                break;

            case 3:
                skinInfo.skinId = 3;
                skinInfo.skinName = "족장 코스프레";
                skinInfo.skinDesc = "\"내가 고블린 족장이 된다면 난이도가 너무 올라가겠는걸?\"";
                skinInfo.skinSkillDesc = "없음";
                break;

            case 4:
                skinInfo.skinId = 4;
                skinInfo.skinName = "네모네모 마민킹";
                skinInfo.skinDesc = "네모마믈메서 파견된 멜리트 괌부밉니다.";
                skinInfo.skinSkillDesc = "멊믐";
                break;

            case 5:
                skinInfo.skinId = 5;
                skinInfo.skinName = "잘못 자란 완두콩";
                skinInfo.skinDesc = "개발자가 완두콩에게 생명을 불어넣었지만 참담한 결과가 나왔습니다.";
                skinInfo.skinSkillDesc = "없음";
                break;

            case 6:
                skinInfo.skinId = 6;
                skinInfo.skinName = "가슴이 터질 것처럼";
                skinInfo.skinDesc = "이 눈사람은 무료로 (폭탄을) 해드립니다.";
                skinInfo.skinSkillDesc = "혹한기의 폭탄병\n일정 확률로 폭탄을 소모하지 않습니다.";
                break;

            case 7:
                skinInfo.skinId = 7;
                skinInfo.skinName = "마녀 곽정팔";
                skinInfo.skinDesc = "정팔이누나한테 맞고 싶어하는 몬스터들이 많다고 합니다.";
                skinInfo.skinSkillDesc = "영혼 수거\n일정 확률로 한번에 타일 파괴합니다.";
                break;

            case 8:
                skinInfo.skinId = 8;
                skinInfo.skinName = "바위늘보";
                skinInfo.skinDesc = "나무에서 삐끗한 늘보는 광산까지 떨어져버렸습니다.";
                skinInfo.skinSkillDesc = "선조의 기억\n점프를 한번 더 할 수 있습니다.";
                break;
        }

        return skinInfo;
    }
  

}
