﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cTimeMng : MonoBehaviour
{
    //상점 갱신 관련 변수
    private const float storeUpdateTime = 30;
    private float storeUpdateTimer;
    public cStore store;
    //

    private void FixedUpdate()
    {
        //상점 가격 변경
        {
            //시간 계속 돌린다.
            storeUpdateTimer += Time.deltaTime;

            //메인 화면일 경우에만 텍스트 변경
            if (cUtil._sm._scene.Equals(SCENE.MAIN))
                SetStoreClock(string.Format("{0:F0}s", storeUpdateTime - storeUpdateTimer));

            //가격 변경
            if (storeUpdateTimer > storeUpdateTime)
            {
                UpdateStorePrice();
                storeUpdateTimer = 0;
            }
        }
    }

    //상점 시간 이동 함수
    public void StoreTimeCapsule()
    {
        UpdateStorePrice_TimeCapsule();
        storeUpdateTimer = 0;
    }


    public void SetStoreClock(string pStr)
    {
        if (store == null)
            return;

        store.SetClock(pStr);
    }

    public void UpdateStorePrice()
    {
        // 실시간 가격 변동
        // y = ax ^ 4 + b
        // y : 가격, a: 기울기, x: 확률변수, b: 평균값
        float maxPrice;
        float lowPrice;
        float maxX = 0;
        float minX = 0;
        float a = 1;
        float randomNum;
        float totalPrice;


        for (byte i = 0; i < 5; i++)
        {
            lowPrice = cUtil._user.GetAvgStorePrice(i) -  (cUtil._user.GetAvgStorePrice(i) * 0.9f);
            maxPrice = cUtil._user.GetAvgStorePrice(i) + (cUtil._user.GetAvgStorePrice(i) * 0.9f);

            minX = Mathf.Pow((maxPrice - cUtil._user.GetAvgStorePrice(i)) / a, 0.25f) * -1;
            maxX = Mathf.Pow((maxPrice - cUtil._user.GetAvgStorePrice(i)) / a, 0.25f);
            randomNum = Random.Range(minX, maxX);

            totalPrice = Mathf.Pow(randomNum, 4) + cUtil._user.GetAvgStorePrice(i);

            cUtil._user.SetPrevStorePrice(i, cUtil._user.GetCurStorePrice(i));
            cUtil._user.SetCurStorePrice(i, (long)totalPrice);            
        }

        if (store != null)
        {
            store.UpdateValue();
            cUtil._soundMng.PlayAuctionBell();
        }
    }

    public void UpdateStorePrice_TimeCapsule()
    {
        // 실시간 가격 변동
        // y = ax ^ 4 + b
        // y : 가격, a: 기울기, x: 확률변수, b: 평균값
        float maxPrice;
        float lowPrice;
        float maxX = 0;
        float minX = 0;
        float a = 1;
        float randomNum;
        float totalPrice;


        for (byte i = 0; i < 5; i++)
        {
            lowPrice = cUtil._user.GetAvgStorePrice(i);
            maxPrice = cUtil._user.GetAvgStorePrice(i) + (cUtil._user.GetAvgStorePrice(i) * 0.9f);

            minX = Mathf.Pow((maxPrice - cUtil._user.GetAvgStorePrice(i)) / a, 0.25f) * -1;
            maxX = Mathf.Pow((maxPrice - cUtil._user.GetAvgStorePrice(i)) / a, 0.25f);
            randomNum = Random.Range(minX, maxX);

            totalPrice = Mathf.Pow(randomNum, 4) + cUtil._user.GetAvgStorePrice(i);

            cUtil._user.SetPrevStorePrice(i, cUtil._user.GetCurStorePrice(i));
            cUtil._user.SetCurStorePrice(i, (long)totalPrice);            
        }

        if (store != null)
        {
            store.UpdateValue();
            cUtil._soundMng.PlayAuctionBell();
        }
    }
}
