﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct struct_weaponInfo
{
    public byte weaponId;
    public string weaponName;
    public string weaponDesc;
}

public static class cWeaponTable
{
    private static struct_weaponInfo weaponInfo;

    //무기 설명 정보 가져오기
    public static struct_weaponInfo GetWeaponInfo(byte pWeaponId)
    {
        switch(pWeaponId)
        {
            case 0:
                weaponInfo.weaponId = 0;
                weaponInfo.weaponName = "유물 곡괭이";
                weaponInfo.weaponDesc = "지나가다가 땅에서 주웠습니다.";
                break;

            case 1:
                weaponInfo.weaponId = 1;
                weaponInfo.weaponName = "노르웨이산 손도끼";
                weaponInfo.weaponDesc = "바이킹족이 즐겨 쓰는 손도끼라고 합니다.";
                break;

            case 2:
                weaponInfo.weaponId = 2;
                weaponInfo.weaponName = "족장 전용 장난감";
                weaponInfo.weaponDesc = "고블린 족장이 재밌게 갖고 놀길래 몰래 훔쳐왔습니다.";
                break;

            case 3:
                weaponInfo.weaponId = 3;
                weaponInfo.weaponName = "차가운 나뭇가지";
                weaponInfo.weaponDesc = "한여름에도 차가워서 선풍기 대신에 사용하기도 합니다.";
                break;

            case 4:
                weaponInfo.weaponId = 4;
                weaponInfo.weaponName = "곡괨미";
                weaponInfo.weaponDesc = "넴모마믈 괌부들미 기즘한 것밉니다.";
                break;

            case 5:
                weaponInfo.weaponId = 5;
                weaponInfo.weaponName = "완두콩 파쇄기";
                weaponInfo.weaponDesc = "완두콩을 파쇄시키려고 누군가 만들었다고 합니다.";
                break;

            case 6:
                weaponInfo.weaponId = 6;
                weaponInfo.weaponName = "곡괭이의 영혼";
                weaponInfo.weaponDesc = "\"정팔이 누나꺼 건들면 큰일나!\"";
                break;
        }

        return weaponInfo;
    }


    //무기 실제 정보 가져오기    
    public static cAxe GetAxeInfo(cProperty pLevel)
    {
        cAxe tAxe = new cAxe();

        //레벨
        tAxe.level.value = pLevel.value;

        //데미지
        if (pLevel.value <= 9)
        {
            tAxe.damage.value = 10 + 2 * pLevel.value;
        }
        else if (pLevel.value <= 17)
        {
            tAxe.damage.value = 26 + (pLevel.value - 9) * 4;
        }
        else if (pLevel.value <= 25)
        {
            tAxe.damage.value = 58 + (pLevel.value - 17) * 10;
        }
        else if (pLevel.value <= 33)
        {
            tAxe.damage.value = 138 + (pLevel.value - 25) * 100;
        }
        else if (pLevel.value <= 41)
        {
            tAxe.damage.value = 938 + (pLevel.value - 33) * 400;
        }
        else if (pLevel.value <= 49)
        {
            tAxe.damage.value = 4138 + (pLevel.value - 41) * 800;
        }
        else if (pLevel.value <= 57)
        {
            tAxe.damage.value = 10538 + (pLevel.value - 49) * 2000;
        }
        else if (pLevel.value <= 65)
        {
            tAxe.damage.value = 26538 + (pLevel.value - 57) * 6000;
        }
        else if (pLevel.value <= 73)
        {
            tAxe.damage.value = 74538 + (pLevel.value - 65) * 12000;
        }
        else if (pLevel.value <= 81)
        {
            tAxe.damage.value = 170538 + (pLevel.value - 73) * 50000;
        }
        else if (pLevel.value <= 89)
        {
            tAxe.damage.value = 570538 + (pLevel.value - 81) * 120000;
        }
        else if (pLevel.value <= 99)
        {
            tAxe.damage.value = 1530538 + (pLevel.value - 89) * 500000;
        }

        //체력
        tAxe.hp.value = 100;

        //내구도
        tAxe.indurance.value =300;


        //가치 
        if(pLevel.value <= 25)
        {
            tAxe.value.value = 3000000 + 5000000 * pLevel.value;
        }
        else if(pLevel.value <= 49)
        {
            tAxe.value.value = 800000000 + 50000000 * (pLevel.value - 25);
        }
        else if (pLevel.value <= 73)
        {
            tAxe.value.value = 200000000 + 100000000 * (pLevel.value - 49);
        }
        else
        {
            tAxe.value.value = 600000000 + 150000000 * (pLevel.value - 73);
        }

        tAxe.attackSpeed = 1.0f;
        tAxe.AxeImgNum = 0;

        return tAxe;
    }
    public static cAxe GetAxeInfo(long pLevel)
    {
        cAxe tAxe = new cAxe();

        //레벨
        tAxe.level.value = pLevel;

        //데미지
        if (tAxe.level.value <= 9)
        {
            tAxe.damage.value = 10 + 2 * tAxe.level.value;
        }
        else if (tAxe.level.value <= 17)
        {
            tAxe.damage.value = 26 + (tAxe.level.value - 9) * 4;
        }
        else if (tAxe.level.value <= 25)
        {
            tAxe.damage.value = 58 + (tAxe.level.value - 17) * 10;
        }
        else if (tAxe.level.value <= 33)
        {
            tAxe.damage.value = 138 + (tAxe.level.value - 25) * 100;
        }
        else if (tAxe.level.value <= 41)
        {
            tAxe.damage.value = 938 + (tAxe.level.value - 33) * 400;
        }
        else if (tAxe.level.value <= 49)
        {
            tAxe.damage.value = 4138 + (tAxe.level.value - 41) * 800;
        }
        else if (tAxe.level.value <= 57)
        {
            tAxe.damage.value = 10538 + (tAxe.level.value - 49) * 2000;
        }
        else if (tAxe.level.value <= 65)
        {
            tAxe.damage.value = 26538 + (tAxe.level.value - 57) * 6000;
        }
        else if (tAxe.level.value <= 73)
        {
            tAxe.damage.value = 74538 + (tAxe.level.value - 65) * 12000;
        }
        else if (tAxe.level.value <= 81)
        {
            tAxe.damage.value = 170538 + (tAxe.level.value - 73) * 50000;
        }
        else if (tAxe.level.value <= 89)
        {
            tAxe.damage.value = 570538 + (tAxe.level.value - 81) * 120000;
        }
        else if (tAxe.level.value <= 99)
        {
            tAxe.damage.value = 1530538 + (tAxe.level.value - 89) * 500000;
        }

        //체력
        tAxe.hp.value = 100;

        //내구도
        tAxe.indurance.value = 300;
        //가치 
        if (tAxe.level.value <= 25)
        {
            tAxe.value.value = 3000000 + 5000000 * tAxe.level.value;
        }
        else if (tAxe.level.value <= 49)
        {
            tAxe.value.value = 800000000 + 50000000 * (tAxe.level.value - 25);
        }
        else if (tAxe.level.value <= 73)
        {
            tAxe.value.value = 200000000 + 100000000 * (tAxe.level.value - 49);
        }
        else
        {
            tAxe.value.value = 600000000 + 150000000 * (tAxe.level.value - 73);
        }

        tAxe.attackSpeed = 1.0f;
        tAxe.AxeImgNum = 0;

        return tAxe;
    }

    public static cProperty GetMaxUpgradeLevelByMaxGold(cProperty pCurLevel, cProperty pMoney)
    {
        cProperty canLevel = new cProperty("adsf", 0);

        long factor = 1;
        long upgradeTotalMoney = 0;

        while(true)
        {
            upgradeTotalMoney += GetAxeInfo(pCurLevel.value + factor).value.value;
            if (upgradeTotalMoney > 9000000000000000000)
            {
                canLevel.value += factor;
                break;
            }

            if (pMoney.value < upgradeTotalMoney)
            {
                canLevel.value += (factor - 1);
                break;
            }

            factor++;
        }

        return canLevel;
    }

    public static cProperty GetUpgradeGoldByLevel(long pCurLevel, long pNextLevel)
    {
        cProperty money = new cProperty("adsf", 0);

        for(long i = pCurLevel; i < pNextLevel; i++)
        {
            money.value +=GetAxeInfo(i + 1).value.value; 
        }

        return money;
    }
}
