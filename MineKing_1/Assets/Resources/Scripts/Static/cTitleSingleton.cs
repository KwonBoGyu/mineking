﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cTitleSingleton : MonoBehaviour
{
    public static cTitleSingleton Instance;
    
    void Awake()
    {
        if (Instance != null)
        {
            Destroy(this.gameObject);
            return;
        }

        Instance = this;

        DontDestroyOnLoad(this.gameObject);

        //해상도 설정
        Screen.SetResolution(1920, 1080, false);
               
        //싱글톤 클래스 설정
        cUtil._sm = this.transform.Find("SceneManager").GetComponent<cSceneManager>();
        cUtil._user = this.GetComponent<cUser>();
        cUtil._timeMng = this.GetComponent<cTimeMng>();

        //씬 매니져 초기화
        cUtil._sm.Init();

        //데이터 로딩 시작
        cUtil._user.Init();

    }

    private void FixedUpdate()
    {
         //돌아가기 버튼 누르면 게임 종료
        if (Input.GetKeyDown(KeyCode.Escape))
            ExitApp();
    }

    public void ExitApp()
    {
        cUtil._user.SaveUserData();
        Application.Quit();
    }

    //private void OnApplicationPause(bool pause)
    //{
    //    cUtil._user.SaveUserData();
    //}

    private void OnApplicationQuit()
    {
        cUtil._user.SaveUserData();
    }
}
