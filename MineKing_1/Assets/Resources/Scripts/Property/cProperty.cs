﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class cProperty
{
    public string _name;
    public long value;

    #region 생성자
    public cProperty(string pName, long pValue)
    {
        _name = pName;
        value = pValue;
    }
    public cProperty(string pName)
    {
        _name = pName;
        value = 0;
    }
    public cProperty(cProperty pProperty)
    {
        _name = pProperty._name;
        value = pProperty.value;
    }
    #endregion    
}
