﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

[System.Serializable]
public enum MaskType { OFF, GONE, RED, GREEN, BLUE, CYAN, MAGENTA, YELLOW, BLACK, WHITE, NEGATIVE, GRAY, FADE};

[ExecuteInEditMode]
public class SpriteMaskScript : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public TilemapRenderer tileRenderer;
    public MaskType type = MaskType.OFF;
    public float distance = 5;
    public GameObject[] target;
    private float[] targetX;
    private float[] targetY;
    public float[] renderDistance;
    public bool isInit;
    private float distMinus;

    public Button[] b_distMinusChange;
    public Text t_distMinus;
    public Button[] b_renderDistChange;
    public Text t_renderDist;

    void Start()
    {
        spriteRenderer = this.GetComponent<SpriteRenderer>();
        if (spriteRenderer == null)
            tileRenderer = this.GetComponent<TilemapRenderer>();

        targetX = new float[30];
        targetY = new float[30];

        isInit = true;
    }

    private void DistChange(bool isPlus)
    {
        if (isPlus.Equals(true))
            distMinus += 20.0f;
        else
            distMinus -= 20.0f;

        t_distMinus.text = distMinus.ToString();
    }
    private void RenderDistChange(bool isPlus)
    {
        cUtil._player.ChangeLightDist(20, isPlus);
    }

    public int SetLight(GameObject pObj, float pDist)
    {
        int r_idx = 0;

        for(int i = 0; i < target.Length; i++)
        {
            if (target[i] == null)
            {
                r_idx = i;
                target[i] = pObj;
                renderDistance[i] = pDist;

                break;
            }
        }

        return r_idx;
    }

    public void ChangeLight(int pIdx, float pDist)
    {
        renderDistance[pIdx] = pDist;

        if (pIdx.Equals(0))
            t_renderDist.text = pDist.ToString();
    }

    public void RemoveLight(int pIdx)
    {
        target[pIdx] = null;
        renderDistance[pIdx] = 0;
    }

    void Update()
    {
        if (target == null)
            return;

        if (isInit.Equals(false))
            return;

        targetX = new float[target.Length];
        targetY = new float[target.Length];

        for(byte i = 0; i < target.Length; i++)
        {
            if(target[i] == null)
            {
                continue;
            }

            targetX[i] = target[i].transform.position.x;
            targetY[i] = target[i].transform.position.y;
        }

        updateShader(targetX, targetY, renderDistance);
        //toggleShader();
    }

    private int typeToInt()
    {
        if (distance <= 0 || type == MaskType.OFF)
            return 0;

        switch(type)
        {
            case MaskType.GONE: return 1;
            case MaskType.RED: return 2;
            case MaskType.GREEN: return 3;
            case MaskType.BLUE: return 4;
            case MaskType.CYAN: return 5;
            case MaskType.MAGENTA: return 6;
            case MaskType.YELLOW: return 7;
            case MaskType.BLACK: return 8;
            case MaskType.WHITE: return 9;
            case MaskType.NEGATIVE: return 10;
            case MaskType.GRAY: return 11;
            case MaskType.FADE: return 12;

        }
        return 0;
    }

    private void toggleShader()
    {
        distance = Mathf.Clamp(distance + Input.GetAxis("Vertical"), -0.1f, 1000);
    }

    private void updateShader(float[] pTargetX, float[] pTargetY, float[] pDistance)
    {
        if (spriteRenderer == null && tileRenderer == null || pTargetX == null)
            return;

        MaterialPropertyBlock mpb = new MaterialPropertyBlock();
        if (spriteRenderer != null)
            spriteRenderer.GetPropertyBlock(mpb);
        if (tileRenderer != null)
            tileRenderer.GetPropertyBlock(mpb);

        mpb.SetFloatArray("_RenderDistance", pDistance);
        mpb.SetInt("_TargetCount", pTargetX.Length);
        mpb.SetFloatArray("_MaskTargetX", pTargetX);
        mpb.SetFloatArray("_MaskTargetY", pTargetY);
        mpb.SetFloat("_DistMinus", distMinus);
        mpb.SetFloat("_MaskType", 12);

        if (spriteRenderer != null)
            spriteRenderer.SetPropertyBlock(mpb);
        if (tileRenderer != null)
            tileRenderer.SetPropertyBlock(mpb);
    }
}
