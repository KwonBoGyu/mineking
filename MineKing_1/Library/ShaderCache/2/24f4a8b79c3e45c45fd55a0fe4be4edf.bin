<Q                           S"  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	mediump vec4 _Color;
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec2 in_TEXCOORD0;
out mediump vec4 vs_COLOR0;
out highp vec2 vs_TEXCOORD0;
out mediump vec4 vs_WORLDPOS0;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat1 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_WORLDPOS0 = u_xlat0;
    u_xlat0 = u_xlat1 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    u_xlat0 = in_COLOR0 * _Color;
    vs_COLOR0 = u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	int _TargetCount;
uniform 	float _MaskTargetX[30];
uniform 	float _MaskTargetY[30];
uniform 	float _RenderDistance[30];
uniform 	float _MaskType;
UNITY_LOCATION(0) uniform mediump sampler2D _MainTex;
in mediump vec4 vs_COLOR0;
in highp vec2 vs_TEXCOORD0;
in mediump vec4 vs_WORLDPOS0;
layout(location = 0) out mediump vec4 SV_TARGET0;
mediump vec4 u_xlat16_0;
bool u_xlatb1;
bvec4 u_xlatb2;
bvec4 u_xlatb3;
bvec4 u_xlatb4;
float u_xlat5;
mediump vec4 u_xlat16_5;
mediump vec3 u_xlat16_6;
mediump vec3 u_xlat16_7;
mediump vec3 u_xlat16_8;
mediump vec3 u_xlat16_9;
mediump vec3 u_xlat16_10;
mediump vec3 u_xlat16_11;
mediump vec4 u_xlat16_12;
mediump vec3 u_xlat16_13;
int u_xlati14;
int u_xlati16;
mediump float u_xlat16_20;
bool u_xlatb29;
int u_xlati31;
bool u_xlatb31;
float u_xlat46;
mediump float u_xlat16_46;
void main()
{
    u_xlat16_0 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat16_0 = u_xlat16_0 * vs_COLOR0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(0.0<_MaskType);
#else
    u_xlatb1 = 0.0<_MaskType;
#endif
    u_xlatb2 = greaterThanEqual(vec4(1.0, 2.0, 3.0, 4.0), vec4(vec4(_MaskType, _MaskType, _MaskType, _MaskType)));
    u_xlatb3 = greaterThanEqual(vec4(5.0, 6.0, 7.0, 8.0), vec4(vec4(_MaskType, _MaskType, _MaskType, _MaskType)));
    u_xlatb4 = greaterThanEqual(vec4(9.0, 10.0, 11.0, 12.0), vec4(vec4(_MaskType, _MaskType, _MaskType, _MaskType)));
    u_xlat16_5.z = -1.0;
    u_xlat16_6.y = float(0.0);
    u_xlat16_6.z = float(0.0);
    u_xlat16_7.x = float(0.0);
    u_xlat16_7.z = float(0.0);
    u_xlat16_8.x = 0.0;
    u_xlat16_9.x = 0.0;
    u_xlat16_10.y = 0.0;
    u_xlat16_11.z = 0.0;
    u_xlat16_12 = u_xlat16_0;
    u_xlati16 = 0;
    while(true){
#ifdef UNITY_ADRENO_ES3
        u_xlatb31 = !!(u_xlati16>=_TargetCount);
#else
        u_xlatb31 = u_xlati16>=_TargetCount;
#endif
        if(u_xlatb31){break;}
#ifdef UNITY_ADRENO_ES3
        u_xlatb31 = !!(0.0==_RenderDistance[u_xlati16]);
#else
        u_xlatb31 = 0.0==_RenderDistance[u_xlati16];
#endif
        if(u_xlatb31){
            u_xlati31 = u_xlati16 + 1;
            u_xlati16 = u_xlati31;
            continue;
        }
        u_xlat16_5.x = (-_MaskTargetX[u_xlati16]);
        u_xlat16_5.y = (-_MaskTargetY[u_xlati16]);
        u_xlat16_5.xyw = u_xlat16_5.xyz + vs_WORLDPOS0.xyw;
        u_xlat16_5.x = dot(u_xlat16_5.xyw, u_xlat16_5.xyw);
        u_xlat16_5.x = sqrt(u_xlat16_5.x);
#ifdef UNITY_ADRENO_ES3
        u_xlatb31 = !!(_RenderDistance[u_xlati16]>=u_xlat16_5.x);
#else
        u_xlatb31 = _RenderDistance[u_xlati16]>=u_xlat16_5.x;
#endif
        u_xlatb31 = u_xlatb31 && u_xlatb1;
        u_xlat16_46 = u_xlat16_5.x + -0.300000012;
        u_xlat46 = u_xlat16_46 / _RenderDistance[u_xlati16];
        u_xlat16_13.xyz = u_xlat16_12.xyz;
        u_xlat16_5.x = u_xlat16_12.w;
        for(int u_xlati_loop_1 = 0 ; u_xlati_loop_1<2 ; u_xlati_loop_1++)
        {
            if(u_xlatb31){
                if(u_xlatb2.x){
                    u_xlat16_5.x = 0.0;
                } else {
                    if(u_xlatb2.y){
                        u_xlat16_6.x = u_xlat16_13.x;
                        u_xlat16_13.xyz = u_xlat16_6.xyz;
                    } else {
                        if(u_xlatb2.z){
                            u_xlat16_7.y = u_xlat16_13.y;
                            u_xlat16_13.xyz = u_xlat16_7.xyz;
                        } else {
                            if(u_xlatb2.w){
                                u_xlat16_8.z = u_xlat16_13.z;
                                u_xlat16_13.xyz = u_xlat16_8.xxz;
                            } else {
                                if(u_xlatb3.x){
                                    u_xlat16_9.yz = u_xlat16_13.yz;
                                    u_xlat16_13.xyz = u_xlat16_9.xyz;
                                } else {
                                    if(u_xlatb3.y){
                                        u_xlat16_10.xz = u_xlat16_13.xz;
                                        u_xlat16_13.xyz = u_xlat16_10.xyz;
                                    } else {
                                        if(u_xlatb3.z){
                                            u_xlat16_11.xy = u_xlat16_13.xy;
                                            u_xlat16_13.xyz = u_xlat16_11.xyz;
                                        } else {
                                            if(u_xlatb3.w){
                                                u_xlat16_13.x = float(0.0);
                                                u_xlat16_13.y = float(0.0);
                                                u_xlat16_13.z = float(0.0);
                                            } else {
                                                if(u_xlatb4.x){
                                                    u_xlat16_13.x = float(1.0);
                                                    u_xlat16_13.y = float(1.0);
                                                    u_xlat16_13.z = float(1.0);
                                                } else {
                                                    if(u_xlatb4.y){
                                                        u_xlat16_13.xyz = (-u_xlat16_13.xyz) + vec3(1.0, 1.0, 1.0);
                                                    } else {
                                                        if(u_xlatb4.z){
                                                            u_xlat16_20 = u_xlat16_13.y * 0.587000012;
                                                            u_xlat16_20 = u_xlat16_13.x * 0.298999995 + u_xlat16_20;
                                                            u_xlat16_13.xyz = u_xlat16_13.zzz * vec3(0.114, 0.114, 0.114) + vec3(u_xlat16_20);
                                                        } else {
                                                            if(u_xlatb4.w){
                                                                u_xlat5 = u_xlat46 * u_xlat16_5.x;
                                                                u_xlat16_5.x = u_xlat5;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            u_xlat16_13.xyz = u_xlat16_5.xxx * u_xlat16_13.xyz;
        }
        u_xlat16_12.xyz = u_xlat16_13.xyz;
        u_xlat16_12.w = u_xlat16_5.x;
        u_xlati16 = u_xlati16 + 1;
    }
    SV_TARGET0 = u_xlat16_12;
    return;
}

#endif
                                 _MainTex               