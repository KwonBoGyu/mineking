<Q                         PIXELSNAP_ON    v  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _ScreenParams;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	mediump vec4 _Color;
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec2 in_TEXCOORD0;
out mediump vec4 vs_COLOR0;
out highp vec2 vs_TEXCOORD0;
out mediump vec4 vs_WORLDPOS0;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat1 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_WORLDPOS0 = u_xlat0;
    u_xlat0 = u_xlat1 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    u_xlat0.xy = u_xlat0.xy / u_xlat0.ww;
    u_xlat1.xy = _ScreenParams.xy * vec2(0.5, 0.5);
    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
    u_xlat0.xy = roundEven(u_xlat0.xy);
    u_xlat0.xy = u_xlat0.xy / u_xlat1.xy;
    gl_Position.xy = u_xlat0.ww * u_xlat0.xy;
    gl_Position.zw = u_xlat0.zw;
    u_xlat0 = in_COLOR0 * _Color;
    vs_COLOR0 = u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	float _MaskTargetX;
uniform 	float _MaskTargetY;
uniform 	float _RenderDistance;
uniform 	float _MaskType;
UNITY_LOCATION(0) uniform mediump sampler2D _MainTex;
in mediump vec4 vs_COLOR0;
in highp vec2 vs_TEXCOORD0;
in mediump vec4 vs_WORLDPOS0;
layout(location = 0) out mediump vec4 SV_TARGET0;
mediump vec4 u_xlat16_0;
mediump vec4 u_xlat16_1;
bvec4 u_xlatb2;
mediump vec4 u_xlat16_3;
bvec2 u_xlatb4;
bool u_xlatb7;
float u_xlat14;
void main()
{
    u_xlat16_0 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat16_0 = u_xlat16_0 * vs_COLOR0;
    u_xlat16_1.xy = (-vec2(_MaskTargetX, _MaskTargetY));
    u_xlat16_1.z = -1.0;
    u_xlat16_1.xyz = u_xlat16_1.xyz + vs_WORLDPOS0.xyw;
    u_xlat16_1.x = dot(u_xlat16_1.xyz, u_xlat16_1.xyz);
    u_xlat16_1.x = sqrt(u_xlat16_1.x);
#ifdef UNITY_ADRENO_ES3
    u_xlatb2.x = !!(0.0<_MaskType);
#else
    u_xlatb2.x = 0.0<_MaskType;
#endif
#ifdef UNITY_ADRENO_ES3
    u_xlatb7 = !!(_RenderDistance>=u_xlat16_1.x);
#else
    u_xlatb7 = _RenderDistance>=u_xlat16_1.x;
#endif
    u_xlatb2.x = u_xlatb7 && u_xlatb2.x;
    if(u_xlatb2.x){
#ifdef UNITY_ADRENO_ES3
        u_xlatb2.x = !!(1.0>=_MaskType);
#else
        u_xlatb2.x = 1.0>=_MaskType;
#endif
        if(u_xlatb2.x){
            u_xlat16_0.w = 0.0;
        } else {
#ifdef UNITY_ADRENO_ES3
            u_xlatb2.x = !!(2.0>=_MaskType);
#else
            u_xlatb2.x = 2.0>=_MaskType;
#endif
            if(u_xlatb2.x){
                u_xlat16_0.y = float(0.0);
                u_xlat16_0.z = float(0.0);
            } else {
#ifdef UNITY_ADRENO_ES3
                u_xlatb2.x = !!(3.0>=_MaskType);
#else
                u_xlatb2.x = 3.0>=_MaskType;
#endif
                if(u_xlatb2.x){
                    u_xlat16_0.x = float(0.0);
                    u_xlat16_0.z = float(0.0);
                } else {
#ifdef UNITY_ADRENO_ES3
                    u_xlatb2.x = !!(4.0>=_MaskType);
#else
                    u_xlatb2.x = 4.0>=_MaskType;
#endif
                    if(u_xlatb2.x){
                        u_xlat16_0.x = 0.0;
                        u_xlat16_0.xyz = u_xlat16_0.xxz;
                    } else {
#ifdef UNITY_ADRENO_ES3
                        u_xlatb2.x = !!(5.0>=_MaskType);
#else
                        u_xlatb2.x = 5.0>=_MaskType;
#endif
                        if(u_xlatb2.x){
                            u_xlat16_0.x = 0.0;
                        } else {
#ifdef UNITY_ADRENO_ES3
                            u_xlatb2.x = !!(6.0>=_MaskType);
#else
                            u_xlatb2.x = 6.0>=_MaskType;
#endif
                            if(u_xlatb2.x){
                                u_xlat16_0.y = 0.0;
                            } else {
                                u_xlatb2 = greaterThanEqual(vec4(7.0, 8.0, 9.0, 10.0), vec4(vec4(_MaskType, _MaskType, _MaskType, _MaskType)));
                                u_xlat16_3.x = dot(u_xlat16_0.xyz, vec3(0.298999995, 0.587000012, 0.114));
                                u_xlatb4.xy = greaterThanEqual(vec4(11.0, 12.0, 0.0, 0.0), vec4(vec4(_MaskType, _MaskType, _MaskType, _MaskType))).xy;
                                u_xlat14 = u_xlat16_1.x / _RenderDistance;
                                u_xlat14 = (-u_xlat14) + 1.0;
                                u_xlat14 = u_xlat16_0.w * u_xlat14;
                                u_xlat16_1.w = (u_xlatb4.y) ? u_xlat14 : u_xlat16_0.w;
                                u_xlat16_3.y = u_xlat16_0.w;
                                u_xlat16_1.xyz = u_xlat16_0.xyz;
                                u_xlat16_1 = (u_xlatb4.x) ? u_xlat16_3.xxxy : u_xlat16_1;
                                u_xlat16_3 = u_xlat16_0 * vec4(-1.0, -1.0, -1.0, 1.0) + vec4(1.0, 1.0, 1.0, 0.0);
                                u_xlat16_1 = (u_xlatb2.w) ? u_xlat16_3 : u_xlat16_1;
                                u_xlat16_3.x = 1.0;
                                u_xlat16_1 = (u_xlatb2.z) ? u_xlat16_3.xxxw : u_xlat16_1;
                                u_xlat16_3.x = 0.0;
                                u_xlat16_1 = (u_xlatb2.y) ? u_xlat16_3.xxxw : u_xlat16_1;
                                u_xlat16_0.z = 0.0;
                                u_xlat16_0 = (u_xlatb2.x) ? u_xlat16_0 : u_xlat16_1;
                            }
                        }
                    }
                }
            }
        }
    }
    u_xlat16_0.xyz = u_xlat16_0.www * u_xlat16_0.xyz;
    SV_TARGET0 = u_xlat16_0;
    return;
}

#endif
                                  _MainTex               